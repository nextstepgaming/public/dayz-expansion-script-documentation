var a02957 =
[
    [ "ExpansionVehicleAerofoil", "d3/ddf/a07538.html", "d3/ddf/a07538" ],
    [ "ExpansionVehicleAerofoilType", "dd/d08/a02957.html#ab16f42902f07206047fd418449950ef2", [
      [ "Fixed", "dd/d08/a02957.html#ab16f42902f07206047fd418449950ef2a8da0283267b1c9960b792a579407ff02", null ],
      [ "Wing", "dd/d08/a02957.html#ab16f42902f07206047fd418449950ef2aff91499595bf5b12344ac7e18357801d", null ],
      [ "Rudder", "dd/d08/a02957.html#ab16f42902f07206047fd418449950ef2a3aebaa706ae6a6115104193dc6ab9a2a", null ],
      [ "Elevator", "dd/d08/a02957.html#ab16f42902f07206047fd418449950ef2ad6a1a7522c999e5b52ad5e9e06f8de7c", null ]
    ] ],
    [ "ExpansionVehicleAerofoilTypeL", "dd/d08/a02957.html#abfd5d44cbcc726c81f9363a128fdb845", [
      [ "fixed", "dd/d08/a02957.html#abfd5d44cbcc726c81f9363a128fdb845ab9ad6d537a019231f52c2e5ed22d5bfb", null ],
      [ "wing", "dd/d08/a02957.html#abfd5d44cbcc726c81f9363a128fdb845aeb5787fd6439a811ecc1a123f0afb4a7", null ],
      [ "rudder", "dd/d08/a02957.html#abfd5d44cbcc726c81f9363a128fdb845aecc4d6747278f4995f9aa69ed026830e", null ],
      [ "elevator", "dd/d08/a02957.html#abfd5d44cbcc726c81f9363a128fdb845a6bdd90c005b31c4b75c97a7de73478a9", null ]
    ] ]
];