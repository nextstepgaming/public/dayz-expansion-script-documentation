var a06038 =
[
    [ "MaxValue", "dd/d4e/a06038.html#a23f773979c36af4e715a1826f535fdd6", null ],
    [ "MoneyDepositValue", "dd/d4e/a06038.html#aabc94f6d85c6bdfc222cbeeaeeefb42c", null ],
    [ "PartyID", "dd/d4e/a06038.html#a27c78b1dd3191aa78ea01a3e72c3acf7", null ],
    [ "PartyMaxValue", "dd/d4e/a06038.html#a05ea17ed752192f2e5e8f637f6d06af1", null ],
    [ "PartyMoneyDepositValue", "dd/d4e/a06038.html#a25da8ab97c63f304987af3219ea8df50", null ],
    [ "PartyName", "dd/d4e/a06038.html#adb9404e52979df18216e57cae4ba7ae4", null ],
    [ "PartyOwner", "dd/d4e/a06038.html#acb5de7c1c9651451df82a59a01d2f7bf", null ],
    [ "PlayerEntries", "dd/d4e/a06038.html#a7aa59444e359ebf59d037aa234a075ac", null ],
    [ "PlayerMoneyValue", "dd/d4e/a06038.html#afdffd0cdb423fa0dc470885aceb65881", null ]
];