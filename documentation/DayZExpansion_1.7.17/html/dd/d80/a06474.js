var a06474 =
[
    [ "ExpansionQuestNPCBase", "dd/d80/a06474.html#a10a4eab433ac7b50b2b99bc83a312086", null ],
    [ "DeferredInit", "dd/d80/a06474.html#aac309467b528b42ee87b8082ba25be26", null ],
    [ "GetQuestNPCData", "dd/d80/a06474.html#a641fc8660ab606d9a1fa21d627b4edb9", null ],
    [ "GetQuestNPCID", "dd/d80/a06474.html#af79265ed197fef36102f8b3ad76d8f40", null ],
    [ "SetQuestNPCData", "dd/d80/a06474.html#a934c7d639150d2caa71518b581ca471a", null ],
    [ "SetQuestNPCID", "dd/d80/a06474.html#a7f332fb3e5996c142f916fe313219a86", null ],
    [ "m_QuestNPCData", "dd/d80/a06474.html#abf5a661bcc78b6987400a3e0a6d0e835", null ],
    [ "m_QuestNPCID", "dd/d80/a06474.html#ab62a8637ee05b8a465493765a9e9e43d", null ]
];