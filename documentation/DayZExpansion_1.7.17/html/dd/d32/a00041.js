var a00041 =
[
    [ "ExpansionTerritorySettingsBase", "df/d5f/a03494.html", "df/d5f/a03494" ],
    [ "Copy", "dd/d32/a00041.html#a8bc9f37c8b51353bb7abe4a847ec9846", null ],
    [ "CopyInternal", "dd/d32/a00041.html#a35f99980d4f7df05b56bf1b48bbb87ef", null ],
    [ "CopyInternal", "dd/d32/a00041.html#a9a0fa6d94d0c59f58757d664e5ebb15a", null ],
    [ "Defaults", "dd/d32/a00041.html#ac06ab75f9e6f636d8b7c17685037667e", null ],
    [ "IsLoaded", "dd/d32/a00041.html#ad9eab3191d8b7cc0f5414602631292b8", null ],
    [ "OnLoad", "dd/d32/a00041.html#a3578afd97750bc428c2737e242dffd3d", null ],
    [ "OnRecieve", "dd/d32/a00041.html#ae3e27fdaa6ddb0d424626c1f0b3aad2e", null ],
    [ "OnSave", "dd/d32/a00041.html#a9fb9cc278f9d90f49e9dd9ce5ce40d43", null ],
    [ "OnSend", "dd/d32/a00041.html#af2c207d1a6c5eea63c6296925ace1d76", null ],
    [ "Send", "dd/d32/a00041.html#a8f26e3b21fa656c6d6048f82cfb30a72", null ],
    [ "SettingName", "dd/d32/a00041.html#a46ab2e61c4bb8c8b19c3aece63908a50", null ],
    [ "Unload", "dd/d32/a00041.html#a064d4576e4b054e72d5de632a7d09a63", null ],
    [ "EnableTerritories", "dd/d32/a00041.html#a3ff335cd5d8861f0a1aa07a280a451ba", null ],
    [ "m_IsLoaded", "dd/d32/a00041.html#a5521ad2d5fb3360b7943c9cbc422642a", null ],
    [ "MaxMembersInTerritory", "dd/d32/a00041.html#a7a2d1e19df817c90c1f8c9c18b8db3c5", null ],
    [ "MaxTerritoryPerPlayer", "dd/d32/a00041.html#a21482c9835487af418d7e2c412d1d442", null ],
    [ "TerritoryAuthenticationRadius", "dd/d32/a00041.html#a0660d5e16dbc928e9daa81f4380250d4", null ],
    [ "TerritorySize", "dd/d32/a00041.html#afc2067c46a67c76ef1dba01f21a015e6", null ],
    [ "UseWholeMapForInviteList", "dd/d32/a00041.html#a403ed449c1c5efd661a8a1c6717c7cad", null ],
    [ "VERSION", "dd/d32/a00041.html#ae48ea820d6b3acc7353b763034c772af", null ]
];