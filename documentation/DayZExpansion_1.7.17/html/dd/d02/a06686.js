var a06686 =
[
    [ "AddAttachment", "dd/d02/a06686.html#ae00456859e628fc1f97f8e42f1f10cf4", null ],
    [ "GetAmount", "dd/d02/a06686.html#a17e1f9be6b6ff3ca46a984827391c78c", null ],
    [ "GetAttachments", "dd/d02/a06686.html#a2abe5581b5456b47524932689db4e290", null ],
    [ "GetClassName", "dd/d02/a06686.html#aadddc6050020a3588e60f979e41794dc", null ],
    [ "IsVehicle", "dd/d02/a06686.html#af0e6e383ba63f90aab9d53118a110ee1", null ],
    [ "OnRecieve", "dd/d02/a06686.html#a9379e99d1af77b16ba1c87a6ac8d3eb4", null ],
    [ "OnSend", "dd/d02/a06686.html#acdfc2acae2e35ea1b735c4c82387d6fa", null ],
    [ "QuestDebug", "dd/d02/a06686.html#a1702b028300a34776fafd37bf221719e", null ],
    [ "SetAmount", "dd/d02/a06686.html#a81229698dc613770df4645581144c4f8", null ],
    [ "SetClassName", "dd/d02/a06686.html#acdae560b2dabbd4dc37e3985d2e69b36", null ],
    [ "Attachments", "dd/d02/a06686.html#aefaf471efacce12828f618c1fbb88e7f", null ]
];