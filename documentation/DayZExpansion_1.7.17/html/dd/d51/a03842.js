var a03842 =
[
    [ "ExpansionCamoTent", "dd/d51/a03842.html#a52d39b2e07bff81adc92bc682af8b8b6", null ],
    [ "~ExpansionCamoTent", "dd/d51/a03842.html#addb58904601ae326ff83752dca157cb0", null ],
    [ "CanBeDamaged", "dd/d51/a03842.html#a425116fbe029599be43938cea4671403", null ],
    [ "CanObstruct", "dd/d51/a03842.html#a07c1d9230e72ef605b6a0cf0dcf15308", null ],
    [ "CanPutInCargo", "dd/d51/a03842.html#a96df264783cd1ec0990fdef2af5fa109", null ],
    [ "CanPutIntoHands", "dd/d51/a03842.html#a2a9e2e98264a5cf0707fcd2492b83318", null ],
    [ "GetConstructionKitType", "dd/d51/a03842.html#ac96360d4c2bedec7e62536d2f818a50c", null ],
    [ "IsInventoryVisible", "dd/d51/a03842.html#a9b013fe0ef14260146e77fded319e54a", null ],
    [ "SetPartsAfterStoreLoad", "dd/d51/a03842.html#a8ac14133aec65a6309b645aaf447b734", null ],
    [ "m_CanBeDamaged", "dd/d51/a03842.html#ab626c54bc28f3d4d97df72d15ca7a263", null ]
];