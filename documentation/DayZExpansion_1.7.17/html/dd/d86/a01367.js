var a01367 =
[
    [ "ExpansionMenuDialogContent_WrapSpacer", "d2/dbd/a05254.html", "d2/dbd/a05254" ],
    [ "ExpansionMenuDialogContent_WrapSpacerController", "d8/d44/a05258.html", "d8/d44/a05258" ],
    [ "ExpansionMenuDialogContent_WrapSpacer_EntryController", "dd/df0/a05262.html", "dd/df0/a05262" ],
    [ "ExpansionMenuDialogContent_WrapSpacer_Entry", "dd/d86/a01367.html#a3ad6bd5315d58088af903111a9fd7838", null ],
    [ "GetControllerType", "dd/d86/a01367.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "dd/d86/a01367.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "GetWrapSpacerElement", "dd/d86/a01367.html#a04c5ec6f6e8fcda6821423d1c9283d3e", null ],
    [ "SetTextColor", "dd/d86/a01367.html#a8a8a420577d32cf39a2ac11d1e97c537", null ],
    [ "SetView", "dd/d86/a01367.html#aff9a284bb904b06f8ab09a622b5d7d6f", null ],
    [ "entry", "dd/d86/a01367.html#a163104245a2e3c95642c1e1df12dcfd6", null ],
    [ "m_EntryController", "dd/d86/a01367.html#a4938268820b8b77fd570a6c3e90d68fe", null ],
    [ "m_Text", "dd/d86/a01367.html#a11fc80cdcb775f018854049236528029", null ],
    [ "m_WrapSpacerElement", "dd/d86/a01367.html#ad98ef6cfe350a6cfc1fc3881f86bc2ab", null ]
];