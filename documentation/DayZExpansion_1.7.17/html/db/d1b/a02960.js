var a02960 =
[
    [ "ExpansionVehicleAxle", "da/dc0/a07542.html", "da/dc0/a07542" ],
    [ "ExpansionVehicleDifferentialType", "db/d1b/a02960.html#a1b2304cb6d7a6d2de09889238283f01a", [
      [ "RWD", "db/d1b/a02960.html#a1b2304cb6d7a6d2de09889238283f01aaaf81f4b887ef89cf36d971924c44123c", null ],
      [ "FWD", "db/d1b/a02960.html#a1b2304cb6d7a6d2de09889238283f01aa7e34b7ae523ac22114cdb965ee8db601", null ],
      [ "AWD", "db/d1b/a02960.html#a1b2304cb6d7a6d2de09889238283f01aa277d3e81316dc1ecb4f604a6607b8a99", null ]
    ] ]
];