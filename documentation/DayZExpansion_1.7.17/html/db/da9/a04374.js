var a04374 =
[
    [ "ExpansionUIManager", "db/da9/a04374.html#acda79f37bfb9c87da39650b7f1ac665d", null ],
    [ "CloseAll", "db/da9/a04374.html#af29f6e20bd3a248401d6768d5386b24c", null ],
    [ "CloseMenu", "db/da9/a04374.html#a83a0ca7cd985109ffcfeb5df1e2e1845", null ],
    [ "CreateMenuInstance", "db/da9/a04374.html#ab3ff2b61f539c61a77432c9ba1477434", null ],
    [ "CreateSVMenu", "db/da9/a04374.html#a449724576ba0b32af725f7c8c87bcb37", null ],
    [ "DestroySVMenu", "db/da9/a04374.html#af2f6c0995f752e28cabc63863b2b5258", null ],
    [ "GetActiveMenus", "db/da9/a04374.html#a935022ec9207f154bda837ba619315cb", null ],
    [ "GetMenu", "db/da9/a04374.html#a53e9126bb47ccafb5f465e7e6676839c", null ],
    [ "SetMenu", "db/da9/a04374.html#a9d2689b8b61e126ee9f5910ed00cfb3f", null ],
    [ "m_ActiveMenus", "db/da9/a04374.html#acf29a80caafab9cfde1e31304e2ed17f", null ],
    [ "m_CurrentMenu", "db/da9/a04374.html#a245741a343a8b69c22df7485fac0ce75", null ]
];