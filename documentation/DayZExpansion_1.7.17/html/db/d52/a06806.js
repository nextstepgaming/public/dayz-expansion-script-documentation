var a06806 =
[
    [ "DefaultText", "db/d52/a06806.html#a828f3fdfced77c3dca96601389687fb5", null ],
    [ "HumanityVal", "db/d52/a06806.html#a175a7101f9eb4134d7bae2ad98b1c408", null ],
    [ "ObjectiveItems", "db/d52/a06806.html#abad8e9d17d8cb41939f8654b25fe21ae", null ],
    [ "QuestDescription", "db/d52/a06806.html#a9cf6bdaa377a1a58c029d615de83bfdf", null ],
    [ "QuestNPCName", "db/d52/a06806.html#a417025a6ee39fc412085669fa67fd8c8", null ],
    [ "QuestObjective", "db/d52/a06806.html#ae41044923c6d959318b1c09a70a531d7", null ],
    [ "Quests", "db/d52/a06806.html#ac07c6e2c665556f7621f737d130825e0", null ],
    [ "QuestTitle", "db/d52/a06806.html#a85876854d0b50b5cf4c1da015b654917", null ],
    [ "RewardEntries", "db/d52/a06806.html#ac4c082213fe026c9a2992f87085b4725", null ]
];