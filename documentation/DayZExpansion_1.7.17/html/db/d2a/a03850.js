var a03850 =
[
    [ "ExpansionHelipad", "db/d2a/a03850.html#a4af72273a77b654dcd63328d591617dd", null ],
    [ "~ExpansionHelipad", "db/d2a/a03850.html#a1644acb9afb9aef5d384c85594deb5e5", null ],
    [ "CanBeDamaged", "db/d2a/a03850.html#ae3a429a16ef42a53ee53f1cb1c4722df", null ],
    [ "CanPutInCargo", "db/d2a/a03850.html#afce09a06f54a14523334a1cc38a7a2be", null ],
    [ "CanPutIntoHands", "db/d2a/a03850.html#a17d7470f44b6ad658a9d0f91c3b92468", null ],
    [ "GetConstructionKitType", "db/d2a/a03850.html#ad6fb1156040cfafc7d5d6754b169710c", null ],
    [ "SetPartsAfterStoreLoad", "db/d2a/a03850.html#aa416e6c9063151362a4929962eaae4fb", null ]
];