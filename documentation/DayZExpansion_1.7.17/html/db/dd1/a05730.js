var a05730 =
[
    [ "AttachmentIDs", "db/dd1/a05730.html#ae9f94d3857a44f7df88570f29271a255", null ],
    [ "CategoryID", "db/dd1/a05730.html#adb4446c674d4447af39abd5001584382", null ],
    [ "ClassName", "db/dd1/a05730.html#a962a71814e106ec41a44277f1ea6101d", null ],
    [ "m_StockOnly", "db/dd1/a05730.html#af3ac791787cad7de9b6d47b6e0e79b34", null ],
    [ "MaxPriceThreshold", "db/dd1/a05730.html#abb7c07744d7deefa33633a7b2b135fa8", null ],
    [ "MaxStockThreshold", "db/dd1/a05730.html#a48626c73512a698933aa60ee52e4248c", null ],
    [ "MinPriceThreshold", "db/dd1/a05730.html#ae8f5c1c34fdd322d4a0d31761bae561e", null ],
    [ "MinStockThreshold", "db/dd1/a05730.html#a538e81ee56d3cc0e2e3645b6ecdf247f", null ],
    [ "Packed", "db/dd1/a05730.html#a81edd6ed7c01721f338147cb75f3b2a9", null ],
    [ "Variants", "db/dd1/a05730.html#a6ccf0552df570db23886483875557570", null ]
];