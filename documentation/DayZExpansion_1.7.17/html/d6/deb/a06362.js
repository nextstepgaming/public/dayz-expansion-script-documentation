var a06362 =
[
    [ "CanCreate3DMarker", "d6/deb/a06362.html#a362b61d38a0df4822118acdb4dfefdb2", null ],
    [ "CanCreateMarker", "d6/deb/a06362.html#a56ff657a1955a2d4fc6e36f46f8e17a6", null ],
    [ "CanOpenMapWithKeyBinding", "d6/deb/a06362.html#a7da9507ae4ac8cf60ff1a7437393355c", null ],
    [ "EnableHUDGPS", "d6/deb/a06362.html#a4b742e94beea6377d6e3d7614308173f", null ],
    [ "EnableMap", "d6/deb/a06362.html#ae0696bee57fe9541a2dd34bec1bc7118", null ],
    [ "EnableServerMarkers", "d6/deb/a06362.html#a016624f90fd5aa741e248c988ff866ca", null ],
    [ "NeedGPSItemForCreateMarker", "d6/deb/a06362.html#ab3f8cd1f4af7bab48c8049dba271014e", null ],
    [ "NeedGPSItemForKeyBinding", "d6/deb/a06362.html#aec40c5a315a2aac3d4369bf8fed7e6bc", null ],
    [ "NeedMapItemForKeyBinding", "d6/deb/a06362.html#a4c4dfceb2d2540592b3d8ba33a1a11aa", null ],
    [ "NeedPenItemForCreateMarker", "d6/deb/a06362.html#a4c90408c283cf42e0a97b3cd3ff12b1b", null ],
    [ "ServerMarkers", "d6/deb/a06362.html#a43690dd2b57ed3b9e36e818cc42f6473", null ],
    [ "ShowDistanceOnPersonalMarkers", "d6/deb/a06362.html#a7f4100ac8366a0bedc58a73d3e73b580", null ],
    [ "ShowDistanceOnServerMarkers", "d6/deb/a06362.html#a142769bb91a392b7bd9dc3db891d69ae", null ],
    [ "ShowMapStats", "d6/deb/a06362.html#acd4465563f25bde6e6fc2e069dc81e9a", null ],
    [ "ShowNameOnServerMarkers", "d6/deb/a06362.html#a26e79765f30e05a02cc38333e96ecbbf", null ],
    [ "ShowPlayerPosition", "d6/deb/a06362.html#a9019080bfb1b9d45e0020a2923b80d06", null ],
    [ "UseMapOnMapItem", "d6/deb/a06362.html#a1cf2590d68606d49a419aa2689ae3755", null ]
];