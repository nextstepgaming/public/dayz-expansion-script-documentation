var a09379 =
[
    [ "ExpansionItemTooltip", "d6/df3/a05294.html", "d6/df3/a05294" ],
    [ "ExpansionItemTooltipStatElement", "de/dd6/a05298.html", "de/dd6/a05298" ],
    [ "ExpansionItemPreviewTooltip", "db/dd7/a05302.html", "db/dd7/a05302" ],
    [ "ItemCleanness", "d6/d31/a09379.html#a7d3fafac1963f7b98aca9fc16f8bfa0a", null ],
    [ "ItemDamage", "d6/d31/a09379.html#a59f633dc62af219ace9e41b24c17029d", null ],
    [ "ItemDescription", "d6/d31/a09379.html#a8cadaadd7312c29a2800b73925cb7399", null ],
    [ "ItemFoodStage", "d6/d31/a09379.html#a6f02b995e286df14f9e72ed636e9ced9", null ],
    [ "ItemLiquidType", "d6/d31/a09379.html#a40a4464b7361d85df0d5dd2ba3cccc94", null ],
    [ "ItemName", "d6/d31/a09379.html#ac2b0050eea85abb5a689375625686d09", null ],
    [ "ItemPreview", "d6/d31/a09379.html#a52eb90453e41dd29e6a6675ad82f4328", null ],
    [ "ItemQuantity", "d6/d31/a09379.html#a9eb87c575177738fde1778a7bdbc7168", null ],
    [ "ItemStatsElements", "d6/d31/a09379.html#a56f218ee196d8986a9860166c647637f", null ],
    [ "ItemTemperature", "d6/d31/a09379.html#ad58844540d2539d26eb6ba55c7d1a771", null ],
    [ "ItemWeight", "d6/d31/a09379.html#a4e6700e5e958e0a05ca2b7fb955efb18", null ],
    [ "ItemWetness", "d6/d31/a09379.html#a47fa0319b5dc58830a4c7a6ff151f624", null ],
    [ "StatText", "d6/d31/a09379.html#aa908d5e6d200a25caa712376d7f5131a", null ]
];