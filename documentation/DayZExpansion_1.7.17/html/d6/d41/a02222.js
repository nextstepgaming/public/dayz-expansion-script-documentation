var a02222 =
[
    [ "ExpansionQuestObjectiveType", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894af", [
      [ "NONE", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "TRAVEL", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa1890da0c78924de84c2b7581a91160a5", null ],
      [ "COLLECT", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afaab0d73b507a86099007b619c09617a5e", null ],
      [ "DELIVERY", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afaa3d86aeb99ea59e36fba09925c54513d", null ],
      [ "TREASUREHUNT", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa441dc945099adf3c20a93b77e2466765", null ],
      [ "AIPATROL", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa60af753c1c853cdd3e52ec5935fb2931", null ],
      [ "AICAMP", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afaf1038255df4ea5fefc02efbfcecc862e", null ],
      [ "AIVIP", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa81a02e1ec9cd1fb51b0ec77a7a072d4b", null ],
      [ "ACTION", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afae1bb1460cb779888412e634e983be161", null ],
      [ "CRAFTING", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa585db143a4a778e7091056664c6740cd", null ],
      [ "SCRIPTED", "d6/d41/a02222.html#aabce516118dec3a3b68bd2e23c8894afa55f0e453b9ae3e38ee1bd8d16c23535d", null ]
    ] ]
];