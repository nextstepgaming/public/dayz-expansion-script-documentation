var a04570 =
[
    [ "ErrorFalse", "d6/dfd/a04570.html#aa4ce627dc3b2c602c716932c6b5bd1df", null ],
    [ "GetFileName", "d6/dfd/a04570.html#ac380e56093e61e9a6960dd8979e27080", null ],
    [ "GetFileName", "d6/dfd/a04570.html#a01096aed0aead534faed401a551bc3a7", null ],
    [ "Restore", "d6/dfd/a04570.html#a425cf58ef58fb7e69421b186722edf4b", null ],
    [ "Restore_Phase1", "d6/dfd/a04570.html#a246d4934d35d0f5ab7b40bd724505636", null ],
    [ "Restore_Phase2", "d6/dfd/a04570.html#a775f55fb056bdc9c2caa292e43fd96b8", null ],
    [ "Restore_Phase3", "d6/dfd/a04570.html#a9fce61305b575c28e001367bea33021d", null ],
    [ "RestoreFromFile", "d6/dfd/a04570.html#a6e265c6167458c274a41c5707bf14e22", null ],
    [ "Save", "d6/dfd/a04570.html#a0dba38f48538c4b3ee66fb8cca041a57", null ],
    [ "Save_Phase1", "d6/dfd/a04570.html#afca706d5515c64240084d712a10dec6a", null ],
    [ "Save_Phase2", "d6/dfd/a04570.html#a0d38c3c24eee1fa89d085c4dc809007a", null ],
    [ "Save_Phase3", "d6/dfd/a04570.html#af334e4e4b6827fe9c439c50b5cda0c03", null ],
    [ "SaveToFile", "d6/dfd/a04570.html#a47337a5fc79f39dd8636b93662e9393a", null ],
    [ "SaveToFileAndReplace", "d6/dfd/a04570.html#a4de4944dfdca5ffae231c582532b7016", null ],
    [ "EXT", "d6/dfd/a04570.html#af68f5ed40a27a0d2790d5cdd5e25dc5b", null ],
    [ "FAILURE", "d6/dfd/a04570.html#abd545e754fd44d4c3d75a3dd40854cfd", null ],
    [ "s_StorageFolderPath", "d6/dfd/a04570.html#affc34abac9bc3f236e67601655b23556", null ],
    [ "SKIP", "d6/dfd/a04570.html#ab689594be9c8e708ac6d3c71446affda", null ],
    [ "SUCCESS", "d6/dfd/a04570.html#a5526f51cb0b45eabc69a5b4fc2f078e7", null ],
    [ "VERSION", "d6/dfd/a04570.html#adda80438adcdd3d33793a93840489f8e", null ]
];