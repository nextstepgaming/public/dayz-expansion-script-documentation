var a07182 =
[
    [ "ExpansionPlayerState", "d6/ded/a07182.html#af23ee8a41199e70564a86e31df146dad", null ],
    [ "AcquireFrom", "d6/ded/a07182.html#adac308b6ce3d6a5499ac8f1f3f077b21", null ],
    [ "ApplyTo", "d6/ded/a07182.html#af5cd1048c0104a05756119ab2cfb3653", null ],
    [ "m_Energy", "d6/ded/a07182.html#a822ee2dc18607f442149f0b7845f8a24", null ],
    [ "m_Health", "d6/ded/a07182.html#ae130172fcf8814fc0ae63aad1fd97ca9", null ],
    [ "m_HeatComfort", "d6/ded/a07182.html#a87cef58aba7c7f3265c258b4fa25c58d", null ],
    [ "m_Position", "d6/ded/a07182.html#aeb6b1c0a533669617e53821bca03f7c4", null ],
    [ "m_Stamina", "d6/ded/a07182.html#ac5560c3a21802e0179aad6fcaadf05dd", null ],
    [ "m_Water", "d6/ded/a07182.html#a65dc3f0f7bcc2874789eb21705c09456", null ],
    [ "m_Wetness", "d6/ded/a07182.html#af4566195465050d672ea6a7771bca071", null ]
];