var a00833 =
[
    [ "ExpansionMapMarkerType", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38", [
      [ "UNKNOWN", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38a6ce26a62afab55d7606ad4e92428b30c", null ],
      [ "PERSONAL", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38a3651cd2d93ef6384b4272197a3c031e7", null ],
      [ "PARTY", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38abb6f1d787307698ca002d5718bed6605", null ],
      [ "SERVER", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38a67c96b24b23bcb408bae7626730a04b7", null ],
      [ "PLAYER", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38ade5dc3e0dbd007d995ed3e37bde5ce7e", null ],
      [ "PARTY_QUICK", "dc/d6e/a00833.html#ad6df390a394237187d15a8c6c03f1f38a6af2b040851a7da858416405c66ec510", null ]
    ] ]
];