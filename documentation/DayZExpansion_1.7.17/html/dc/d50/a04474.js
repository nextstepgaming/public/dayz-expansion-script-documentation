var a04474 =
[
    [ "ExpansionSettingBase", "dc/d50/a04474.html#aa28659549a4963b743ed612f9de65d9d", null ],
    [ "~ExpansionSettingBase", "dc/d50/a04474.html#ae41ef9e6e0e5f8dca61c980be077583a", null ],
    [ "Copy", "dc/d50/a04474.html#a94a078402154b55ebee796629776ecfe", null ],
    [ "Defaults", "dc/d50/a04474.html#a9d3ac80f7dc4f5577bb1651bc7153a25", null ],
    [ "IsLoaded", "dc/d50/a04474.html#a8d356fb5e72b05b84c90400345d88286", null ],
    [ "Load", "dc/d50/a04474.html#aa2413220815a302a199400eb0a0188d7", null ],
    [ "MoveSettings", "dc/d50/a04474.html#a7aaa39a85a70d59a3773bfed3a55923f", null ],
    [ "OnLoad", "dc/d50/a04474.html#a3aed6bd52d610b59d7b94e3cd081a83b", null ],
    [ "OnRecieve", "dc/d50/a04474.html#acb05e5dc646314b2f581553e9bda8ff3", null ],
    [ "OnSave", "dc/d50/a04474.html#af60828b24959acd49f10e7f0e2d334ba", null ],
    [ "OnSend", "dc/d50/a04474.html#a2aed999982996895d90e70bff87271d3", null ],
    [ "Save", "dc/d50/a04474.html#a61d8e1b05f7a562841a0e9f037f80b51", null ],
    [ "Send", "dc/d50/a04474.html#afd43d06c0590d6179f6f668ddfc6195b", null ],
    [ "SettingName", "dc/d50/a04474.html#a5b8e2387d7a0c23ca9eb8b50a6d3f529", null ],
    [ "Unload", "dc/d50/a04474.html#af567e9cd8575d3e8619e78bc19b6b653", null ],
    [ "Update", "dc/d50/a04474.html#a519b6582560bae19163bc592942f8556", null ],
    [ "m_Version", "dc/d50/a04474.html#ad541ae0da305802229fdbe9e5cf0cc0d", null ]
];