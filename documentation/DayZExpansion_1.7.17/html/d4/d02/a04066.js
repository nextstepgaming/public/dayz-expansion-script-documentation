var a04066 =
[
    [ "ExpansionBookCraftingItem", "d4/d02/a04066.html#a36bf61939725c7e02b9fa38073e2d9c9", null ],
    [ "CanUseInRecipe", "d4/d02/a04066.html#aba56c3c8086a619229892bc59e3a0ded", null ],
    [ "CanUseInRecipe", "d4/d02/a04066.html#a50c0ad89bcea51f1bcadcd5cebc806d3", null ],
    [ "Format", "d4/d02/a04066.html#a7fffce37b682c3a0b92cefff720a3635", null ],
    [ "Amount", "d4/d02/a04066.html#a49d2f2548f5949de27af139d4860fc05", null ],
    [ "ClassNames", "d4/d02/a04066.html#ac13cf4ea0ed9041d9736b3834120e570", null ],
    [ "DisplayName", "d4/d02/a04066.html#a95a3c4b08cad6ab38e4e6b0472b07a27", null ],
    [ "m_CanUseInRecipe", "d4/d02/a04066.html#a955f06b13f486b908356b7399ea43937", null ],
    [ "m_PlayerItem", "d4/d02/a04066.html#a7612985a7aae106e650ecfecf25e2b78", null ]
];