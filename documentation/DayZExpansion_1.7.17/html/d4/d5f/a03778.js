var a03778 =
[
    [ "bldr_Spotlight", "d4/d5f/a03778.html#a6cf205ca036cdfbe128be105d8ff0091", null ],
    [ "CanLoadItemIntoCargo", "d4/d5f/a03778.html#aa3c9fa22abad8ddc4eb8634a4909f807", null ],
    [ "CanPutIntoHands", "d4/d5f/a03778.html#a910e4ca34c92778d75d719b8447063d6", null ],
    [ "CanReceiveItemIntoHands", "d4/d5f/a03778.html#a3c6ad562605f285494eeb5cf886757bf", null ],
    [ "CanReleaseFromHands", "d4/d5f/a03778.html#a27f49ac8c897a35effa26e8c11a3f214", null ],
    [ "CanRemoveFromHands", "d4/d5f/a03778.html#acdb5d4e2839309e18e8795101982958f", null ],
    [ "CreateLight", "d4/d5f/a03778.html#a81a294c5a6a8e360fd4623b65b8c975d", null ],
    [ "ExpansionCanRecievePower", "d4/d5f/a03778.html#a993a9e0c5d36ba0bd4d8e92f7d7a41a6", null ],
    [ "IsTakeable", "d4/d5f/a03778.html#aa566ccbe9ea4c649c7ed5413d088b77a", null ],
    [ "OnEnergyConsumed", "d4/d5f/a03778.html#a9954094d92c53c508fb27e84b2f072da", null ],
    [ "UpdateAllSelections", "d4/d5f/a03778.html#ac5909240071b9a703d9d2cf31675c35a", null ],
    [ "m_LightProp", "d4/d5f/a03778.html#a5dbb4bd228c3d37b5330b4f58b0c5ef6", null ]
];