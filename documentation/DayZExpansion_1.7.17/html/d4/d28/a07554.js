var a07554 =
[
    [ "ExpansionVehicleCarEngine", "d4/d28/a07554.html#a956b90558018d3e878b2bfdc4f70f509", null ],
    [ "CalculateTorque", "d4/d28/a07554.html#a2185c09df45f01533dd9cee879ba055b", null ],
    [ "m_PowerMax", "d4/d28/a07554.html#aa7c171abf5899728ebd253074832419d", null ],
    [ "m_PowerRPM", "d4/d28/a07554.html#a5d0001ead64cfd99f367671e3bc6ec74", null ],
    [ "m_Steepness", "d4/d28/a07554.html#ae7bf18d8cb8f7e8edf62db84cb4f913a", null ],
    [ "m_TorqueMax", "d4/d28/a07554.html#a10e271e9741a54685ed7b6cac19e12ac", null ],
    [ "m_TorqueRPM", "d4/d28/a07554.html#a6e873e4fe3cae29efce4216a5095d577", null ]
];