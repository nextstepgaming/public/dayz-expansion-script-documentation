var a00704 =
[
    [ "ExpansionChatSettingsBase", "d1/da4/a04242.html", "d1/da4/a04242" ],
    [ "ExpansionChatSettings", "d1/d5a/a04246.html", "d1/d5a/a04246" ],
    [ "AdminChatColor", "d4/d6a/a00704.html#aca017c0e25edc3161d2ebd1c8a98a09f", null ],
    [ "DirectChatColor", "d4/d6a/a00704.html#aa578e2742d97a90be87e35a7ed265b10", null ],
    [ "EnableGlobalChat", "d4/d6a/a00704.html#aacf768f5738a580c7aa1604dcca74b9f", null ],
    [ "EnableTransportChat", "d4/d6a/a00704.html#a9ac85a6fe03a5ee2a389557e2c38fbd4", null ],
    [ "GlobalChatColor", "d4/d6a/a00704.html#af0e2bdf3db041194c203c90a21780ee4", null ],
    [ "SystemChatColor", "d4/d6a/a00704.html#ace351867ec2c78c2dbe5f78826adf221", null ],
    [ "TransmitterChatColor", "d4/d6a/a00704.html#a9ea1d9f0e4cb2f30585b171ed1948e65", null ],
    [ "TransportChatColor", "d4/d6a/a00704.html#ae5e5ba30865e08782996624071dc6812", null ]
];