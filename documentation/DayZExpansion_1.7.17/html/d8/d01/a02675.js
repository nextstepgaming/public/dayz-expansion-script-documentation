var a02675 =
[
    [ "CfgPatches", "d8/d01/a02675.html#dd/dd2/a07090", [
      [ "DayZExpansion_Sounds_Common", "d5/d2f/a07094.html", [
        [ "requiredAddons", "d5/d2f/a07094.html#ae6e3924fb332bd8ab6ec7613d3d47d34", null ],
        [ "requiredVersion", "d5/d2f/a07094.html#ab65e62b98cae558cefb5cc2dcd3e2a12", null ],
        [ "units", "d5/d2f/a07094.html#a604cc3db68f62ffbf13a71403deb99c1", null ],
        [ "weapons", "d5/d2f/a07094.html#a9680ee77b82448e42d1b96dab4182856", null ]
      ] ]
    ] ],
    [ "CfgPatches::DayZExpansion_Sounds_Common", "d5/d2f/a07094.html", "d5/d2f/a07094" ],
    [ "CfgSoundShaders", "d8/d01/a02675.html#db/d4a/a07098", [
      [ "Expansion_Car_Lock_SoundShader", "d8/dc2/a07126.html", [
        [ "samples", "d8/dc2/a07126.html#abbbee4566dc4644525adb06a3a20f8e8", null ],
        [ "volume", "d8/dc2/a07126.html#a3c035bb49b0fb3df3992408b57a1c01c", null ]
      ] ],
      [ "Expansion_CarLock_SoundShader", "d6/d43/a07122.html", [
        [ "range", "d6/d43/a07122.html#a9d96b8f079816c156f810f7a0ded853b", null ]
      ] ],
      [ "Expansion_Horn_Ext_SoundShader", "dc/da2/a07106.html", [
        [ "samples", "dc/da2/a07106.html#a952b73b6e0dcf997016bedd86b80e7e3", null ],
        [ "volume", "dc/da2/a07106.html#a815384326671b3959726f38d58960be3", null ]
      ] ],
      [ "Expansion_Horn_Int_SoundShader", "da/d25/a07110.html", [
        [ "samples", "da/d25/a07110.html#a8cc388111022a32c90ac6ffdf4b71f95", null ],
        [ "volume", "da/d25/a07110.html#aac326c64fca45cce42a6f005da97ee7d", null ]
      ] ],
      [ "Expansion_Horn_SoundShader", "d3/dd9/a07102.html", [
        [ "range", "d3/dd9/a07102.html#a318e376b39cd19e4528d4417a1559281", null ]
      ] ],
      [ "Expansion_Truck_Horn_Ext_SoundShader", "d4/dff/a07114.html", [
        [ "samples", "d4/dff/a07114.html#ac761f1d71ac65cb9c0cd01746f1213cf", null ],
        [ "volume", "d4/dff/a07114.html#a8e55dda4bc37f43cccd8eb1af150f6c9", null ]
      ] ],
      [ "Expansion_Truck_Horn_Int_SoundShader", "df/df1/a07118.html", [
        [ "samples", "df/df1/a07118.html#ae4a567088f27b7c5f8113d094292d99a", null ],
        [ "volume", "df/df1/a07118.html#abfbd965432a67707d4b383a88d8221f3", null ]
      ] ]
    ] ],
    [ "CfgSoundShaders::Expansion_Horn_SoundShader", "d3/dd9/a07102.html", "d3/dd9/a07102" ],
    [ "CfgSoundShaders::Expansion_Horn_Ext_SoundShader", "dc/da2/a07106.html", "dc/da2/a07106" ],
    [ "CfgSoundShaders::Expansion_Horn_Int_SoundShader", "da/d25/a07110.html", "da/d25/a07110" ],
    [ "CfgSoundShaders::Expansion_Truck_Horn_Ext_SoundShader", "d4/dff/a07114.html", "d4/dff/a07114" ],
    [ "CfgSoundShaders::Expansion_Truck_Horn_Int_SoundShader", "df/df1/a07118.html", "df/df1/a07118" ],
    [ "CfgSoundShaders::Expansion_CarLock_SoundShader", "d6/d43/a07122.html", "d6/d43/a07122" ],
    [ "CfgSoundShaders::Expansion_Car_Lock_SoundShader", "d8/dc2/a07126.html", "d8/dc2/a07126" ],
    [ "CfgSoundSets", "d8/d01/a02675.html#d1/d1a/a07130", [
      [ "Expansion_Car_Lock_SoundSet", "d1/d77/a07154.html", [
        [ "soundShaders", "d1/d77/a07154.html#aefa2635444b6fa023c6f945081a0a27d", null ],
        [ "volumeFactor", "d1/d77/a07154.html#a2e51b40052eb4de3cb8ad8ec777fcf1b", null ]
      ] ],
      [ "Expansion_Horn_Ext_SoundSet", "db/dcc/a07138.html", [
        [ "soundShaders", "db/dcc/a07138.html#a7635af162c482f6adefafe72761fd24c", null ],
        [ "volumeFactor", "db/dcc/a07138.html#acbafb41fb619f64a5edee49afcc3b93f", null ]
      ] ],
      [ "Expansion_Horn_Int_SoundSet", "d9/d5f/a07142.html", [
        [ "soundShaders", "d9/d5f/a07142.html#a5013e670ae740b916fee5809a5fe1d01", null ],
        [ "volumeFactor", "d9/d5f/a07142.html#a28ec358eba1b668a9ae93305782bcab8", null ]
      ] ],
      [ "Expansion_Horn_SoundSet", "d7/d57/a07134.html", [
        [ "distanceFilter", "d7/d57/a07134.html#a441bce50a6d67f7b20f16a039718db69", null ],
        [ "loop", "d7/d57/a07134.html#ad7e7518dcdaae245f4070a29cffc77d9", null ],
        [ "sound3DProcessingType", "d7/d57/a07134.html#a0f408b25ab5c51ef5d65caeed872a0c1", null ],
        [ "spatial", "d7/d57/a07134.html#a68f2c76a0e08b84755f24f3a45bd7553", null ],
        [ "volumeCurve", "d7/d57/a07134.html#a2fc5f84e229aa5fab0d740ff6cd005bd", null ],
        [ "volumeFactor", "d7/d57/a07134.html#a1b44204878798af2cf699725d7645654", null ]
      ] ],
      [ "Expansion_Truck_Horn_Ext_SoundSet", "de/d85/a07146.html", [
        [ "soundShaders", "de/d85/a07146.html#abeade9b174d04512270273a8a667d810", null ],
        [ "volumeFactor", "de/d85/a07146.html#a2827968068127d4f7c7d6767ba3b9780", null ]
      ] ],
      [ "Expansion_Truck_Horn_Int_SoundSet", "df/ddd/a07150.html", [
        [ "soundShaders", "df/ddd/a07150.html#a2d8f69f1d5cb3933655793b9c055798d", null ],
        [ "volumeFactor", "df/ddd/a07150.html#ad916f342c7c493d6511ce2f4d8b4681e", null ]
      ] ]
    ] ],
    [ "CfgSoundSets::Expansion_Horn_SoundSet", "d7/d57/a07134.html", "d7/d57/a07134" ],
    [ "CfgSoundSets::Expansion_Horn_Ext_SoundSet", "db/dcc/a07138.html", "db/dcc/a07138" ],
    [ "CfgSoundSets::Expansion_Horn_Int_SoundSet", "d9/d5f/a07142.html", "d9/d5f/a07142" ],
    [ "CfgSoundSets::Expansion_Truck_Horn_Ext_SoundSet", "de/d85/a07146.html", "de/d85/a07146" ],
    [ "CfgSoundSets::Expansion_Truck_Horn_Int_SoundSet", "df/ddd/a07150.html", "df/ddd/a07150" ],
    [ "CfgSoundSets::Expansion_Car_Lock_SoundSet", "d1/d77/a07154.html", "d1/d77/a07154" ]
];