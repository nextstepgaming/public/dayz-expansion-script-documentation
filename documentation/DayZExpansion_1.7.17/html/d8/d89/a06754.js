var a06754 =
[
    [ "CopyConfig", "d8/d89/a06754.html#a3ff35ed923a7ad55b336c66863356e41", null ],
    [ "GetMarkerName", "d8/d89/a06754.html#a34dd07d687b5796561a6e2be7cbd5a26", null ],
    [ "GetMaxDistance", "d8/d89/a06754.html#a3d101f690fa1ba49d4be935ec2c9be8f", null ],
    [ "GetPosition", "d8/d89/a06754.html#a58ec2057c078bcfb2ad10e9b0d42c653", null ],
    [ "Load", "d8/d89/a06754.html#a2783751081252ed603797b4bd5fb4a38", null ],
    [ "OnRecieve", "d8/d89/a06754.html#a3deb30899c0e3e77d12972a74df18f47", null ],
    [ "OnSend", "d8/d89/a06754.html#a2bf288caf47ad8095f0483614f3792bc", null ],
    [ "Save", "d8/d89/a06754.html#ac6e14cb6756c19a074a8431e5d08827a", null ],
    [ "SetMarkerName", "d8/d89/a06754.html#a2bcbd420c6c9f7c7e60a5fa09b7e2e1d", null ],
    [ "SetMaxDistance", "d8/d89/a06754.html#a82bd9b908d307ba4f020b5cbc01bea3f", null ],
    [ "SetPosition", "d8/d89/a06754.html#a219cc0eddbef2852d2ec3fd702f38ece", null ],
    [ "ShowDistance", "d8/d89/a06754.html#a16e9e791d52be0c1da3f565b9c68b419", null ],
    [ "ShowDistance", "d8/d89/a06754.html#a7666adaf0ad78725e34d8c36589bb8db", null ]
];