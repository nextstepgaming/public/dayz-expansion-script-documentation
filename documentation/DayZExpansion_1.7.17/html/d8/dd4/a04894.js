var a04894 =
[
    [ "Weapon_Base", "d8/dd4/a04894.html#a37be549d4bb2e6c2e4e9112f756dea83", null ],
    [ "CalculateBarrelLength", "d8/dd4/a04894.html#a125c1b3da919ffb14b7f69563cc69c34", null ],
    [ "EOnSimulate", "d8/dd4/a04894.html#a623feb8dade142dda78c3761b460e400", null ],
    [ "Expansion_HasAmmo", "d8/dd4/a04894.html#a7fee48bede708d806c31b8b4e48253e7", null ],
    [ "Expansion_IsChambered", "d8/dd4/a04894.html#a11fe0f2f0f63f5487e5446525cdec53a", null ],
    [ "ExpansionFire", "d8/dd4/a04894.html#ab351910f4d3f74bf7a6cefabc9d543f7", null ],
    [ "ExpansionGetMagAttachedFSMStateID", "d8/dd4/a04894.html#a25237554229a3359715eb5b4fd59598c", null ],
    [ "ExpansionGetMagAttachedFSMStateID", "d8/dd4/a04894.html#a25237554229a3359715eb5b4fd59598c", null ],
    [ "ExpansionHideWeaponPart", "d8/dd4/a04894.html#a86b2b3f7208858b319c8a6777b816b11", null ],
    [ "ExpansionSetNextFire", "d8/dd4/a04894.html#a5ff0625594e5a74482275917be1eca30", null ],
    [ "GetExpansionFireType", "d8/dd4/a04894.html#a638ffdd6be32d5b4b04fbd6eefdf0835", null ],
    [ "GetFirePosition", "d8/dd4/a04894.html#afee9a641cacb736514a3a88951a71f33", null ],
    [ "OnInventoryExit", "d8/dd4/a04894.html#a42db3c74cbc34de967eb791cdffccfd8", null ],
    [ "SetActions", "d8/dd4/a04894.html#a836aa8b7b6c068918c1010089b817a2e", null ],
    [ "UpdateLaser", "d8/dd4/a04894.html#abe831245f5684243f71d4919904d5c14", null ],
    [ "m_ExMuzzleIndices", "d8/dd4/a04894.html#a26836141e450f6cfca413649e244879f", null ],
    [ "m_ExShouldFire", "d8/dd4/a04894.html#ad71c244166581ff05cef32bdc6296697", null ]
];