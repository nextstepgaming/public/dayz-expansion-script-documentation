var a00932 =
[
    [ "ExpansionSafeZoneSettingsBase", "d7/dba/a04502.html", "d7/dba/a04502" ],
    [ "ExpansionSafeZoneSettings", "d8/d4c/a04506.html", "d8/d4c/a04506" ],
    [ "CircleZones", "d8/dbb/a00932.html#a80f12b968e61dc48ca4488fde825bd97", null ],
    [ "Enabled", "d8/dbb/a00932.html#a558f5c44426d0eb7abb82a65e8892d9a", null ],
    [ "EnableVehicleinvincibleInsideSafeZone", "d8/dbb/a00932.html#a4a24cd99d951245c0e4c49551c1d7418", null ],
    [ "FrameRateCheckSafeZoneInMs", "d8/dbb/a00932.html#a63fa878491bbb94f0bb5d95f02735cb2", null ],
    [ "PolygonZones", "d8/dbb/a00932.html#a95f36a14026d170362326ba1c56e4444", null ]
];