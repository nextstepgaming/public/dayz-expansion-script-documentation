var a06414 =
[
    [ "ExpansionPlayerListEntry", "d8/db0/a06414.html#ab6ee2ed773055a6c1dae590b8e41080a", null ],
    [ "~ExpansionPlayerListEntry", "d8/db0/a06414.html#a62094faa7311453099cd85d9100de60b", null ],
    [ "GetControllerType", "d8/db0/a06414.html#adc2c153538f97eeaa4d257c14cffee4c", null ],
    [ "GetLayoutFile", "d8/db0/a06414.html#aea5a0df1ddca30fec3408f8eb132ab31", null ],
    [ "OnMouseEnter", "d8/db0/a06414.html#a30c287d647d2009115f14fb8939f5e6b", null ],
    [ "OnMouseLeave", "d8/db0/a06414.html#a6f1b42a37e87b519e2621731ec25362a", null ],
    [ "SetID", "d8/db0/a06414.html#ab1dcb6483d5b2ebe68982b414012a30d", null ],
    [ "SetName", "d8/db0/a06414.html#a6f28abb9f5b7bcb7865d1a4099d4a114", null ],
    [ "m_EntryController", "d8/db0/a06414.html#adfdbc722c028f00e7599564fa30a396a", null ],
    [ "m_Name", "d8/db0/a06414.html#a29f0fff1de0d41ae2cc33d56c18327a3", null ],
    [ "m_PlayerID", "d8/db0/a06414.html#a62e79e2022d38dd1b778cd9aed494b5f", null ],
    [ "m_PlayerPlainID", "d8/db0/a06414.html#a9e01a2a9b15ff136cceed0c23943ea32", null ],
    [ "m_Tooltip", "d8/db0/a06414.html#ac996f33afe7a64abab3e62f6a64ccfb4", null ],
    [ "player_icon", "d8/db0/a06414.html#a414c922038446410c76724d19bc409d3", null ],
    [ "player_name", "d8/db0/a06414.html#a1f6515ae5366a21830bc1dd209adcc18", null ]
];