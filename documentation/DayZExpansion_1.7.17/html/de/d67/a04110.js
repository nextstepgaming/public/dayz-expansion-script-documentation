var a04110 =
[
    [ "ExpansionBookMenuTabNotes", "de/d67/a04110.html#afd661e227cabd05468786dc3f9afa03b", null ],
    [ "CanShow", "de/d67/a04110.html#ae3457b90febfe56ad3b3a4f3a7f855a8", null ],
    [ "GetControllerType", "de/d67/a04110.html#a06a5e8d8cbaec450004d53bc6847a6ba", null ],
    [ "GetLayoutFile", "de/d67/a04110.html#a9bed6b4b4ef5df1312644e07f9061047", null ],
    [ "GetTabColor", "de/d67/a04110.html#a8e610a0107222c7f10e7805f880efff9", null ],
    [ "GetTabIconName", "de/d67/a04110.html#aa83ee8136b3520423326ab4364a4e589", null ],
    [ "GetTabName", "de/d67/a04110.html#aa3e4f5e93d8039116e221651b0811dfe", null ],
    [ "IgnoreBackButtonBase", "de/d67/a04110.html#ae92de5966a5b2130c0a69cdcee133a74", null ],
    [ "IsParentTab", "de/d67/a04110.html#ad8f7d24b92632ad273cc5ec834e6a914", null ],
    [ "OnBackButtonClick", "de/d67/a04110.html#a9edd1f00a95fa1732dc7852f30703bd2", null ],
    [ "OnHide", "de/d67/a04110.html#a827024c229b8262abb96d447c0fc1b97", null ],
    [ "OnMouseEnter", "de/d67/a04110.html#affafcd9e4634cc0347c2f0a6dcd09823", null ],
    [ "OnMouseLeave", "de/d67/a04110.html#a77fff6264dd5fd37948bdb4d0d84c638", null ],
    [ "OnShow", "de/d67/a04110.html#ae6dc2b29b2b665b5525ad14f67fd0b12", null ],
    [ "SetView", "de/d67/a04110.html#a1745568b31eef282f2a750617565f52b", null ],
    [ "m_NotesTabController", "de/d67/a04110.html#a0caceaf49ec326c29bd5004e9bc5df41", null ]
];