var a06154 =
[
    [ "AddAttachment", "de/d33/a06154.html#a058b0494700bef360eef6b32b27d10d9", null ],
    [ "GetAttachments", "de/d33/a06154.html#a54bd92a89b4c6bed09cc403bf7c2cbd2", null ],
    [ "LoadItemPreset", "de/d33/a06154.html#a889e0199e4b82c23d6253486d99d204f", null ],
    [ "RemoveAttachment", "de/d33/a06154.html#afa8046e43432b1298708b2966f1c8c55", null ],
    [ "SaveItemPreset", "de/d33/a06154.html#a426dfb34f544ce8b6b682bef708f80f5", null ],
    [ "ClassName", "de/d33/a06154.html#aa5d6b5634d5c002845aaedfbab12067c", null ],
    [ "ItemAttachments", "de/d33/a06154.html#ad78693d77a3fc3c756be1aeb02df13b7", null ],
    [ "PresetName", "de/d33/a06154.html#a6fc49c85cd587971b61a367e18ac85e5", null ]
];