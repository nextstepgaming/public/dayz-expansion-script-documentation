var a06890 =
[
    [ "Copy", "de/d9b/a06890.html#a950caa9842d6c294eef9cb2b44977b6b", null ],
    [ "CopyInternal", "de/d9b/a06890.html#a9272db1336f7b5fde1d35d28966bc438", null ],
    [ "CopyInternal", "de/d9b/a06890.html#aa04348d20083432068fa55395103d9d2", null ],
    [ "DefaultNewsFeedLinks", "de/d9b/a06890.html#af0a4b93c3329773ad3c66117124c265e", null ],
    [ "DefaultNewsFeedTexts", "de/d9b/a06890.html#a5793076d50e0ed752f1118fb57ef304c", null ],
    [ "Defaults", "de/d9b/a06890.html#ade11400f53e2d511580da12b2e8ae5f0", null ],
    [ "IsLoaded", "de/d9b/a06890.html#a7467a4327feb2d0cba850e3e3ed8af4d", null ],
    [ "OnLoad", "de/d9b/a06890.html#a27f1b78e62674127aac2187f91353eed", null ],
    [ "OnRecieve", "de/d9b/a06890.html#ac84b6ac947d1f7020806b9e01c43c4f1", null ],
    [ "OnSave", "de/d9b/a06890.html#a3c0bfa3a4aebae811f5d3956b8eddfd9", null ],
    [ "OnSend", "de/d9b/a06890.html#ad036f436354d2e20a23bef23ad00b519", null ],
    [ "Send", "de/d9b/a06890.html#aa8735d42adf2ea4768f09f077502bdd1", null ],
    [ "SettingName", "de/d9b/a06890.html#a21a9f957c6f80b91a104688bef70d51e", null ],
    [ "Unload", "de/d9b/a06890.html#ae7cdbd01c0b738ab34e73589f22ffa45", null ],
    [ "Update", "de/d9b/a06890.html#a279f179ed9f61459644c5589222dcfce", null ],
    [ "m_IsLoaded", "de/d9b/a06890.html#a91505540c962a0fd4c91e2eb28fd4970", null ],
    [ "VERSION", "de/d9b/a06890.html#a3c1bd69af770f23e3e3e02f67bbef097", null ]
];