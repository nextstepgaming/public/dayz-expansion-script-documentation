var a08872 =
[
    [ "ExpansionActionDamageBaseBuildingCB", "df/df2/a03638.html", "df/df2/a03638" ],
    [ "ActionCondition", "d7/d5c/a08872.html#aa9aa4864fae11769c65e644b5fd97c66", null ],
    [ "CreateActionComponent", "d7/d5c/a08872.html#a31cb402a575585bac90f7e97ee5066b1", null ],
    [ "CreateConditionComponents", "d7/d5c/a08872.html#a6b922d3bb1d26f790bc52030566ba2df", null ],
    [ "DestroyCondition", "d7/d5c/a08872.html#ab5976173dbe840aa82fce09e0e8680a6", null ],
    [ "ExpansionActionDamageBaseBuilding", "d7/d5c/a08872.html#ab6a9e7bc8789cddeac2272661a9f7da6", null ],
    [ "GetAdminLogMessage", "d7/d5c/a08872.html#a5cf7c0a3bd1ed2154d3b917244047a40", null ],
    [ "GetText", "d7/d5c/a08872.html#af127b91bedeb70f79583b0183d86be98", null ],
    [ "OnFinishProgressServer", "d7/d5c/a08872.html#a00cda41fe9dd093059da5c4b83b591bc", null ]
];