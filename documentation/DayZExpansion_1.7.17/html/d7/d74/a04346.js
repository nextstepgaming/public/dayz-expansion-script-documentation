var a04346 =
[
    [ "Acquire", "d7/d74/a04346.html#a1edfe07d082a2def7db3a5b2c48f77ec", null ],
    [ "IDToHex", "d7/d74/a04346.html#aa8b9403c52f4fb018777a5abb432e4f9", null ],
    [ "IDToString", "d7/d74/a04346.html#aaf2177fa1c4579c1a7433881558d1c10", null ],
    [ "IncrementNext", "d7/d74/a04346.html#ad0c80f51211efce21a325a6c94f57980", null ],
    [ "IsEqual", "d7/d74/a04346.html#aef2cb60078aa028a26a0185595a8d577", null ],
    [ "IsEqual", "d7/d74/a04346.html#a214be93e086c4a5a38abeafb18377988", null ],
    [ "IsZero", "d7/d74/a04346.html#a6f84962535ed8b1c856e68a97cfdb063", null ],
    [ "ResetNext", "d7/d74/a04346.html#a2eef723e5d83e2da4b63926083d85b4f", null ],
    [ "Set", "d7/d74/a04346.html#a5a56a9e38c2d8007cab62c7587725656", null ],
    [ "UpdateNext", "d7/d74/a04346.html#abb7381ee239fffcb684e8e1b533349cf", null ],
    [ "m_ID", "d7/d74/a04346.html#a401d2f95cf1e71f052a82eed6daa1fda", null ],
    [ "m_IsSet", "d7/d74/a04346.html#a3d28b5080f8d8f40355c1394ba7541ea", null ],
    [ "s_NextID", "d7/d74/a04346.html#a6d010763b47f13081d10803850b3bf7b", null ]
];