var a04482 =
[
    [ "ExpansionMonitoringSettings", "d7/d2e/a04482.html#a29e108148cf5c1842e568bd4ddd3fb73", null ],
    [ "Copy", "d7/d2e/a04482.html#afba882bcfd162346b05ba738c79a252a", null ],
    [ "CopyInternal", "d7/d2e/a04482.html#a5e6221ad75c69a18931f2c1014967f45", null ],
    [ "Defaults", "d7/d2e/a04482.html#a964ea7ff3e1ca8605a78207c54fdd554", null ],
    [ "IsLoaded", "d7/d2e/a04482.html#a628452547638bd4a1dd48336a85cbb90", null ],
    [ "OnLoad", "d7/d2e/a04482.html#a447a7bb3984622611a6460ad583e63d2", null ],
    [ "OnRecieve", "d7/d2e/a04482.html#a08c1683587fb074c50ad708fcec89876", null ],
    [ "OnSave", "d7/d2e/a04482.html#a8592db30f45236962cdb505af349ba6b", null ],
    [ "OnSend", "d7/d2e/a04482.html#a29fd734b250728bf002615e388131e16", null ],
    [ "Send", "d7/d2e/a04482.html#a18d5d39b102387115bc6e91df1296c9a", null ],
    [ "SettingName", "d7/d2e/a04482.html#a5b407b6715b64c8d701346fb0448ff35", null ],
    [ "Unload", "d7/d2e/a04482.html#a875703a0fcf7e2dc573e6a1e56b5e70a", null ],
    [ "Update", "d7/d2e/a04482.html#a2ee521d3f3fb26fa853fed812bbd7ad8", null ],
    [ "Enabled", "d7/d2e/a04482.html#aaea99aaa8823dd1a07e38afbd11732c4", null ],
    [ "m_IsLoaded", "d7/d2e/a04482.html#ac706dac4442d3b84857e8f7412855e4a", null ],
    [ "VERSION", "d7/d2e/a04482.html#a7d71c584e53f68c306ac08a6715f3e2c", null ]
];