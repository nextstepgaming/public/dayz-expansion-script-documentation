var a06874 =
[
    [ "Copy", "d7/dfe/a06874.html#a1ea51439cfcf2d0c6f2871492194586f", null ],
    [ "CopyInternal", "d7/dfe/a06874.html#a6cebc54cb7470b6d06f73606732c47dc", null ],
    [ "Defaults", "d7/dfe/a06874.html#a77cf7cc54e853a79394fc1067b4e41fb", null ],
    [ "IsLoaded", "d7/dfe/a06874.html#aac38f2b984f6c7c05808896a9e8e19ae", null ],
    [ "OnLoad", "d7/dfe/a06874.html#a2a355a835e8461ddfd3e56f996f6d90e", null ],
    [ "OnRecieve", "d7/dfe/a06874.html#a2b9b5d1cd89470d39db8cb88add90bf2", null ],
    [ "OnSave", "d7/dfe/a06874.html#aa321f322dc52282d34bf091e7167b56b", null ],
    [ "OnSend", "d7/dfe/a06874.html#a741e1c0911becfadde72a495cd97f0c0", null ],
    [ "Send", "d7/dfe/a06874.html#a91aac2c30b9c7d6509e27ed0ae6adbf0", null ],
    [ "SettingName", "d7/dfe/a06874.html#a6289a33f635ab37286e877692a8eb8fd", null ],
    [ "Unload", "d7/dfe/a06874.html#a4b31225ba4604e1ef8eadde19f4b69ca", null ],
    [ "Update", "d7/dfe/a06874.html#a15807bef965e270f5930499ba8f2272f", null ],
    [ "EnablePlayerList", "d7/dfe/a06874.html#aceb77326668dacd54ceba8fbc017d967", null ],
    [ "EnableTooltip", "d7/dfe/a06874.html#ab01d7e3d7f16a30b71293a7fac334ca5", null ],
    [ "m_IsLoaded", "d7/dfe/a06874.html#acd0a93b2372b92ba67fed9ffa3eb0e8c", null ],
    [ "VERSION", "d7/dfe/a06874.html#acf873799fb2278d59f66dc6adb7a51d4", null ]
];