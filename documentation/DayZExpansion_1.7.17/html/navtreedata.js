/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "DayZExpansion", "index.html", [
    [ "DayZExpansion Script Documentation", "index.html", null ],
    [ "Deprecated List", "d8/d01/a03458.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/d3b/a09271_source.html",
"d0/dad/a04630.html#a9ee32140865ba4bd5688850022f80376",
"d0/dde/a02978.html#a59c95d426f41e0f49a0fcf8d527c1164adc0f401e772f1affa81071e70631abbf",
"d1/d0b/a04890.html#abdafeb073af3e76c3762bd11b5d749b6",
"d1/d47/a02300.html#aeffed4440eb116932532b96fdb26663e",
"d1/d8f/a04202.html#a057968299e7a0c122ac1d46dabd03d4a",
"d1/d9c/a03790.html#aa19f46f260831278cc4c80d97229e779",
"d1/db8/a05450.html#a9528606f0accbfaa3e4fdad395d9a1ba",
"d1/de1/a02435.html#ab6eeaf19420a33c14f721090e9ada3a6a350a0b7b579cdbb91949630e0f9fcf2d",
"d2/d41/a01037.html",
"d2/d74/a05318.html#a5e9021de62baac59fd83421e46772f2e",
"d2/dc1/a03994.html#acf3b506515841a16b08c42af43f01489",
"d3/d0f/a09109_source.html",
"d3/d45/a07086.html",
"d3/d6e/a03570.html#afa4dfe5822f88b46142579a3c423a5dc",
"d3/d99/a02942.html#a448b5c4bde7dc6de8026f86d208bc736a6b4af979c9694e48f340397ac08dfd1c",
"d3/de5/a00956.html#a91fff2923c73524aa9ddd1aa0e5c0337aa3a9adacc85c180950255762543c88ac",
"d4/d24/a06742.html#aa568397c334b69f9b7b0143f6b9f9df6",
"d4/d5f/a03778.html#a81a294c5a6a8e360fd4623b65b8c975d",
"d4/d9d/a02681_source.html",
"d4/dcb/a06638.html#a99d5e532b8c17fc2545ab9d4b87fd12a",
"d5/d25/a02687.html#a964281d8f762740336b8f313ac1d0d3e",
"d5/d43/a05406.html#a91a8e8d09cd89023d2de9f5ec390176b",
"d5/d8c/a06438.html#adb1626767f861d12686e8a0f09af6794",
"d5/dd9/a04150.html#a9d2d8059fd37c8792171140bb77dec8d",
"d6/d15/a01697.html",
"d6/d6e/a02174.html#aad33a6b2b4a3fffc6fb9bf46b045fce2",
"d6/d82/a00920.html#ae3e27fdaa6ddb0d424626c1f0b3aad2e",
"d6/db4/a04266.html",
"d6/ded/a03942.html#a851bf2a2912d4b77701ba7497edb888e",
"d7/d1e/a04382.html#a5a055a2860ea476415330c15410dd711",
"d7/d54/a04666.html#a386ac5c35fc67a66b02890cafefa9a0d",
"d7/d82/a03774.html#a0f42df497e5e03153b7863b65754478d",
"d7/d82/a03774.html#a5e940243b6723319fba6925cd48b6952",
"d7/d82/a03774.html#ab25ae69addd27f6c73194dd9cae125fa",
"d7/d82/a03774.html#afe8590ded705bb6676f24d6e8cfcfcbf",
"d7/dc9/a04370.html#a0ee29c0c7e8a4cf2785e5a5537c05d24",
"d8/d13/a09406.html",
"d8/d2f/a06122.html#a7c44c32942040f81eee0dccaa39d238c",
"d8/d61/a06642.html#a943816b1896e5c910ea904e9520dfece",
"d8/d93/a04174.html#a0906b5590a570e2db9ba666147e52939",
"d8/ddf/a01238.html",
"d9/d41/a07174.html#a3c0dccea6626711ceb30c929be8ef041",
"d9/d6f/a01445.html#a31e96dd8760e75548103aa24a3f3219fad521b66900d38236c68f006a96919e9f",
"d9/da4/a02798.html",
"d9/df0/a03782.html#aa70f25021edb3e8f1c3e32f0d993a2bb",
"da/d03/a07750.html#ad72125b1e4208ad9c3a7475645264be3",
"da/d28/a09115.html#a7df9f1a355eccdcfd78830e38a431d0f",
"da/d4d/a05322.html#aee372d129578920ba2d50802778ffb18",
"da/d7e/a04326.html#ad88fd72229089a627468e5e8f6816f4b",
"da/d96/a00782.html#a571856d0ec60fcf3403358046928fca4a185b37ea14b5b27cb4e897d5bcc6a024",
"da/dbe/a02084_source.html",
"db/d1c/a01229_source.html",
"db/d70/a06410.html#a0bf30522a5aaf9307deab6a64adf543e",
"db/dc8/a05314.html#a55da7190dc5fc8f995a6591f7fba919e",
"dc/d1c/a06350.html#a8d87702f39ffd82bfa7a95f2369c56e5",
"dc/d36/a06074.html#ac9b065234d32451d8bf3aaf2e7ebedd3",
"dc/d7b/a03654.html#a4a0a6a16b0477f44eedc4ac0b4633735",
"dc/dae/a00563.html",
"dc/ddd/a05170.html#aba1ebc7053a8cc113e541702e4098d05",
"dc/df9/a06354.html#a8899cce4eb5cc9125d45e60d2c83f546",
"dd/d32/a00041.html#a3578afd97750bc428c2737e242dffd3d",
"dd/d7d/a06202.html",
"dd/daf/a04050.html#a142d59520c745004e03f4e823f5a8c28",
"dd/ddf/a04270.html#a27c856422f59885d3f2d8efd263d9d6d",
"de/d33/a04558.html#a158f90ba00afcb42d344530ccfb3724a",
"de/d60/a06018.html#a339ae05e5f2c3aae5a1127bf0e7defc3",
"de/d8e/a02213.html#a79eb0ce6971b66744e99c78d1d3bba41a7ffb697aac745545e72e76795f262462",
"de/dc0/a05446.html",
"df/d03/a00062_source.html",
"df/d0f/a05070.html#a60ab76e7b49735504300e89706073a84",
"df/d0f/a05070.html#ac6c4cc035cf203a1cc1081aef03bd1c2",
"df/d39/a00950.html",
"df/d87/a07250.html#ae2a0fc9d7c68cdee5082cac6511bb7dd",
"df/de5/a03966.html#a0fa3d717cd4399cfef28636f7f4e04d6",
"df/df6/a04914.html#afc3434515f637a75d3b28bca8c9f91f9",
"dir_615b393561a33c79b94313d87105de30.html",
"dir_cd0f90d441c69fd5cd53de873c70605e.html",
"globals_l.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';