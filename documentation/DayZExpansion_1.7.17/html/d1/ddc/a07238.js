var a07238 =
[
    [ "ExpansionPhysicsGeometry", "d1/ddc/a07238.html#a6dc4492e70e7e4ea8c11a840f775427a", null ],
    [ "Create", "d1/ddc/a07238.html#af32660113c47d0e3e94f3724fa3a3ade", null ],
    [ "CreateBox", "d1/ddc/a07238.html#ad288d40a7ac0726192186358d1eab1d9", null ],
    [ "SetGeom", "d1/ddc/a07238.html#a4ea8bd353935f43e08e9763f1964371c", null ],
    [ "SetMask", "d1/ddc/a07238.html#ac153c7a2eae30ecf46fadd75b296f392", null ],
    [ "SetMaterial", "d1/ddc/a07238.html#a2d7c5b6fcf15c3124b6da19edb32052f", null ],
    [ "SetPosition", "d1/ddc/a07238.html#a086f15e9e6f7bbd81981af1332547485", null ],
    [ "SetTransform", "d1/ddc/a07238.html#a5f03d966d23ff45f279346254bfac90e", null ]
];