var a01928 =
[
    [ "ExpansionMarketMenuItemTooltipEntryBase", "d4/de0/a06170.html", "d4/de0/a06170" ],
    [ "ExpansionMarketMenuItemTooltipEntryItemInfoController", "d4/d17/a06174.html", "d4/d17/a06174" ],
    [ "ExpansionMarketMenuItemTooltipEntryItemInfo", "d1/db0/a01928.html#aee380edd581eeb295c9b94602a32578e", null ],
    [ "GetControllerType", "d1/db0/a01928.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "d1/db0/a01928.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "SetColor", "d1/db0/a01928.html#a9b8d6ae5aa9ccd4fe010396c6564f2e3", null ],
    [ "SetIcon", "d1/db0/a01928.html#a015fb32c827408f1515af730bee07bd1", null ],
    [ "SetText", "d1/db0/a01928.html#a47a3d1a4bd1182cef70d052c48bc5eb7", null ],
    [ "m_TooltipEntryController", "d1/db0/a01928.html#a1b6db07c5871b60137346f2f17b2668a", null ],
    [ "tooltip_entry_text", "d1/db0/a01928.html#a0f6b18cd99aeb7a24903bade02ef10d3", null ],
    [ "tooltip_entry_title", "d1/db0/a01928.html#a44988ad2d6f8b85a78bdb72e26be1084", null ],
    [ "tooltip_icon", "d1/db0/a01928.html#a32dc742f1e6c71123f6d527d571903f1", null ]
];