var a05390 =
[
    [ "bldr_Candle", "d1/df3/a05390.html#a6c53e0c3ce74475b55f6939cffa45ec0", null ],
    [ "CanAssignToQuickbar", "d1/df3/a05390.html#ab50861bb0edbd5b96dd99973b6308abe", null ],
    [ "CanBePlaced", "d1/df3/a05390.html#a32bebffbea2e128ed08c92ce1654b6dc", null ],
    [ "CanDetachAttachment", "d1/df3/a05390.html#ae336030ab3427bed54e05211544f4360", null ],
    [ "CanDisplayAttachmentCategory", "d1/df3/a05390.html#a570de9cad115194972beced1c02e5083", null ],
    [ "CanDisplayAttachmentSlot", "d1/df3/a05390.html#a1edc2e9ef42895d45618be3182a09706", null ],
    [ "CanDisplayCargo", "d1/df3/a05390.html#aada18059563e8b88fd5987308ca60bff", null ],
    [ "CanLoadAttachment", "d1/df3/a05390.html#ab64aff95285be2bf2e67303172e6c891", null ],
    [ "CanLoadItemIntoCargo", "d1/df3/a05390.html#a333d0b24da29e2748d3091c4a0738a0c", null ],
    [ "CanPutAsAttachment", "d1/df3/a05390.html#a2b117fa1b39f1cc300e2256beef89891", null ],
    [ "CanPutInCargo", "d1/df3/a05390.html#a6676cea7c9800c7d93878509b985c85b", null ],
    [ "CanPutIntoHands", "d1/df3/a05390.html#ae2a90bd0eee4729ccba7f9bd3a80a0ec", null ],
    [ "CanReceiveAttachment", "d1/df3/a05390.html#a15d0b768d88d3fa74431a45ee0f548c6", null ],
    [ "CanReceiveItemIntoCargo", "d1/df3/a05390.html#ad7fa27ba1296f3bb3b723bbfd30d0330", null ],
    [ "CanReceiveItemIntoHands", "d1/df3/a05390.html#aeb05dd3163bb843499aa57a25d290c6f", null ],
    [ "CanReleaseCargo", "d1/df3/a05390.html#afa55cba8aaa3bbb08984abfef98934b1", null ],
    [ "CanReleaseFromHands", "d1/df3/a05390.html#ab9a566eb9986e903120c4f73ed1165a5", null ],
    [ "CanRemoveFromCargo", "d1/df3/a05390.html#a1d21ecea5d0d8f28a31b71d8a7eb7ef1", null ],
    [ "CanRemoveFromHands", "d1/df3/a05390.html#a5aa5e0d7935dcad428055f1ee746e132", null ],
    [ "CanSaveItemInHands", "d1/df3/a05390.html#aa4f0be351315388273b2c20e32dac484", null ],
    [ "CanSwapItemInCargo", "d1/df3/a05390.html#a10b22341ec50ef28a1a829e45da1251b", null ],
    [ "CreateLight", "d1/df3/a05390.html#ab1ab38ddd44a3b5cadb87aee3237dbbe", null ],
    [ "DeferredInit", "d1/df3/a05390.html#a1916ada565ae9134c184ded06702a062", null ],
    [ "IsInventoryVisible", "d1/df3/a05390.html#a083fffa9963f5a477af3873625fd980d", null ],
    [ "IsTakeable", "d1/df3/a05390.html#a8f4982956f2efa125b05d62d9ddb9c11", null ],
    [ "OnEnergyConsumed", "d1/df3/a05390.html#a262e07f7676b360d80c0e4f30496c001", null ],
    [ "m_Light", "d1/df3/a05390.html#aea7ad15be06c7a016a6c6e574c940ebe", null ]
];