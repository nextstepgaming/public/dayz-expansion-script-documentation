var a04890 =
[
    [ "ExpansionEntityStoragePlaceholder", "d1/d0b/a04890.html#af4c6ff8b825eada9d6fefe532dc13805", null ],
    [ "~ExpansionEntityStoragePlaceholder", "d1/d0b/a04890.html#a4c0692a361764686ea67a7f72ea800c1", null ],
    [ "CanDisplayAttachmentSlot", "d1/d0b/a04890.html#a2f45f4cded67c3cf6bac2b1f5ea0e926", null ],
    [ "CanDisplayCargo", "d1/d0b/a04890.html#aa28c88e2bfd90dace9e35d8e053cf0a7", null ],
    [ "CanPutAsAttachment", "d1/d0b/a04890.html#a4f75b9538b376b4b93ba94b9eae3d004", null ],
    [ "CanPutInCargo", "d1/d0b/a04890.html#ab458b31b52948291f0fc4b0547248109", null ],
    [ "CanPutIntoHands", "d1/d0b/a04890.html#ae1fd39fcab60ba867b7376e32f32d8e2", null ],
    [ "EEDelete", "d1/d0b/a04890.html#aa8af6a32ebf7cbe3cc91bbf5e630f6b3", null ],
    [ "Expansion_GetEntityStorageFileName", "d1/d0b/a04890.html#a9a89ba80c18a0a7beff042445121fd60", null ],
    [ "Expansion_GetStoredEntityDisplayName", "d1/d0b/a04890.html#add7351428968e1e576f85ee8c3b18c3e", null ],
    [ "Expansion_HasPlaceholder", "d1/d0b/a04890.html#a7b9cda167589cb443592aea48b63eaa5", null ],
    [ "Expansion_HasStoredEntity", "d1/d0b/a04890.html#a02c4979f3bd1a40817deb14d3e557943", null ],
    [ "Expansion_SetStoredEntityData", "d1/d0b/a04890.html#a45d7ec4d9bc66de752eec36f7d1c09aa", null ],
    [ "Expansion_StoreEntityAndReplace", "d1/d0b/a04890.html#a314c04c9587c0ad2837fee36e67e5b02", null ],
    [ "GetByStoredEntityGlobalID", "d1/d0b/a04890.html#a7f3b6672b27527eb34bd4e7a9489ba48", null ],
    [ "GetDisplayName", "d1/d0b/a04890.html#a5dd95326d82887f99665752c8b39cbcb", null ],
    [ "IsInventoryVisible", "d1/d0b/a04890.html#a291c6c516fa31d21d54be5a861dabaa0", null ],
    [ "OnRPC", "d1/d0b/a04890.html#a47ea63956212be4b99232d023cd53274", null ],
    [ "m_Expansion_NetsyncData", "d1/d0b/a04890.html#a00545b643710fd09ea1b2a71c8ac6ae0", null ],
    [ "m_Expansion_StoredEntityGlobalID", "d1/d0b/a04890.html#a80a182ca885d8466b94716575850fe25", null ],
    [ "s_Expansion_AllPlaceholders", "d1/d0b/a04890.html#abdafeb073af3e76c3762bd11b5d749b6", null ]
];