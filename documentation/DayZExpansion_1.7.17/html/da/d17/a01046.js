var a01046 =
[
    [ "ExpansionSyncedPlayerStats", "d7/d54/a04666.html", "d7/d54/a04666" ],
    [ "ExpansionPlayerStanceStatus", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892", [
      [ "UNKNOWN", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a6ce26a62afab55d7606ad4e92428b30c", null ],
      [ "STAND", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892af422fb81d42ecd479de08e64b6533d18", null ],
      [ "CROUCH", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a3cdd4783c5dbeae45bbcd15570a6b273", null ],
      [ "PRONE", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892ae57cc9240c1e64f42c414a6acb2ae626", null ],
      [ "CAR", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a5fc54ebcb1dd4bf1e1b93cbc77b57b40", null ],
      [ "HELICOPTER", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a5ed875b8404837e6474a4b500e656fcd", null ],
      [ "BOAT", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a5fcfdc36f4798fb6ace9348a10f1e1b3", null ],
      [ "DEAD", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a11fd9ca455f92c69c084484d5cd803c2", null ],
      [ "UNCONSCIOUS", "da/d17/a01046.html#a56153e5facc83b13d834608965b09892a467a2353100f3e76f03d339fa75ce599", null ]
    ] ],
    [ "OnRecieve", "da/d17/a01046.html#ad8508e874a78237bca7ef7b5dcc2c684", null ],
    [ "OnSend", "da/d17/a01046.html#a463b7e474edb11a48e9290e40d6e7df8", null ],
    [ "m_Bones", "da/d17/a01046.html#afb29a76b30e7264e8f29764176c2c148", null ],
    [ "m_Cholera", "da/d17/a01046.html#a7da63f7db3feb9e35cc49000fd66d6ea", null ],
    [ "m_Cuts", "da/d17/a01046.html#a9c33e17337543b45b0b3b2c8def6ad5f", null ],
    [ "m_Infection", "da/d17/a01046.html#a59a4b85f6e8e6f701d0c862d4c4fae66", null ],
    [ "m_Influenza", "da/d17/a01046.html#a1019767be5dec9e08a0aea02dee626d3", null ],
    [ "m_PlainID", "da/d17/a01046.html#aaa5e8e41ccfab57fd3812c38591c2d49", null ],
    [ "m_Poison", "da/d17/a01046.html#ac60853aed5bba4e0d17ae275d2e44217", null ],
    [ "m_Salmonella", "da/d17/a01046.html#ad6e1aad666bc4a61626321b52a5b7513", null ],
    [ "m_Sick", "da/d17/a01046.html#af3e20bba42c1d55bc117d92276a9dbb3", null ],
    [ "m_Stance", "da/d17/a01046.html#afa5c1cfda1764cece2a66c337de5bf68", null ]
];