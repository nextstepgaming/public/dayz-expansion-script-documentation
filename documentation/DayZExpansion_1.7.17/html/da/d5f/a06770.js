var a06770 =
[
    [ "CheckEntity", "da/d5f/a06770.html#ab6134cc80d97352239862b31f76e6491", null ],
    [ "GetActionState", "da/d5f/a06770.html#aad7b139d1c5b46e03b98f0e987919084", null ],
    [ "GetObjectiveType", "da/d5f/a06770.html#a083ac5b7c5443cbc5c67dde7c5ebb46d", null ],
    [ "OnActionUsed", "da/d5f/a06770.html#a33a2507111676650d21747ba6dfa33ac", null ],
    [ "QuestPrint", "da/d5f/a06770.html#a5fb531ece86ba8e33d08e63840a274fc", null ],
    [ "SetActionState", "da/d5f/a06770.html#afb9a82c531e4b33104506ef4d7d83485", null ],
    [ "m_ActionState", "da/d5f/a06770.html#ac3e5089df2bcbe23377cd93e0ebac250", null ],
    [ "m_CallLater", "da/d5f/a06770.html#a982d7d7d47ad1c8f103b8e63f4f3f5c9", null ],
    [ "m_LastEntity", "da/d5f/a06770.html#a369c2db73e8ff008d068f540eb881b55", null ]
];