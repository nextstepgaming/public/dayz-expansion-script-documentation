var a07610 =
[
    [ "ExpansionVehicleRotational", "da/d42/a07610.html#a46672766edf1ebc72b919bfe997a735c", null ],
    [ "AddComponent", "da/d42/a07610.html#a09a5924c11ecdcf512bc224c2feef808", null ],
    [ "FromRPM", "da/d42/a07610.html#a3f550a7cadde51d0e970eb6eab8839ce", null ],
    [ "GetInertia", "da/d42/a07610.html#a11f00f4a57f00dd02e23eae0c9871caa", null ],
    [ "PreSimulate", "da/d42/a07610.html#aa844355b4a49daf6c23538b4d35b33e6", null ],
    [ "ProcessAcceleration", "da/d42/a07610.html#ab0f54aa1e4794b47f71fed090beb5b98", null ],
    [ "ProcessTorque", "da/d42/a07610.html#adf5e270cc5839b5ed8c727b8d2bbefea", null ],
    [ "Sign", "da/d42/a07610.html#adb1cd6aa38436f8b46e01fdad5809c53", null ],
    [ "Simulate", "da/d42/a07610.html#a3eef57122f863f1e9af49665610c455b", null ],
    [ "ToRPM", "da/d42/a07610.html#a966841074976df2219a2267077312ab0", null ],
    [ "m_Acceleration", "da/d42/a07610.html#ac4c5021de6bc5d8c762ba98ecf3a7204", null ],
    [ "m_Components", "da/d42/a07610.html#ae66b97f78dc5082f18718ecd8ec18a21", null ],
    [ "m_Inertia", "da/d42/a07610.html#aa56079904db9de94691b7caf48395511", null ],
    [ "m_InvRatio", "da/d42/a07610.html#a781de9578fee413402e6308d555ec72c", null ],
    [ "m_Parent", "da/d42/a07610.html#a9131987f362f075907bda4931b84f7b1", null ],
    [ "m_Radius", "da/d42/a07610.html#a8abe778cb089ff9ce5e91d981fcd441a", null ],
    [ "m_Ratio", "da/d42/a07610.html#a5747131bac4fadb45898afd62361fa74", null ],
    [ "m_Torque", "da/d42/a07610.html#a0cbc813bbbc9d4fc44a374f7ed30190c", null ],
    [ "m_Velocity", "da/d42/a07610.html#a52142c1d5ef7983a97c71aee1bf03b8d", null ]
];