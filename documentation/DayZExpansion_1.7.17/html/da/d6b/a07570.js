var a07570 =
[
    [ "ExpansionVehicleEngineBase", "da/d6b/a07570.html#a24023284914785c466e5b0b32bdef2ac", null ],
    [ "CalculateMinTorque", "da/d6b/a07570.html#a880a0b837f20ee5d1b2ea740b93d157e", null ],
    [ "CalculateTorque", "da/d6b/a07570.html#ac61f2b69d5765a3955cd42e0618006ca", null ],
    [ "Init", "da/d6b/a07570.html#a3741ac9c8c009a0ccdc93a5b4bdb9510", null ],
    [ "Init_CarScript", "da/d6b/a07570.html#a09f12b1c31d2e5a1befaa7dc00146e5d", null ],
    [ "Init_ExpansionVehicle", "da/d6b/a07570.html#a238974f1babbd3920f601f9a63d11061", null ],
    [ "PreSimulate", "da/d6b/a07570.html#a843851b674b6e6bee5f9fa3b7046ba2f", null ],
    [ "ProcessAcceleration", "da/d6b/a07570.html#add1a77b40f005d5085b5430316f70c89", null ],
    [ "ProcessHealth", "da/d6b/a07570.html#a2acfea9b81587e75dec1312134268ff1", null ],
    [ "ProcessTorque", "da/d6b/a07570.html#a917eeeec1096410c83d3799b15cc4d4f", null ],
    [ "SmoothStep", "da/d6b/a07570.html#a8f124acd1e815063a5bf66053b613b4e", null ],
    [ "m_DamageZone", "da/d6b/a07570.html#ab349142b716bc8d72a6a9c998be2e078", null ],
    [ "m_EngineIndex", "da/d6b/a07570.html#a4f4665d7689709c997e0ab560c6c0285", null ],
    [ "m_FuelConsumption", "da/d6b/a07570.html#a6f1b98bfb91959e183bd967e22a60247", null ],
    [ "m_Gearbox", "da/d6b/a07570.html#a2c6c99013bf44d9beaa87ecce8abcf3b", null ],
    [ "m_GearIndex", "da/d6b/a07570.html#a8369b30e5facd9eedd2e963855d733db", null ],
    [ "m_Health", "da/d6b/a07570.html#ad2ce87349cc56119254115ccd839e41c", null ],
    [ "m_MaxTorque", "da/d6b/a07570.html#ad9dbae7e76c9ebed223551a1a680f99b", null ],
    [ "m_MinTorque", "da/d6b/a07570.html#a8d100976bc00a22aca1e4cd29fd288b1", null ],
    [ "m_RPM", "da/d6b/a07570.html#a6b2c5d428aef751464b7f54c422584c1", null ],
    [ "m_RPMClutch", "da/d6b/a07570.html#a522e6a92f2a59c4f2de613ea4ced1450", null ],
    [ "m_RPMIdle", "da/d6b/a07570.html#afb46854696edf5d8f37e760a89e0bbea", null ],
    [ "m_RPMMax", "da/d6b/a07570.html#ab1db37d307c9d16fd4e0ea2d5a2adc2e", null ],
    [ "m_RPMMin", "da/d6b/a07570.html#a9d33935c72334d5865c54849cdb2cbef", null ],
    [ "m_RPMRedline", "da/d6b/a07570.html#a21348bae7171855b1238b8afc48c7bcd", null ],
    [ "m_State", "da/d6b/a07570.html#a973076cc1b1cf4cb5766da7ecca13402", null ],
    [ "m_Throttle", "da/d6b/a07570.html#aff17b2960b66250c62c243fbe7c3bd00", null ],
    [ "m_ThrottleIdle", "da/d6b/a07570.html#a80d5b6d76f7b2a3fcf0461a9c1b756de", null ],
    [ "m_ThrottleIndex", "da/d6b/a07570.html#a6ec9a6b1e1d9b07a34f5f39268ce7320", null ],
    [ "m_ThrottleRange", "da/d6b/a07570.html#adbed5e9e10ddca8bfe77bacaffa21983", null ]
];