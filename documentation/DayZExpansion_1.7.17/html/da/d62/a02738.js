var a02738 =
[
    [ "ExpansionVehicleSettingsBase", "d7/d22/a07246.html", "d7/d22/a07246" ],
    [ "ExpansionVehicleSettings", "df/d87/a07250.html", "df/d87/a07250" ],
    [ "ExpansionPPOGORIVMode", "da/d62/a02738.html#ad828747a6913c96d8d15ba8922a28f95", [
      [ "Disabled", "da/d62/a02738.html#ad828747a6913c96d8d15ba8922a28f95af31108c24daa76e574c8259d9fe46c09", null ],
      [ "Always", "da/d62/a02738.html#ad828747a6913c96d8d15ba8922a28f95ae1a8e8cd4807ad56b76df10995885c33", null ],
      [ "OnlyOnServerRestart", "da/d62/a02738.html#ad828747a6913c96d8d15ba8922a28f95a0dc8bf43601396528de4b9b0f5ae5304", null ]
    ] ],
    [ "CanChangeLock", "da/d62/a02738.html#a85d95cd4ecaa4cf3320953781a7c7d46", null ],
    [ "CanPickLock", "da/d62/a02738.html#a97656709f67889f34b88e9bd80314f7f", null ],
    [ "ChangeLockTimeSeconds", "da/d62/a02738.html#af26a635204ebd8852c9895d60f7b4d7d", null ],
    [ "ChangeLockToolDamagePercent", "da/d62/a02738.html#a5f36b44c77d6021e3a347af9cc17bca5", null ],
    [ "ChangeLockTools", "da/d62/a02738.html#a33d4d5473dab8e7f11b59d13d249ec1b", null ],
    [ "DisableVehicleDamage", "da/d62/a02738.html#a5c429c727a89a47fe965ab24571bc3f2", null ],
    [ "EnableHelicopterExplosions", "da/d62/a02738.html#a17618acfbf4c081f614db93537299159", null ],
    [ "EnableTailRotorDamage", "da/d62/a02738.html#acab5ec6f75128707aa8845c5a8ab223c", null ],
    [ "EnableWindAerodynamics", "da/d62/a02738.html#a4bcaae1308a1db41476a4a7c89be5571", null ],
    [ "MasterKeyPairingMode", "da/d62/a02738.html#af518c0da25ea7cf61cb6dc5faa638c91", null ],
    [ "MasterKeyUses", "da/d62/a02738.html#adb4ce5ff22028a786de5fe04b9ae8619", null ],
    [ "PickLockChancePercent", "da/d62/a02738.html#a744957e6a894b9143d1e07594b8b7830", null ],
    [ "PickLockTimeSeconds", "da/d62/a02738.html#ae08ad63b77efc065fd2b0fabbd981ffa", null ],
    [ "PickLockToolDamagePercent", "da/d62/a02738.html#a752cb2f2e7a3728996af7b153704a85a", null ],
    [ "PickLockTools", "da/d62/a02738.html#afad4cc6869a217ac8e3c28394b3a9a41", null ],
    [ "PlayerAttachment", "da/d62/a02738.html#a9ef5d71404122e7ee20120286fdf1c33", null ],
    [ "Towing", "da/d62/a02738.html#affd0719c4ccb66d395b6a053b0e20970", null ],
    [ "VehicleCrewDamageMultiplier", "da/d62/a02738.html#a3e50b6ec74f69f91f4f6361f7a14dca1", null ],
    [ "VehicleLockedAllowInventoryAccess", "da/d62/a02738.html#ac7df8ce45d1bcb64ded892fd5bd3423e", null ],
    [ "VehicleLockedAllowInventoryAccessWithoutDoors", "da/d62/a02738.html#a0897706c6f6681725d028ae0163ead3a", null ],
    [ "VehicleRequireAllDoors", "da/d62/a02738.html#a5ce5ba03a23b281222348c0393794740", null ],
    [ "VehicleRequireKeyToStart", "da/d62/a02738.html#ac2284e81f5c79bc862dd0b2073db779d", null ],
    [ "VehicleSpeedDamageMultiplier", "da/d62/a02738.html#a09aedfa926661b335b219700dacbcc6e", null ],
    [ "VehicleSync", "da/d62/a02738.html#a6bd166ed47d6cc58595e271dcbfbf805", null ]
];