var a06406 =
[
    [ "ExpansionNotificationSchedule", "da/d70/a06406.html#af541593d9558cf683b1241a592d0ff22", null ],
    [ "InitColor", "da/d70/a06406.html#a4fe18463489e03927702556a710c4626", null ],
    [ "OnReceive", "da/d70/a06406.html#a0ce105f3c898bee9e3b18d0701595fee", null ],
    [ "OnSend", "da/d70/a06406.html#afc3a4f7886e279a195461048fabea8e1", null ],
    [ "Color", "da/d70/a06406.html#aa26f6e588b6cb739ec01eacc3c0524d2", null ],
    [ "Hour", "da/d70/a06406.html#a830c71bc2cd2cf16e9dfe75527570628", null ],
    [ "Icon", "da/d70/a06406.html#aaedaf7c92c1feb14cd4ab5f4b5cda379", null ],
    [ "m_Color", "da/d70/a06406.html#abcc91a8673d439390b2ce98a90358bbc", null ],
    [ "m_LastShownTimestamp", "da/d70/a06406.html#a483a0b673b74d6ea70267f734c65b97e", null ],
    [ "Minute", "da/d70/a06406.html#adeb0c3e1e60ba7cc5a8c08e918d45754", null ],
    [ "Second", "da/d70/a06406.html#af2ce528692b90e84b743be110c7d5345", null ],
    [ "Text", "da/d70/a06406.html#a149ae3cb15eea433e901102c27602439", null ],
    [ "Title", "da/d70/a06406.html#ad214930331a67c255a0a38b5c81b6801", null ]
];