var a06374 =
[
    [ "ExpansionMarkerClientInfo", "da/d02/a06374.html#a14515f5e93a99439158b06d0d7e1d98f", null ],
    [ "ClearVisibility", "da/d02/a06374.html#aa281b9ce0351060ae4ba79c1be97f823", null ],
    [ "FlipVisibility", "da/d02/a06374.html#a057ccd8d1148e6d12fa49a8816a113c7", null ],
    [ "GetUID", "da/d02/a06374.html#a7ba88ded1ccdcc5a1bd5156612ef2113", null ],
    [ "GetVisibility", "da/d02/a06374.html#a5c277c3446fd8c2b18c91d36894dbb4c", null ],
    [ "IsMapVisible", "da/d02/a06374.html#a5ef9000db268872889f9c91542c1706a", null ],
    [ "IsVisible", "da/d02/a06374.html#a142b3697fe799f4ae26cd630f028a00c", null ],
    [ "IsWorldVisible", "da/d02/a06374.html#a2a756ce19501eadacbc1663e1bee5d3a", null ],
    [ "OnStoreLoad", "da/d02/a06374.html#aa33e2c79a56b75035bb78940cd16de76", null ],
    [ "OnStoreSave", "da/d02/a06374.html#a509fc985ae3bc7cb6edd0cc3434a912c", null ],
    [ "RemoveVisibility", "da/d02/a06374.html#af4cc1662a6232d77fb8b96598c6f7655", null ],
    [ "SetUID", "da/d02/a06374.html#a67c36c8f5f9c9eea7a45a35dabec43c9", null ],
    [ "SetVisibility", "da/d02/a06374.html#a2352b78712ef91df5262dcd6f6bbcdfe", null ],
    [ "m_UID", "da/d02/a06374.html#a5d56be89032d90429769580f1e1ec4ed", null ],
    [ "m_Visibility", "da/d02/a06374.html#a4533b424b16900cc25f5703925fadb77", null ]
];