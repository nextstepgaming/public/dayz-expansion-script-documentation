var a07162 =
[
    [ "ExpansionSpawnLocation", "d9/dc8/a07162.html#ad332f9e96943074b78b51c365ac4d052", null ],
    [ "~ExpansionSpawnLocation", "d9/dc8/a07162.html#a83bb54a0f7d79e5a0e42ec666ab46db0", null ],
    [ "Copy", "d9/dc8/a07162.html#a1ec62cfb38f6a5c3b7c93872b8fe25b3", null ],
    [ "IsTerritory", "d9/dc8/a07162.html#a30a12bcdb92d34c87a256d79602a850f", null ],
    [ "SetIsTerritory", "d9/dc8/a07162.html#af71ba83fdd1350ee9cc883cef0e25184", null ],
    [ "SetUseCooldown", "d9/dc8/a07162.html#a8dbd913aa3c723ead6be7ea6995f4b2d", null ],
    [ "UseCooldown", "d9/dc8/a07162.html#aac271defc9f33f12d86492367c87881d", null ],
    [ "IsTerritory", "d9/dc8/a07162.html#af1e3280d4b3eee74e1d4e15980264372", null ],
    [ "Name", "d9/dc8/a07162.html#a984012ec9d7f92a788bc700f1834bab5", null ],
    [ "Positions", "d9/dc8/a07162.html#a9b46d22c72506a8212de210728deae0f", null ],
    [ "UseCooldown", "d9/dc8/a07162.html#aeb0e3d551dd73785553e08668e1578ff", null ]
];