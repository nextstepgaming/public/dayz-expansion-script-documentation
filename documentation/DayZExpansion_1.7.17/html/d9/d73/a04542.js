var a04542 =
[
    [ "ExpansionZonePolygon", "d9/d73/a04542.html#ac2875d763f808cbd405088fcc9fa1127", null ],
    [ "Check", "d9/d73/a04542.html#a239226562ba1315192f6d7cf5b8137be", null ],
    [ "ToStr", "d9/d73/a04542.html#aa23285185b9b0f490c93fd4ee183a9c6", null ],
    [ "m_Count", "d9/d73/a04542.html#ac6594137a976005b81bd49c5711974dd", null ],
    [ "m_Position", "d9/d73/a04542.html#a6aec8b6d4e526a1083dafc23db648458", null ],
    [ "m_Positions_X", "d9/d73/a04542.html#a9f6610f38db0a6f8c082c85af280bd3b", null ],
    [ "m_Positions_Z", "d9/d73/a04542.html#a5b79488b4f7ecfdf797c9d44d571f3bf", null ],
    [ "m_Radius", "d9/d73/a04542.html#a678e1fab033c0e140108d7906e048a7a", null ],
    [ "s_ItrJ", "d9/d73/a04542.html#ac749a192fc9deadc014ec4ffdde555c2", null ],
    [ "s_ItrK", "d9/d73/a04542.html#aef76a791afefc7d28dcc6959cbee1a1b", null ],
    [ "s_PosX", "d9/d73/a04542.html#aef815e1b787b85afdf9c1e75448531ba", null ],
    [ "s_PosX0", "d9/d73/a04542.html#ad3c67a2d9768f794f7d71fa388ce5818", null ],
    [ "s_PosX1", "d9/d73/a04542.html#ac5791785c7a70ac6ebe96c46a96d02ea", null ],
    [ "s_PosZ", "d9/d73/a04542.html#a646a5efb3abfba61389afae2e330384e", null ],
    [ "s_PosZ0", "d9/d73/a04542.html#a2c60a753385954ed83108854ecbfa115", null ],
    [ "s_PosZ1", "d9/d73/a04542.html#a57111fc633a2c091af008c3b995e9abf", null ]
];