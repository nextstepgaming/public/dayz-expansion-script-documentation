var a05418 =
[
    [ "ATMPartyLockerEnabled", "d9/d54/a05418.html#a868159e3a21c000f8de6adc32721639d", null ],
    [ "ATMPlayerTransferEnabled", "d9/d54/a05418.html#a50d90a828d12b17312562b156a668452", null ],
    [ "ATMSystemEnabled", "d9/d54/a05418.html#a7e466a394f2df88dd8fe377aa9762065", null ],
    [ "CurrencyIcon", "d9/d54/a05418.html#a39c6a79f5cb78196008af9f4f45ac5f8", null ],
    [ "DefaultDepositMoney", "d9/d54/a05418.html#a56225eb069f4dc97c35dfcf6dc9133fd", null ],
    [ "MarketSystemEnabled", "d9/d54/a05418.html#a734df0461401a2314bb260b00897e3f1", null ],
    [ "MaxDepositMoney", "d9/d54/a05418.html#a590edaae6d642d5438488553642d97cb", null ],
    [ "MaxPartyDepositMoney", "d9/d54/a05418.html#a38ce3c988709ceb5daa40652d6234a87", null ],
    [ "NetworkCategories", "d9/d54/a05418.html#a9faea17412fa7157e53f17e84938fcfb", null ]
];