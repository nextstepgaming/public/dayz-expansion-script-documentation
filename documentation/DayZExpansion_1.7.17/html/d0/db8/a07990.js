var a07990 =
[
    [ "CanDisplayAttachmentSlot", "d0/db8/a07990.html#a04e1ddbdae3a8bb4a1b19bc9931d6745", null ],
    [ "CanEnterIronsights", "d0/db8/a07990.html#aae37ff985c0e888a9ae7ffd8ded990dc", null ],
    [ "CanReceiveAttachment", "d0/db8/a07990.html#a27d4a8dcd40b98f71de2c1937b257521", null ],
    [ "CanReleaseAttachment", "d0/db8/a07990.html#aa9b461c583e835b9e5f6969460008ec7", null ],
    [ "OnDebugSpawn", "d0/db8/a07990.html#aeae572a599481a4f9f8de5e155a840ee", null ],
    [ "SpawnRecoilObject", "d0/db8/a07990.html#aa9c6159fd824afffbb477916593220f5", null ]
];