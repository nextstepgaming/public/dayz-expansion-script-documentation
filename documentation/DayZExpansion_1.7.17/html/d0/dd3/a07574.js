var a07574 =
[
    [ "ExpansionVehicleGearbox", "d0/dd3/a07574.html#ad8ee5afea81dc47dcc2522cacadea289", null ],
    [ "Count", "d0/dd3/a07574.html#a8935c522ba5fc6280aa76f8846963022", null ],
    [ "PreSimulate", "d0/dd3/a07574.html#a336d62ae6e3b0a380e7142cfe8799ff8", null ],
    [ "m_Clutch", "d0/dd3/a07574.html#a74cd7cca8c04ef28e6c6873e397bd00c", null ],
    [ "m_ClutchTorque", "d0/dd3/a07574.html#a7b9eec482c371e67b62e29b7107b9e44", null ],
    [ "m_DeltaVelocity", "d0/dd3/a07574.html#aadb0cb7a4c0dffe95e95b6ea19a5ed4f", null ],
    [ "m_Engine", "d0/dd3/a07574.html#a35882ee767ec80bff2c0b046ef838bab", null ],
    [ "m_Gear", "d0/dd3/a07574.html#af9a3d0126bf75e9357844014f339a668", null ],
    [ "m_GearIndex", "d0/dd3/a07574.html#a9bd4f53ac6ad4b734f34dd8d1b58b53e", null ],
    [ "m_Gears", "d0/dd3/a07574.html#ac0f5949c1eac3c4925c9df52b144707e", null ],
    [ "m_MaxClutchTorque", "d0/dd3/a07574.html#a59dab0e8fa24eca5316812f0be3edeaa", null ],
    [ "m_Ratios", "d0/dd3/a07574.html#a990f644b7c51acf7a5a5ad657b7cdb98", null ],
    [ "m_TimeToCoupleClutch", "d0/dd3/a07574.html#a27079a88e3a1802a959e81b52819bdfe", null ],
    [ "m_TimeToUncoupleClutch", "d0/dd3/a07574.html#a0c917cd9199f6b294af9cebb0564e827", null ]
];