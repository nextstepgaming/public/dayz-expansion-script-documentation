var a04874 =
[
    [ "ExpansionLampLightBase", "d5/d8e/a04874.html#afba078875be5ff401b84bba3a3ffd521", null ],
    [ "~ExpansionLampLightBase", "d5/d8e/a04874.html#adf50ca1259ccba7a466834d8d726f96a", null ],
    [ "CanBeEnabled", "d5/d8e/a04874.html#abfd5301159b3c5e42fe371af9e0931d7", null ],
    [ "Disable", "d5/d8e/a04874.html#a0ec842665a216d26a465650b23ff87d0", null ],
    [ "Enable", "d5/d8e/a04874.html#a9352d9fc5affdaf412617fbe006cff97", null ],
    [ "ExpansionAttachOnMemoryPoint", "d5/d8e/a04874.html#ae80fa6dfe90465c543ff3a4a7b2cf52f", null ],
    [ "ExpansionAttachOnObject", "d5/d8e/a04874.html#a4bb18e30bf8b665a5de13bf1eb13afce", null ],
    [ "GetLampColor", "d5/d8e/a04874.html#a4ad0d7e2fccc42ebcdf7b4e07044a178", null ],
    [ "GetLampPosition", "d5/d8e/a04874.html#ad8dcaf7869c634e1fa20ce72d88c5871", null ],
    [ "IsEnabled", "d5/d8e/a04874.html#a7b6c0741d0a6ca648612347af653d6a5", null ],
    [ "LampInGenerator", "d5/d8e/a04874.html#a53d9d1e265e4cedb491a3678839cb158", null ],
    [ "OnDisable", "d5/d8e/a04874.html#a2e7b9c66534f1b71bdd864bef19190e7", null ],
    [ "OnEnable", "d5/d8e/a04874.html#ab8f12e76060603672eafd8a42eec7b8d", null ],
    [ "OnSettingsUpdated", "d5/d8e/a04874.html#a5eb0e7d0695947015da4e6661965c6a4", null ],
    [ "m_CanBeEnabled", "d5/d8e/a04874.html#a92c839584f6353a86dd321fe1ba885b3", null ],
    [ "m_CastShadow", "d5/d8e/a04874.html#ac5021daf6af393ea9a1cc7e70baab571", null ],
    [ "m_LampColor", "d5/d8e/a04874.html#aef3af46f626d2b6781550fb344092870", null ],
    [ "m_LampPosition", "d5/d8e/a04874.html#a6f5abbd50b8a7d312a43e55373f25bee", null ],
    [ "m_Light", "d5/d8e/a04874.html#a89745e616c5360c6080415c46c5034bb", null ],
    [ "m_ShouldBeEnabled", "d5/d8e/a04874.html#a5c5de2a4d54751582a406985f731700b", null ]
];