var a06710 =
[
    [ "AddAllowedWeapon", "d5/dee/a06710.html#adb172ea6b5810a67da57ff8bc2c32f62", null ],
    [ "AddClassName", "d5/dee/a06710.html#a5cab2d44c55a4f803cb59b25e8fe092d", null ],
    [ "AddExcludedClassName", "d5/dee/a06710.html#a0fb2df0e6aa9da6897abc237ff2aebde", null ],
    [ "GetAllowedWeapons", "d5/dee/a06710.html#ad6d14d3b627bc9c64d903e054aea5bf1", null ],
    [ "GetAmount", "d5/dee/a06710.html#afe19c7f54a705fcb089887dd48fd4e37", null ],
    [ "GetClassNames", "d5/dee/a06710.html#a0aeeaa23068d88e6732636031267772a", null ],
    [ "GetExcludedClassNames", "d5/dee/a06710.html#a5feaae69f12e2746cec94261fe7c5f32", null ],
    [ "NeedSpecialWeapon", "d5/dee/a06710.html#a796cb8c0fd3ce47c38b387537ba348d5", null ],
    [ "QuestDebug", "d5/dee/a06710.html#af597f9005beb3c7e79ded08974621f5f", null ],
    [ "SetAmount", "d5/dee/a06710.html#a6fdae59b862e9d5595deadbdafbff9a5", null ],
    [ "SetNeedSpecialWeapon", "d5/dee/a06710.html#a4c88cf9c36164cb4765aec83bbd602aa", null ],
    [ "AllowedWeapons", "d5/dee/a06710.html#a435568003ebd22671a94baad4bc087d6", null ],
    [ "Amount", "d5/dee/a06710.html#adadead7c3bbd5e79f7064261eb752c29", null ],
    [ "ClassNames", "d5/dee/a06710.html#a711004424553256c8ee1f0fc890b7c6a", null ],
    [ "CountSelfKill", "d5/dee/a06710.html#acfe7e91b5886b41a4dcda28f3f8d5f57", null ],
    [ "ExcludedClassNames", "d5/dee/a06710.html#a3987f27d8486d8f069ce8868655da2aa", null ],
    [ "SpecialWeapon", "d5/dee/a06710.html#ad13bb5fc079a5ecae2f8c347397280bd", null ]
];