var a07622 =
[
    [ "ExpansionVehicleCarThrottle", "d5/dbe/a07622.html#a527436325f438f7c373a8f6bd7195860", null ],
    [ "Control", "d5/dbe/a07622.html#aea58a0609e1616b5f6544537fa8659a4", null ],
    [ "m_Brake", "d5/dbe/a07622.html#a980ba06f62063799aeb60bd8cf6f6b79", null ],
    [ "m_DefaultThrust", "d5/dbe/a07622.html#ac7943250145b97fd063d66a3f41fb667", null ],
    [ "m_GentleCoef", "d5/dbe/a07622.html#ad2ee4a80fcc3761232f527c2c56d13a8", null ],
    [ "m_GentleThrust", "d5/dbe/a07622.html#abbc5810c402c2016041a56ef1faa9465", null ],
    [ "m_ReactionTime", "d5/dbe/a07622.html#a26006bc30894757e2c8896444028b2ed", null ],
    [ "m_TurboCoef", "d5/dbe/a07622.html#ad1733d80849a6c1f9e38a01837d6f6dd", null ]
];