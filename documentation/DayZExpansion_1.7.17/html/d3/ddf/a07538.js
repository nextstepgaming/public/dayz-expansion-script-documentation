var a07538 =
[
    [ "ExpansionVehicleAerofoil", "d3/ddf/a07538.html#a2deaac96a27b08850974fbff759afc79", null ],
    [ "Animate", "d3/ddf/a07538.html#a817d9a01e9c145e36ff0ad2387fa8e62", null ],
    [ "PreSimulate", "d3/ddf/a07538.html#a15ad9a31d9638a8138b105351c3e4f75", null ],
    [ "Simulate", "d3/ddf/a07538.html#a0932de530c41eaafdefbec5630eff114", null ],
    [ "m_AirflowMagnitudeSq", "d3/ddf/a07538.html#ad128bf2ef1e24d4f2b4ae2d326d8e265", null ],
    [ "m_AirFlowNormal", "d3/ddf/a07538.html#a1e2d8927b5aff1b716f43a985395fa92", null ],
    [ "m_Angle", "d3/ddf/a07538.html#af607ad06680a1dc1e7a8b801d5dcdab6", null ],
    [ "m_AngleOfAttack", "d3/ddf/a07538.html#a04d0e81c3a1dc78c29bd778fe7349a73", null ],
    [ "m_Animation", "d3/ddf/a07538.html#a179fd43a8dcc85d847960e3d860d7ff8", null ],
    [ "m_Area", "d3/ddf/a07538.html#ad7a3d8264b94a9a116ae7c72cee2a690", null ],
    [ "m_Camber", "d3/ddf/a07538.html#a153a00fdce87906825de05ae39f16191", null ],
    [ "m_DragAngle", "d3/ddf/a07538.html#a4ed6cd6e8f5bc67eb1d5a0258805e460", null ],
    [ "m_DragCoef", "d3/ddf/a07538.html#a6513113d1673ca960e68af5e404bd784", null ],
    [ "m_Flap", "d3/ddf/a07538.html#ada434e6ff133106822aa694eb29b3fe5", null ],
    [ "m_Input", "d3/ddf/a07538.html#a10683f0ba7a4459104912c82d68fa17e", null ],
    [ "m_LiftAngle", "d3/ddf/a07538.html#af89f2f3f94767cb6c88dafe5bdd2f458", null ],
    [ "m_LiftCoef", "d3/ddf/a07538.html#a6f3ff14d9bbd2104d10f0c203d8e069a", null ],
    [ "m_MaxControlAngle", "d3/ddf/a07538.html#a0371a9b0314d1bd51b4f727dc713ee55", null ],
    [ "m_Name", "d3/ddf/a07538.html#a1ff97fd4c82030fefad98199efb07ff9", null ],
    [ "m_PressureCoef", "d3/ddf/a07538.html#ab744786bd76363ff092973f0793370b9", null ],
    [ "m_StallAngle", "d3/ddf/a07538.html#a9cad512eae6f3c137c3c19d8f9eb4b77", null ],
    [ "m_Stalling", "d3/ddf/a07538.html#aa4decb721aa95eeaa1b8b7d9d00b9c19", null ],
    [ "m_Type", "d3/ddf/a07538.html#a4594d9e2e2896145f6e31b042ab789c4", null ],
    [ "m_Up", "d3/ddf/a07538.html#ae3a173c024879135bab21f41928761bd", null ]
];