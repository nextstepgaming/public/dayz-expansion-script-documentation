var a07606 =
[
    [ "ExpansionVehicleProp", "d3/dd4/a07606.html#afdc44c4cc5e9aeaed74a10d843cde64b", null ],
    [ "Animate", "d3/dd4/a07606.html#a792b11750c02ed5584b928bf4fb90d9b", null ],
    [ "PreSimulate", "d3/dd4/a07606.html#ab38d27ea3074c3398e2b1995e5764659", null ],
    [ "Simulate", "d3/dd4/a07606.html#a261ddfd508591ca4decb4876f9932e0e", null ],
    [ "m_Animation", "d3/dd4/a07606.html#a071b9e11abc21621e3e0d50a73381cad", null ],
    [ "m_Direction", "d3/dd4/a07606.html#a88debedfacf733f905869e6c85c4bfa0", null ],
    [ "m_EngineIndex", "d3/dd4/a07606.html#abf146236d329211ac11a0b5e314cd046", null ],
    [ "m_HideRotor", "d3/dd4/a07606.html#a5f72d72e5d9ed69b0af4b07632a128ff", null ],
    [ "m_HideRotorBlur", "d3/dd4/a07606.html#afa1d2cc41d7d4e873a3b890a4d5ed787", null ],
    [ "m_MaxPitch", "d3/dd4/a07606.html#a034916b6072a31ec35980cd7cd2665b9", null ],
    [ "m_MaxYaw", "d3/dd4/a07606.html#ae2dab86f5cb7e8c54ea8b010687b93a7", null ],
    [ "m_PitchAnimation", "d3/dd4/a07606.html#a930d5b6368384d16ab38c29a82206b56", null ],
    [ "m_RotorPosition", "d3/dd4/a07606.html#aa29b60501ea967b5e2842a2efce7fd1c", null ],
    [ "m_RotorRadius", "d3/dd4/a07606.html#a9d8a8a9c6414afa76810c23e26cce710", null ],
    [ "m_Type", "d3/dd4/a07606.html#aad070772914fd448f73b815185a706a3", null ],
    [ "m_UseBlur", "d3/dd4/a07606.html#a63acfed87d64b40e903aca89426e13e8", null ],
    [ "m_Velocity", "d3/dd4/a07606.html#a56f558582e4b8ccc38b12f180dd9e7f7", null ],
    [ "m_YawAnimation", "d3/dd4/a07606.html#ada5f76f1a9769fda2a1947cc2e392bc3", null ]
];