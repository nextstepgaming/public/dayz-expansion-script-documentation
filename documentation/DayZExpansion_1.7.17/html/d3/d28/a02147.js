var a02147 =
[
    [ "ExpansionNotificationSchedule", "da/d70/a06406.html", "da/d70/a06406" ],
    [ "ExpansionNotificationSchedulerSettings", "db/d70/a06410.html", "db/d70/a06410" ],
    [ "ExpansionSettingsNS_RPC", "d3/d28/a02147.html#ae8cc021075db76fc3c783c3e38abb0ea", [
      [ "INVALID", "d3/d28/a02147.html#ae8cc021075db76fc3c783c3e38abb0eaaef2863a469df3ea6871d640e3669a2f2", null ],
      [ "NotificationScheduler", "d3/d28/a02147.html#ae8cc021075db76fc3c783c3e38abb0eaade04e054c2132d9c9b07a04599e9b661", null ],
      [ "COUNT", "d3/d28/a02147.html#ae8cc021075db76fc3c783c3e38abb0eaa2addb49878f50c95dc669e5fdbd130a2", null ]
    ] ],
    [ "Update", "d3/d28/a02147.html#a70ac517aa5d3b3af95f61a2d560d811f", null ],
    [ "Color", "d3/d28/a02147.html#a5ed59cfae72726e4c032fcd4e2f53780", null ]
];