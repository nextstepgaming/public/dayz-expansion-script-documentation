var a09445 =
[
    [ "ItemBase", "d7/d82/a03774.html", "d7/d82/a03774" ],
    [ "ExpansionVehicleDynamicState", "d3/d4a/a09445.html#a976d157d65e05cfe2e24f2ffab8665dd", [
      [ "STATIC", "d3/d4a/a09445.html#a976d157d65e05cfe2e24f2ffab8665ddae55a36a850c67d46b3b3325de7fce0b8", null ],
      [ "TRANSITIONING", "d3/d4a/a09445.html#a976d157d65e05cfe2e24f2ffab8665dda1496dab105bcfaaf0c3d2a69f649598b", null ],
      [ "DYNAMIC", "d3/d4a/a09445.html#a976d157d65e05cfe2e24f2ffab8665ddaaac65e0072e6ff1f4c3209d2fdd8730a", null ]
    ] ]
];