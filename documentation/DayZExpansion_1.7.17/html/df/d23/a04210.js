var a04210 =
[
    [ "ExpansionBookMenuTabBase", "df/d23/a04210.html#a02a3892b4ea14a75f7155ee32b0ee940", null ],
    [ "AddChildTab", "df/d23/a04210.html#a801dbe4b039261e6797b6802937d10d4", null ],
    [ "CanClose", "df/d23/a04210.html#a3a06335824c212a948e61f39e63470a8", null ],
    [ "GetBookMenu", "df/d23/a04210.html#aafb4335f21f9546db5534a17bec4b5d1", null ],
    [ "GetChildTabs", "df/d23/a04210.html#a5be5abd8e89d1a440b04539f58b90282", null ],
    [ "GetParentTab", "df/d23/a04210.html#abd3112e6a8e598f2764204f44667f4c0", null ],
    [ "GetTabColor", "df/d23/a04210.html#a6b72818acb9c73e18bb1872031e197b5", null ],
    [ "GetTabIconName", "df/d23/a04210.html#a81cfab6b14cd88ad42bb4c451103c170", null ],
    [ "GetTabName", "df/d23/a04210.html#a908971c1cd83a1a488bc3cb37f08efa6", null ],
    [ "IgnoreBackButtonBase", "df/d23/a04210.html#a520dc62d8f05972571c7f7172693adb3", null ],
    [ "IsChildTab", "df/d23/a04210.html#a1c7257b9e0a4c7ae417ea9c8ea4f89ef", null ],
    [ "IsParentTab", "df/d23/a04210.html#a1870bcdbcdefa04aed4ca7350941f05d", null ],
    [ "OnBackButtonClick", "df/d23/a04210.html#a6c06d693ad4d424939c5f459fc647096", null ],
    [ "OnHide", "df/d23/a04210.html#a0ca00f2496da2fb40d59f25274278c5b", null ],
    [ "OnShow", "df/d23/a04210.html#a14a3da1dab03191398d0ad5830974f7c", null ],
    [ "SetParentTab", "df/d23/a04210.html#a4d9f693d0d2bbe42f0cd21bef24e01a6", null ],
    [ "SwitchMovementLockState", "df/d23/a04210.html#a3db21a8368465b60ace224b7da9b17c5", null ],
    [ "m_BookMenu", "df/d23/a04210.html#af262a87ec5c71aaf511222bcdfaebdaf", null ],
    [ "m_ParentTab", "df/d23/a04210.html#a871e93ea56c003e0ac98def94c2072d4", null ],
    [ "m_TabChildren", "df/d23/a04210.html#a9311c66d23db008012e6f1c681a7f803", null ]
];