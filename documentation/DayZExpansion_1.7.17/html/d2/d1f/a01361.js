var a01361 =
[
    [ "ExpansionDialogContent_Text", "d6/de6/a05234.html", "d6/de6/a05234" ],
    [ "ExpansionDialogContent_TextController", "d6/d66/a05238.html", "d6/d66/a05238" ],
    [ "ExpansionMenuDialogContent_TextController", "d6/d63/a05242.html", "d6/d63/a05242" ],
    [ "ExpansionMenuDialogContent_Text", "d2/d1f/a01361.html#a3a744778e6fcfabd0b481b677172bc9b", null ],
    [ "GetControllerType", "d2/d1f/a01361.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "d2/d1f/a01361.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "GetText", "d2/d1f/a01361.html#ad37cd855eec1e2636806ff274469540a", null ],
    [ "OnShow", "d2/d1f/a01361.html#a44c86b20c27f7a8ddb7c1337e9538f80", null ],
    [ "SetContent", "d2/d1f/a01361.html#a145033b88bc00b5e2dc75940a0b6ddb8", null ],
    [ "SetText", "d2/d1f/a01361.html#a47a3d1a4bd1182cef70d052c48bc5eb7", null ],
    [ "SetTextColor", "d2/d1f/a01361.html#a8a8a420577d32cf39a2ac11d1e97c537", null ],
    [ "dialog_text", "d2/d1f/a01361.html#a1a501a8971a2838045fd057e30e25b1e", null ],
    [ "m_Text", "d2/d1f/a01361.html#af5260a044e7e2e3996533ab172c2b342", null ],
    [ "m_TextController", "d2/d1f/a01361.html#a1f12cb6dca45dcd1307492b6b1fe8490", null ]
];