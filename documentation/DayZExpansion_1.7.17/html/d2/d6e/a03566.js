var a03566 =
[
    [ "ExpansionTerritoryMember", "d2/d6e/a03566.html#adfed12b763f5f6ce98224be09c98b401", null ],
    [ "GetID", "d2/d6e/a03566.html#a4a0d9f2aa7d54791414d927857c4afd4", null ],
    [ "GetName", "d2/d6e/a03566.html#a6342ce891c8bf7d86c0b770eb080ecb4", null ],
    [ "GetRank", "d2/d6e/a03566.html#ae303f000699d643270683d509b828389", null ],
    [ "GetRankName", "d2/d6e/a03566.html#a1c9bdfe1e67f0bf3336b8c47c03baf10", null ],
    [ "OnRecieve", "d2/d6e/a03566.html#a23fa0de567cc2cfea7bb0818a09dfb67", null ],
    [ "OnSend", "d2/d6e/a03566.html#a1892cc587f01dfacea350055a1603220", null ],
    [ "SetRank", "d2/d6e/a03566.html#a59cf7d6d7f2a3631dad86e9448af5b90", null ],
    [ "m_ID", "d2/d6e/a03566.html#acfb027af3b28689bd73d6eaf671a428f", null ],
    [ "m_Name", "d2/d6e/a03566.html#acca8064c0876917a0229c903625240cb", null ],
    [ "m_Rank", "d2/d6e/a03566.html#a615db3942b4959755b2eb373185fb489", null ]
];