var a06977 =
[
    [ "CanPickLock", "d6/de9/a06977.html#a20e92d823743920c5ce53c065f3119b5", null ],
    [ "DisableVehicleDamage", "d6/de9/a06977.html#a65af023deac32f2c3b282ac221956091", null ],
    [ "EnableHelicopterExplosions", "d6/de9/a06977.html#a3f8e0e31c54966f3785252e7dfc6059a", null ],
    [ "EnableTailRotorDamage", "d6/de9/a06977.html#ada3f6091b10f7f15d0e40c12f9734398", null ],
    [ "EnableWindAerodynamics", "d6/de9/a06977.html#ad01233d5cf16d23384c2f177b8254a59", null ],
    [ "MasterKeyPairingMode", "d6/de9/a06977.html#a9e4109a89b3c7801ddec5d980f38ec45", null ],
    [ "MasterKeyUses", "d6/de9/a06977.html#ad7490cfe69e0d4f70876230643f624c5", null ],
    [ "PickLockChancePercent", "d6/de9/a06977.html#a2f34421f536ee033407885fadcc9996a", null ],
    [ "PickLockTimeSeconds", "d6/de9/a06977.html#a7ca8c72e11a81dd9e8fc3538da674a87", null ],
    [ "PickLockToolDamagePercent", "d6/de9/a06977.html#add361440b99f70ce8a77113fc53739e9", null ],
    [ "PickLockTools", "d6/de9/a06977.html#ad6bd143367ddc919fe016e867464b4e0", null ],
    [ "PlayerAttachment", "d6/de9/a06977.html#a967fec4a40ce9d107b8c37f31b07d6d5", null ],
    [ "Towing", "d6/de9/a06977.html#a06a1dc90bcac11ee475a012fce1ba99f", null ],
    [ "VehicleCrewDamageMultiplier", "d6/de9/a06977.html#a5eb7baa05209f30e95e79883d5791917", null ],
    [ "VehicleLockedAllowInventoryAccess", "d6/de9/a06977.html#ab63e1fc1a38d230cb3694d23bd6485b2", null ],
    [ "VehicleLockedAllowInventoryAccessWithoutDoors", "d6/de9/a06977.html#a47241073d224f8f7e1ba0e113974fada", null ],
    [ "VehicleRequireAllDoors", "d6/de9/a06977.html#a39fd17c1c69bca6de3e1426de5a2dcf4", null ],
    [ "VehicleRequireKeyToStart", "d6/de9/a06977.html#aefc112240e649f80ca6bd7c340f7fe32", null ],
    [ "VehicleSpeedDamageMultiplier", "d6/de9/a06977.html#a156fb272a13bdac52f3a192baaa7d772", null ],
    [ "VehicleSync", "d6/de9/a06977.html#adc680391bfdbb3a3c1eee10c2ada0ea3", null ]
];