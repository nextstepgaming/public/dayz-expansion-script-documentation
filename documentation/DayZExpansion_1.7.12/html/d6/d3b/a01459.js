var a01459 =
[
    [ "ExpansionTerritoryMember", "d6/d3b/a01459.html#adfed12b763f5f6ce98224be09c98b401", null ],
    [ "GetID", "d6/d3b/a01459.html#a4a0d9f2aa7d54791414d927857c4afd4", null ],
    [ "GetName", "d6/d3b/a01459.html#a6342ce891c8bf7d86c0b770eb080ecb4", null ],
    [ "GetRank", "d6/d3b/a01459.html#ae303f000699d643270683d509b828389", null ],
    [ "GetRankName", "d6/d3b/a01459.html#a1c9bdfe1e67f0bf3336b8c47c03baf10", null ],
    [ "SetRank", "d6/d3b/a01459.html#a59cf7d6d7f2a3631dad86e9448af5b90", null ],
    [ "m_ID", "d6/d3b/a01459.html#acfb027af3b28689bd73d6eaf671a428f", null ],
    [ "m_Name", "d6/d3b/a01459.html#acca8064c0876917a0229c903625240cb", null ],
    [ "m_Rank", "d6/d3b/a01459.html#a615db3942b4959755b2eb373185fb489", null ]
];