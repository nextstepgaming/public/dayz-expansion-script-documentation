var a06469 =
[
    [ "CopyConfig", "d6/d1b/a06469.html#ac19e540fc2be384ddde2e5b3f733e04d", null ],
    [ "GetLastDailyResetTimeDiff", "d6/d1b/a06469.html#a1de616c2968e830f05cdff61d9a4bad7", null ],
    [ "GetLastWeeklyResetTimeDiff", "d6/d1b/a06469.html#aff6038e42cc2a3a96c5a44ad1076bd50", null ],
    [ "HasDailyResetCooldown", "d6/d1b/a06469.html#a41796bdabf9107a315618f56324c61d0", null ],
    [ "HasWeeklyResetCooldown", "d6/d1b/a06469.html#a8876a497c59286596568bdd12994479c", null ],
    [ "Load", "d6/d1b/a06469.html#a282bbedf59e00769e1bfc8696733b63d", null ],
    [ "Save", "d6/d1b/a06469.html#af59aa6f666e3d5b9204ab698c1b6e0ff", null ],
    [ "SetDailyResetTime", "d6/d1b/a06469.html#a57949613bf615db2e2f7857361aaa654", null ],
    [ "SetWeeklyResetTime", "d6/d1b/a06469.html#a62062703a8a8aa6caa46a2f0b531ef2c", null ],
    [ "CONFIGVERSION", "d6/d1b/a06469.html#aa4ffca8a2792420d7e2218608628b48b", null ],
    [ "DAY_TIME", "d6/d1b/a06469.html#a2b950596474fa455a2a744e8d076da8d", null ],
    [ "WEEK_TIME", "d6/d1b/a06469.html#aaec92cabe9ce47dffa1612a5b8467778", null ]
];