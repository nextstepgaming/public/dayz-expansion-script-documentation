var a01387 =
[
    [ "EnableTerritories", "d9/d40/a01387.html#aedf640cba106c100f32af8a7465cc366", null ],
    [ "MaxMembersInTerritory", "d9/d40/a01387.html#a8654d0d5a0b9fd4345e32ec3b98cf191", null ],
    [ "MaxTerritoryPerPlayer", "d9/d40/a01387.html#a24112f5cd29ccaf77f2fc83fcb489fdd", null ],
    [ "TerritorySize", "d9/d40/a01387.html#a8b195554e785fd14f42bce0694a2b5a6", null ],
    [ "UseWholeMapForInviteList", "d9/d40/a01387.html#a3df8acabbb2fb60f4310af013ec90805", null ]
];