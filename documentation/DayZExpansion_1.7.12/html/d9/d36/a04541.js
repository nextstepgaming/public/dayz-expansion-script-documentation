var a04541 =
[
    [ "Acquire", "d9/d36/a04541.html#a0fa6dd9b6dff8994771b40019d8a4f72", null ],
    [ "OnRecieve", "d9/d36/a04541.html#ac1b1f621ee7659cd6074838a7fb7195d", null ],
    [ "OnSend", "d9/d36/a04541.html#a6d9813824f447269d8491c1f1a6cd6f9", null ],
    [ "m_AnimalsKilled", "d9/d36/a04541.html#af4748e79098c2eb31f48a6b44bca72e6", null ],
    [ "m_Blood", "d9/d36/a04541.html#a386ac5c35fc67a66b02890cafefa9a0d", null ],
    [ "m_Distance", "d9/d36/a04541.html#aec7e4350a9cabbe2b98223e3a778212e", null ],
    [ "m_Energy", "d9/d36/a04541.html#a129670989b6a5ec8b1c0e4d1b7b15a68", null ],
    [ "m_HasBaseStats", "d9/d36/a04541.html#afeda392525a5dd89e7c01dc18202b48a", null ],
    [ "m_HasRegisteredStats", "d9/d36/a04541.html#a3d08f3e93f2febebe6c6f7d3e6a71de1", null ],
    [ "m_Health", "d9/d36/a04541.html#a3a47afe18588cf04099939866f667ac8", null ],
    [ "m_InfectedKilled", "d9/d36/a04541.html#aa71c542beac9afbab20246273c62ec8f", null ],
    [ "m_LongestShot", "d9/d36/a04541.html#a76446f08845e7e6e7ad723dd43858347", null ],
    [ "m_PlainID", "d9/d36/a04541.html#af1bdf892773fe32beec0cc4c81168651", null ],
    [ "m_PlayersKilled", "d9/d36/a04541.html#a3a49cfa8d3a7c66c0b9aa0e9f023c5cb", null ],
    [ "m_Playtime", "d9/d36/a04541.html#ac0f1de93577939499c55019e8bb9efe8", null ],
    [ "m_Stamina", "d9/d36/a04541.html#a12b7ca9d34af4572b7a8ef53eec449a2", null ],
    [ "m_Water", "d9/d36/a04541.html#a120b97f5f91be2878ac47c34c0ec4cd8", null ],
    [ "m_Weight", "d9/d36/a04541.html#ab1b157515d9af81bc5cf07a1c64785cd", null ]
];