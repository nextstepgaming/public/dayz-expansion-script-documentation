var expansionactiontoolbase_8c =
[
    [ "ExpansionActionToolBaseCB", "d4/d5b/class_expansion_action_tool_base_c_b.html", "d4/d5b/class_expansion_action_tool_base_c_b" ],
    [ "ActionCondition", "d9/d59/expansionactiontoolbase_8c.html#aa9aa4864fae11769c65e644b5fd97c66", null ],
    [ "CreateAndSetupActionCallback", "d9/d59/expansionactiontoolbase_8c.html#ad7c2b71ba925a43a0fef9ef4aa765963", null ],
    [ "ExpansionActionToolBase", "d9/d59/expansionactiontoolbase_8c.html#a2ec52ac05c8ad9e5ed942654ca2bf05b", null ],
    [ "GetActualTargetObject", "d9/d59/expansionactiontoolbase_8c.html#ab13031d4de848683f92141940303ee08", null ],
    [ "GetTargetItem", "d9/d59/expansionactiontoolbase_8c.html#a7e310183f43025ce658eacd370b5c9d5", null ],
    [ "OnEndAnimationLoopServer", "d9/d59/expansionactiontoolbase_8c.html#acda6d37054929cd6366f74e65d1fcb37", null ],
    [ "OnFinishProgressServer", "d9/d59/expansionactiontoolbase_8c.html#a00cda41fe9dd093059da5c4b83b591bc", null ],
    [ "OnStartAnimationLoopServer", "d9/d59/expansionactiontoolbase_8c.html#a498da9d7d9b0a9126f488206f249e2f2", null ],
    [ "Setup", "d9/d59/expansionactiontoolbase_8c.html#ae6a4a9c901dbed9f819534aac090ffdb", null ],
    [ "m_Cycles", "d9/d59/expansionactiontoolbase_8c.html#add986a8b0d3f724b01921fec4db98151", null ],
    [ "m_MinHealth01", "d9/d59/expansionactiontoolbase_8c.html#a5d58fa44f06a47200f78a92dedcb5efd", null ],
    [ "m_TargetName", "d9/d59/expansionactiontoolbase_8c.html#a4749cf45b5ac2fa0714864c058c69ef7", null ],
    [ "m_Time", "d9/d59/expansionactiontoolbase_8c.html#a251cefc67ac9028e215f99dcff20e945", null ],
    [ "m_ToolDamagePercent", "d9/d59/expansionactiontoolbase_8c.html#a51b79bdc2b37130cb81f4e4dece7bcc8", null ]
];