var a05977 =
[
    [ "CurrencyIcon", "d9/d20/a05977.html#a224fe5b506170e9a681fd122d8efa705", null ],
    [ "ItemBuyPrice", "d9/d20/a05977.html#a3feb19222a0e794395e826ee3e867211", null ],
    [ "ItemMarketStock", "d9/d20/a05977.html#a75e4971173e2d3f9e3f2d7d11660c622", null ],
    [ "ItemName", "d9/d20/a05977.html#adc5f29d5df69ac538f3cb06266f796ec", null ],
    [ "ItemPlayerStock", "d9/d20/a05977.html#a0016b482105398fb17861ca04d33249e", null ],
    [ "ItemSellPrice", "d9/d20/a05977.html#ad2dc60db930edc2ec547d4ed8dbc4acb", null ],
    [ "Preview", "d9/d20/a05977.html#ae50f0c92c58a1272684f8cbfdeef21f7", null ]
];