var expansionitemtooltip_8c =
[
    [ "ExpansionItemTooltip", "de/db4/class_expansion_item_tooltip.html", "de/db4/class_expansion_item_tooltip" ],
    [ "ExpansionItemTooltipStatElement", "dc/d54/class_expansion_item_tooltip_stat_element.html", "dc/d54/class_expansion_item_tooltip_stat_element" ],
    [ "ExpansionItemPreviewTooltip", "d9/db9/class_expansion_item_preview_tooltip.html", "d9/db9/class_expansion_item_preview_tooltip" ],
    [ "ItemCleanness", "d9/de4/expansionitemtooltip_8c.html#a7d3fafac1963f7b98aca9fc16f8bfa0a", null ],
    [ "ItemDamage", "d9/de4/expansionitemtooltip_8c.html#a59f633dc62af219ace9e41b24c17029d", null ],
    [ "ItemDescription", "d9/de4/expansionitemtooltip_8c.html#a8cadaadd7312c29a2800b73925cb7399", null ],
    [ "ItemFoodStage", "d9/de4/expansionitemtooltip_8c.html#a6f02b995e286df14f9e72ed636e9ced9", null ],
    [ "ItemLiquidType", "d9/de4/expansionitemtooltip_8c.html#a40a4464b7361d85df0d5dd2ba3cccc94", null ],
    [ "ItemName", "d9/de4/expansionitemtooltip_8c.html#ac2b0050eea85abb5a689375625686d09", null ],
    [ "ItemPreview", "d9/de4/expansionitemtooltip_8c.html#a52eb90453e41dd29e6a6675ad82f4328", null ],
    [ "ItemQuantity", "d9/de4/expansionitemtooltip_8c.html#a9eb87c575177738fde1778a7bdbc7168", null ],
    [ "ItemStatsElements", "d9/de4/expansionitemtooltip_8c.html#a56f218ee196d8986a9860166c647637f", null ],
    [ "ItemTemperature", "d9/de4/expansionitemtooltip_8c.html#ad58844540d2539d26eb6ba55c7d1a771", null ],
    [ "ItemWeight", "d9/de4/expansionitemtooltip_8c.html#a4e6700e5e958e0a05ca2b7fb955efb18", null ],
    [ "ItemWetness", "d9/de4/expansionitemtooltip_8c.html#a47fa0319b5dc58830a4c7a6ff151f624", null ],
    [ "StatText", "d9/de4/expansionitemtooltip_8c.html#aa908d5e6d200a25caa712376d7f5131a", null ]
];