var class_expansion_book_menu_tab_server_info_controller =
[
    [ "Descriptions", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#ab949389c6aaece18e473b9be493ecc2a", null ],
    [ "ServerAdress", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#ab317389fc983a24f776c63c7e6cfe724", null ],
    [ "ServerDescription", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#a77288a993b332b355e466db70d8cd6d5", null ],
    [ "ServerName", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#ab0fc2affff16e336aab5f444e81ca949", null ],
    [ "ServerSocial", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#a7202789ae4a221281aec79b53e4bd0fa", null ],
    [ "SettingsCategories", "d9/d3a/class_expansion_book_menu_tab_server_info_controller.html#ae6e6cadded99abf2b02ecf41c995ccad", null ]
];