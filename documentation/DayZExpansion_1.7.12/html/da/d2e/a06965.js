var a06965 =
[
    [ "CalculateAngularVelocity", "da/d2e/a06965.html#a35046a7c75a895b4c1eb6849df12f9d6", null ],
    [ "CalculateBuoyancyAtPosition", "da/d2e/a06965.html#aa52fedde3a29f236c2e90f4ae9be00c6", null ],
    [ "CalculateVelocity", "da/d2e/a06965.html#a2c85aca80eaeeacc52a022676292f74b", null ],
    [ "ComputeImpulseDenominator", "da/d2e/a06965.html#a863c9beea8ca782e8583861c7b432303", null ],
    [ "GetVelocityInLocal", "da/d2e/a06965.html#ad7526f831bcdc6f474420f71250f3551", null ],
    [ "IntegrateTransform", "da/d2e/a06965.html#ae3d1a2328db9d555ac2d2147963b89e9", null ],
    [ "MatrixToQuat", "da/d2e/a06965.html#a6cc8037f3f4fe94c61de1449b071099e", null ],
    [ "QuatAdd", "da/d2e/a06965.html#a6bd3fe88885aa2d4ca2b0e80f84b35aa", null ],
    [ "QuatAxis", "da/d2e/a06965.html#a8f543a899ba4e78ec4eadb75b5636a1b", null ],
    [ "QuatAxis", "da/d2e/a06965.html#a0c6c071501704b649de59d8b9aa78948", null ],
    [ "QuatDot", "da/d2e/a06965.html#ae73a9dc44a984c1f3910c3f9a34d0176", null ],
    [ "QuatInverse", "da/d2e/a06965.html#aa82f71c1e0aadbf2c0ce8ddba0c01439", null ],
    [ "QuatMultiply", "da/d2e/a06965.html#af52f442786c3fd5ed367c4c233912ede", null ],
    [ "QuatNearest", "da/d2e/a06965.html#afc91d533811f62d061f92ae66cb9961f", null ],
    [ "QuatNormalize", "da/d2e/a06965.html#a7183b328c5c50dfbc61043cd11ecf3c6", null ],
    [ "QuatSlerp", "da/d2e/a06965.html#a0cc733fea1e40a1b28da2fe4d7205741", null ],
    [ "QuatSub", "da/d2e/a06965.html#aff70ba7e1ca6d2593e368ba6c38b28cf", null ],
    [ "QuatToAxis", "da/d2e/a06965.html#af2e73f5d3e84708bb21854eb9ea16ced", null ],
    [ "QuatToAxisAndAngle", "da/d2e/a06965.html#a7488deaed63bb21b4bbbb510cd705314", null ],
    [ "ResolveSingleBilateral", "da/d2e/a06965.html#abe3b3c1f3ad2dd332d5429979b9bffc5", null ]
];