var class_expansion_zone =
[
    [ "ExpansionZone", "da/d38/class_expansion_zone.html#a9d70294768588fea361c032c94399773", null ],
    [ "~ExpansionZone", "da/d38/class_expansion_zone.html#ae87ce85b8436b35da247f967579a82cd", null ],
    [ "Check", "da/d38/class_expansion_zone.html#a6f4427aadcf62433cc92aa1f65fa75a0", null ],
    [ "ToStr", "da/d38/class_expansion_zone.html#a24045b560e6105869508de726216b994", null ],
    [ "COUNT", "da/d38/class_expansion_zone.html#adf8ffb3f6b7a617784cc53a0410aee16", null ],
    [ "m_ID", "da/d38/class_expansion_zone.html#abb5ac10fa8f40013162a1ddfc6a7194a", null ],
    [ "m_Next", "da/d38/class_expansion_zone.html#a7baae5f6f64f5ac6e156adca3ebf1ba2", null ],
    [ "m_Prev", "da/d38/class_expansion_zone.html#ac57aca7275403c4db68cec5faa7ab474", null ],
    [ "m_Type", "da/d38/class_expansion_zone.html#a0aeccba03ffbdde1fd63a03affc93bf1", null ],
    [ "s_InsideBuffer", "da/d38/class_expansion_zone.html#a08e557a3f03d20c156f87b0c7c289b7a", null ]
];