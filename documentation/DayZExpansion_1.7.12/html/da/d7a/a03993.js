var a03993 =
[
    [ "ExpansionBookMenuTabNotes", "da/d7a/a03993.html#afd661e227cabd05468786dc3f9afa03b", null ],
    [ "CanShow", "da/d7a/a03993.html#ae3457b90febfe56ad3b3a4f3a7f855a8", null ],
    [ "GetControllerType", "da/d7a/a03993.html#a06a5e8d8cbaec450004d53bc6847a6ba", null ],
    [ "GetLayoutFile", "da/d7a/a03993.html#a9bed6b4b4ef5df1312644e07f9061047", null ],
    [ "GetTabColor", "da/d7a/a03993.html#a8e610a0107222c7f10e7805f880efff9", null ],
    [ "GetTabIconName", "da/d7a/a03993.html#aa83ee8136b3520423326ab4364a4e589", null ],
    [ "GetTabName", "da/d7a/a03993.html#aa3e4f5e93d8039116e221651b0811dfe", null ],
    [ "IgnoreBackButtonBase", "da/d7a/a03993.html#ae92de5966a5b2130c0a69cdcee133a74", null ],
    [ "IsParentTab", "da/d7a/a03993.html#ad8f7d24b92632ad273cc5ec834e6a914", null ],
    [ "OnBackButtonClick", "da/d7a/a03993.html#a9edd1f00a95fa1732dc7852f30703bd2", null ],
    [ "OnHide", "da/d7a/a03993.html#a827024c229b8262abb96d447c0fc1b97", null ],
    [ "OnMouseEnter", "da/d7a/a03993.html#affafcd9e4634cc0347c2f0a6dcd09823", null ],
    [ "OnMouseLeave", "da/d7a/a03993.html#a77fff6264dd5fd37948bdb4d0d84c638", null ],
    [ "OnShow", "da/d7a/a03993.html#ae6dc2b29b2b665b5525ad14f67fd0b12", null ],
    [ "SetView", "da/d7a/a03993.html#a1745568b31eef282f2a750617565f52b", null ],
    [ "m_NotesTabController", "da/d7a/a03993.html#a0caceaf49ec326c29bd5004e9bc5df41", null ]
];