var expansionsafezonesettings_8c =
[
    [ "ExpansionSafeZoneSettingsBase", "d4/d06/class_expansion_safe_zone_settings_base.html", "d4/d06/class_expansion_safe_zone_settings_base" ],
    [ "ExpansionSafeZoneSettings", "d5/d5a/class_expansion_safe_zone_settings.html", "d5/d5a/class_expansion_safe_zone_settings" ],
    [ "CircleZones", "da/d87/expansionsafezonesettings_8c.html#a80f12b968e61dc48ca4488fde825bd97", null ],
    [ "Enabled", "da/d87/expansionsafezonesettings_8c.html#a558f5c44426d0eb7abb82a65e8892d9a", null ],
    [ "EnableVehicleinvincibleInsideSafeZone", "da/d87/expansionsafezonesettings_8c.html#a4a24cd99d951245c0e4c49551c1d7418", null ],
    [ "FrameRateCheckSafeZoneInMs", "da/d87/expansionsafezonesettings_8c.html#a63fa878491bbb94f0bb5d95f02735cb2", null ],
    [ "PolygonZones", "da/d87/expansionsafezonesettings_8c.html#a95f36a14026d170362326ba1c56e4444", null ]
];