var expansionbasebuildingsettings_8c =
[
    [ "ExpansionBaseBuildingSettingsBase", "d8/d41/class_expansion_base_building_settings_base.html", "d8/d41/class_expansion_base_building_settings_base" ],
    [ "ExpansionBaseBuildingSettingsV2", "d3/d2a/class_expansion_base_building_settings_v2.html", "d3/d2a/class_expansion_base_building_settings_v2" ],
    [ "Copy", "d0/dc7/expansionbasebuildingsettings_8c.html#a8bc9f37c8b51353bb7abe4a847ec9846", null ],
    [ "CopyInternal", "d0/dc7/expansionbasebuildingsettings_8c.html#ad911f0b3a2f61957d9b35f7aba2018a9", null ],
    [ "CopyInternal", "d0/dc7/expansionbasebuildingsettings_8c.html#a26e72d08f43db7e781557887121f3b81", null ],
    [ "DefaultChernarusNonBuildingZones", "d0/dc7/expansionbasebuildingsettings_8c.html#a70e4379bd98898b2ba8179f5a7315046", null ],
    [ "DefaultNamalskNonBuildingZones", "d0/dc7/expansionbasebuildingsettings_8c.html#a6fd156b6a83a24fd4264bb0e5eee05ce", null ],
    [ "Defaults", "d0/dc7/expansionbasebuildingsettings_8c.html#ac06ab75f9e6f636d8b7c17685037667e", null ],
    [ "DefaultTakistanNonBuildingZones", "d0/dc7/expansionbasebuildingsettings_8c.html#a2674bb22ea3469f4dfcc176ef371f4ff", null ],
    [ "ExpansionBaseBuildingSettings", "d0/dc7/expansionbasebuildingsettings_8c.html#a161effbd9c2e9a5015a7151e8a940110", null ],
    [ "GetNoBuildZoneWhitelist", "d0/dc7/expansionbasebuildingsettings_8c.html#a456cd5bd33c13ad607ed08d312e7bccd", null ],
    [ "IsLoaded", "d0/dc7/expansionbasebuildingsettings_8c.html#ad9eab3191d8b7cc0f5414602631292b8", null ],
    [ "OnLoad", "d0/dc7/expansionbasebuildingsettings_8c.html#a3578afd97750bc428c2737e242dffd3d", null ],
    [ "OnRecieve", "d0/dc7/expansionbasebuildingsettings_8c.html#ae3e27fdaa6ddb0d424626c1f0b3aad2e", null ],
    [ "OnSave", "d0/dc7/expansionbasebuildingsettings_8c.html#a9fb9cc278f9d90f49e9dd9ce5ce40d43", null ],
    [ "OnSend", "d0/dc7/expansionbasebuildingsettings_8c.html#af2c207d1a6c5eea63c6296925ace1d76", null ],
    [ "Send", "d0/dc7/expansionbasebuildingsettings_8c.html#a8f26e3b21fa656c6d6048f82cfb30a72", null ],
    [ "SettingName", "d0/dc7/expansionbasebuildingsettings_8c.html#a46ab2e61c4bb8c8b19c3aece63908a50", null ],
    [ "Unload", "d0/dc7/expansionbasebuildingsettings_8c.html#a064d4576e4b054e72d5de632a7d09a63", null ],
    [ "AllowBuildingWithoutATerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#ab51017be8dd7106b1f81b128ecc93aa0", null ],
    [ "AutomaticFlagOnCreation", "d0/dc7/expansionbasebuildingsettings_8c.html#a4f700fbdc33c070115caa77b631431b2", null ],
    [ "BuildZoneRequiredCustomMessage", "d0/dc7/expansionbasebuildingsettings_8c.html#a351f124cc339c60fc9385ee8739eeb2c", null ],
    [ "CanAttachCodelock", "d0/dc7/expansionbasebuildingsettings_8c.html#ae108a14b484886e76c2b548b05a8e30a", null ],
    [ "CanBuildAnywhere", "d0/dc7/expansionbasebuildingsettings_8c.html#a51f5a5a10933fe95e18fb48cec1bbc0c", null ],
    [ "CanCraftExpansionBasebuilding", "d0/dc7/expansionbasebuildingsettings_8c.html#ace13b58b2b5e0cbabd37a53cfeb0e567", null ],
    [ "CanCraftTerritoryFlagKit", "d0/dc7/expansionbasebuildingsettings_8c.html#a9a3c7e8bc64226857d5f659f98c85d8f", null ],
    [ "CanCraftVanillaBasebuilding", "d0/dc7/expansionbasebuildingsettings_8c.html#a88411b337d3c056629231f842cda3c63", null ],
    [ "CodelockActionsAnywhere", "d0/dc7/expansionbasebuildingsettings_8c.html#a6b438f8f437f9ad7ab5c0d2f064dc2fe", null ],
    [ "CodelockAttachMode", "d0/dc7/expansionbasebuildingsettings_8c.html#a91ef73c732fa0b08438b95f7a580d60c", null ],
    [ "CodeLockLength", "d0/dc7/expansionbasebuildingsettings_8c.html#a7f238076c7491d8f2b8fc5c920ca335c", null ],
    [ "DamageWhenEnterWrongCodeLock", "d0/dc7/expansionbasebuildingsettings_8c.html#a7c64405c02e3c26f4bc740de26837013", null ],
    [ "DeployableInsideAEnemyTerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#ad00018a1f3626faeac87d3c139ccf1e5", null ],
    [ "DeployableOutsideATerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#a4b0835727152029a680341f0ef7054da", null ],
    [ "DestroyFlagOnDismantle", "d0/dc7/expansionbasebuildingsettings_8c.html#ad354c56cce2cbedb617cf9301d53a37e", null ],
    [ "DismantleAnywhere", "d0/dc7/expansionbasebuildingsettings_8c.html#accd27851720c4bf7ac77436c7075a6fa", null ],
    [ "DismantleFlagMode", "d0/dc7/expansionbasebuildingsettings_8c.html#ab80ac484189f4e52187e1651eb910a98", null ],
    [ "DismantleFlagRequireTools", "d0/dc7/expansionbasebuildingsettings_8c.html#a62d379b3db291fa8736290b086f22291", null ],
    [ "DismantleInsideTerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#a876eb1ff267b8e96b5358a591ade584e", null ],
    [ "DismantleOutsideTerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#abd7dbb0454aacb1115e8d1622777564c", null ],
    [ "DoDamageWhenEnterWrongCodeLock", "d0/dc7/expansionbasebuildingsettings_8c.html#ae1dd131b619e8b6478fc42fe7ed766b4", null ],
    [ "EnableFlagMenu", "d0/dc7/expansionbasebuildingsettings_8c.html#a1bac37781b0af2fbe2061cbb3d3a97d4", null ],
    [ "FlagMenuMode", "d0/dc7/expansionbasebuildingsettings_8c.html#a3fa6e329182a02beff6e6f4941bc99be", null ],
    [ "GetTerritoryFlagKitAfterBuild", "d0/dc7/expansionbasebuildingsettings_8c.html#a23b7910721ff7608df0e59475bb11dc8", null ],
    [ "m_IsLoaded", "d0/dc7/expansionbasebuildingsettings_8c.html#a5521ad2d5fb3360b7943c9cbc422642a", null ],
    [ "RememberCode", "d0/dc7/expansionbasebuildingsettings_8c.html#a626ff3f33ec9a6723c7f3af63f2eede7", null ],
    [ "SimpleTerritory", "d0/dc7/expansionbasebuildingsettings_8c.html#aca80aa73a886ece945f164dbc3cea875", null ],
    [ "VERSION", "d0/dc7/expansionbasebuildingsettings_8c.html#ae48ea820d6b3acc7353b763034c772af", null ],
    [ "Zones", "d0/dc7/expansionbasebuildingsettings_8c.html#a1a2efc847e1903dfbe7b7331b5e5d4a4", null ],
    [ "ZonesAreNoBuildZones", "d0/dc7/expansionbasebuildingsettings_8c.html#acea5182dbc333164751f61c59a27c6de", null ]
];