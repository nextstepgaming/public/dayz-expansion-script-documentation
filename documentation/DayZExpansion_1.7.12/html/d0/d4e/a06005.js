var a06005 =
[
    [ "AddAttachment", "d0/d4e/a06005.html#a058b0494700bef360eef6b32b27d10d9", null ],
    [ "GetAttachments", "d0/d4e/a06005.html#a54bd92a89b4c6bed09cc403bf7c2cbd2", null ],
    [ "LoadItemPreset", "d0/d4e/a06005.html#a889e0199e4b82c23d6253486d99d204f", null ],
    [ "RemoveAttachment", "d0/d4e/a06005.html#afa8046e43432b1298708b2966f1c8c55", null ],
    [ "SaveItemPreset", "d0/d4e/a06005.html#a426dfb34f544ce8b6b682bef708f80f5", null ],
    [ "ClassName", "d0/d4e/a06005.html#aa5d6b5634d5c002845aaedfbab12067c", null ],
    [ "ItemAttachments", "d0/d4e/a06005.html#ad78693d77a3fc3c756be1aeb02df13b7", null ],
    [ "PresetName", "d0/d4e/a06005.html#a6fc49c85cd587971b61a367e18ac85e5", null ]
];