var a04277 =
[
    [ "Quaternion", "d0/d08/a04277.html#aae30469c2439185896ed556b37edb812", null ],
    [ "Add", "d0/d08/a04277.html#a942b7fe7afa4d760f75b07e14f09820b", null ],
    [ "AddSelf", "d0/d08/a04277.html#a3937e33bae8535505e9a6b6a74a7d0c5", null ],
    [ "Multiply", "d0/d08/a04277.html#ab1215a1f1918a1ac71f333b862ab9956", null ],
    [ "MultiplySelf", "d0/d08/a04277.html#a09969be58a5f5c4d1f24f1032f6edc3c", null ],
    [ "Rotation", "d0/d08/a04277.html#a0bacb752a046b906d787beb252b0d8ae", null ],
    [ "SetRotation", "d0/d08/a04277.html#a2e7e7008a293d2e11698205ee612d3c4", null ],
    [ "ToAngles", "d0/d08/a04277.html#aadf46acc8e4e4950ef1a608eddf6aeff", null ],
    [ "ToMatrix", "d0/d08/a04277.html#a0207ecf08bbd58536c9de04afc70a1cd", null ],
    [ "Update", "d0/d08/a04277.html#ae61f269d43ec60a61e17c627d0226ee6", null ],
    [ "data", "d0/d08/a04277.html#a727473a1ed1f49125865f71bb5adfd6d", null ],
    [ "m_trans", "d0/d08/a04277.html#ac4601a46ee540902ccb0cda905386d49", null ]
];