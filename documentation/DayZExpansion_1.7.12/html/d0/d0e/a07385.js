var a07385 =
[
    [ "ExpansionWheelBase", "d0/d0e/a07385.html#aea5c9bf832d86d2f13862c62d136938b", null ],
    [ "EEKilled", "d0/d0e/a07385.html#ac68b9f08bf4eeb7ad4501ec2947e4cdc", null ],
    [ "EEKilled", "d0/d0e/a07385.html#ac68b9f08bf4eeb7ad4501ec2947e4cdc", null ],
    [ "Expansion_CarContactActivates", "d0/d0e/a07385.html#a46c1e78eb0ce7a23a2c7f677dfbb614b", null ],
    [ "GetMeleeTargetType", "d0/d0e/a07385.html#a14ceab4abbea01d678154beddf984288", null ],
    [ "GetMeleeTargetType", "d0/d0e/a07385.html#a14ceab4abbea01d678154beddf984288", null ],
    [ "SetActions", "d0/d0e/a07385.html#afaf6edd0d5561306afb4dc2ce774daad", null ],
    [ "SetActions", "d0/d0e/a07385.html#afaf6edd0d5561306afb4dc2ce774daad", null ],
    [ "m_Friction", "d0/d0e/a07385.html#ac08b60c89b94147e3e1c4baa68613b29", null ],
    [ "m_Mass", "d0/d0e/a07385.html#a6c507f99412cc9a08387015a48f910c3", null ],
    [ "m_Radius", "d0/d0e/a07385.html#a3ec8d96113a0bd814081ba3cfd04fe59", null ],
    [ "m_TyreRollDrag", "d0/d0e/a07385.html#a3e4b2054a3dc04de47e2eb80548b30ab", null ],
    [ "m_TyreRollResistance", "d0/d0e/a07385.html#a2a7601a9f58e7482714080bbdfeac234", null ],
    [ "m_TyreRoughness", "d0/d0e/a07385.html#a36b6306368002354e1365a6875c65707", null ],
    [ "m_TyreTread", "d0/d0e/a07385.html#ada4971542aa99742c3f7a6dad0e84846", null ],
    [ "m_Width", "d0/d0e/a07385.html#a0cce0fa7541e2d73cc677e20bbcb63ea", null ]
];