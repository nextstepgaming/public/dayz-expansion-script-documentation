var a07621 =
[
    [ "~Expansion_Laser_Base", "d0/dca/a07621.html#a3b3ee0445e477ae1bc90790caf5a15e0", null ],
    [ "CanDisplayAttachmentSlot", "d0/dca/a07621.html#a8037e2bad8724edc631c59022b24a314", null ],
    [ "CanPutAsAttachment", "d0/dca/a07621.html#a3c021bbbef5f36c770ae8ffd2a6cfd69", null ],
    [ "CheckNVGState", "d0/dca/a07621.html#a9f3acf5607ea1811d63c0777e3d14738", null ],
    [ "Expansion_Laser_Base", "d0/dca/a07621.html#a0e3d251693b09714c6f63eccca63ccd9", null ],
    [ "GetDistance", "d0/dca/a07621.html#a6691fa17a5543a99d85418adb962aa4d", null ],
    [ "InvisibleColor", "d0/dca/a07621.html#ac5bc858edf5e560101fd910a887a21bc", null ],
    [ "IsLaserIR", "d0/dca/a07621.html#a1eb16ea8c5808daebc28602bc27c5001", null ],
    [ "IsVisibleWithoutNVG", "d0/dca/a07621.html#a1a1b005bdbe66e51e4b1a4345d064a7b", null ],
    [ "LaserColor", "d0/dca/a07621.html#ac66d62acaf8c0146ae6f5c36dd6b61dc", null ],
    [ "LaserLightColor", "d0/dca/a07621.html#a209dd62ba61c7fa680fbe4dec3bc7dd3", null ],
    [ "LaserMaterial", "d0/dca/a07621.html#a192496f6c5ee855c6ae3d99311a7fc91", null ],
    [ "OnWorkStart", "d0/dca/a07621.html#a7dcc527023fb1ea147b78626b9a0f492", null ],
    [ "OnWorkStop", "d0/dca/a07621.html#a3338e37c884a6090632a33faf9551614", null ],
    [ "SetActions", "d0/dca/a07621.html#afe540210f40eae930e3a3fc60767e7ba", null ],
    [ "UpdateLaser", "d0/dca/a07621.html#ad7e85ffe48ddf8469f69d713f94f715a", null ],
    [ "m_Laser", "d0/dca/a07621.html#ab44cd408514d703d5bddaa2ee4f0ae2e", null ]
];