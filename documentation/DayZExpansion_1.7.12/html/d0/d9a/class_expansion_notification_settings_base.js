var class_expansion_notification_settings_base =
[
    [ "EnableNotification", "d0/d9a/class_expansion_notification_settings_base.html#af550e67f1c13cdc5a9447f48a0daa3ed", null ],
    [ "JoinMessageType", "d0/d9a/class_expansion_notification_settings_base.html#a0a48112708031000770a7ad1ff9c7cf6", null ],
    [ "LeftMessageType", "d0/d9a/class_expansion_notification_settings_base.html#a1740232c122bb5a8e1a876a01efa9e7e", null ],
    [ "ShowPlayerJoinServer", "d0/d9a/class_expansion_notification_settings_base.html#a6d47f46c9f78b4ec1b407aa20ba82881", null ],
    [ "ShowPlayerLeftServer", "d0/d9a/class_expansion_notification_settings_base.html#a580d304b217241b90df036f7e09b49ce", null ]
];