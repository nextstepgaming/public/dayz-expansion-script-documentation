var a03725 =
[
    [ "ExpansionCamoBox", "d0/d69/a03725.html#ac5d1c8d3608a2f67115325a55a75f309", null ],
    [ "~ExpansionCamoBox", "d0/d69/a03725.html#a2a802bf4f3514daee5098e543bc67f6c", null ],
    [ "CanBeDamaged", "d0/d69/a03725.html#aa646ffea25014114e375ddf0319a5ef3", null ],
    [ "CanObstruct", "d0/d69/a03725.html#a7067cee5caf08404ed9bae0fa9358fa0", null ],
    [ "CanPutInCargo", "d0/d69/a03725.html#a0065925566736d28f5da5d6ef55b9ffe", null ],
    [ "CanPutIntoHands", "d0/d69/a03725.html#acdb576e0338457f5127f7496f180384f", null ],
    [ "GetConstructionKitType", "d0/d69/a03725.html#a8b7cc96343ce5868caf6907b53acf7f2", null ],
    [ "IsInventoryVisible", "d0/d69/a03725.html#a4170fe7cccfa92bd01151c7a3e0f24ea", null ],
    [ "SetPartsAfterStoreLoad", "d0/d69/a03725.html#a4ecc7409844e71e4d196a499f309b76a", null ],
    [ "m_CanBeDamaged", "d0/d69/a03725.html#aa4ea2abcee39db40acfacea8311d813c", null ]
];