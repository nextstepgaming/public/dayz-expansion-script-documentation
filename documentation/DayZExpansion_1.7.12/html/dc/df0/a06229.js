var a06229 =
[
    [ "ExpansionTooltipPlayerListEntry", "dc/df0/a06229.html#a9b06c51400f0296daa17f6fdfb7be0a0", null ],
    [ "~ExpansionTooltipPlayerListEntry", "dc/df0/a06229.html#a46abc752deead3094fa7aacf74b0f300", null ],
    [ "GetControllerType", "dc/df0/a06229.html#a5e2442c2e4b2e8fc56aeba44cb628d3b", null ],
    [ "GetLayoutFile", "dc/df0/a06229.html#a1775ab8fbc6c18c04fc64bd666912bfa", null ],
    [ "OnHide", "dc/df0/a06229.html#a9637c0fc5764b2b3bb8bcbec99fab091", null ],
    [ "OnShow", "dc/df0/a06229.html#a177b81d3bf930815aba7962a692b4dc9", null ],
    [ "OnTooltipDataRecived", "dc/df0/a06229.html#a6b6bdf3118f0345c9f0cf302275d89c0", null ],
    [ "SetStats", "dc/df0/a06229.html#a19c88f84042880431fe51370ac66fbc7", null ],
    [ "m_PlayerID", "dc/df0/a06229.html#a4c40167e6c5b9c5d7d9f27caf11b0f51", null ],
    [ "m_PlayerPlainID", "dc/df0/a06229.html#ae0051acd65bedf182a074d2324f4e90a", null ],
    [ "m_StatsRequested", "dc/df0/a06229.html#ac31d22f843753302afa55c3734f81360", null ],
    [ "m_Title", "dc/df0/a06229.html#a6f0d92535cddc411377bac3840df8bcc", null ]
];