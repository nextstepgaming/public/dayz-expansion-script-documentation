var a06433 =
[
    [ "ClassName", "dc/d99/a06433.html#abb2108e9960494b4d26f808a3ba58f99", null ],
    [ "ConfigVersion", "dc/d99/a06433.html#a1e91b7b3f0281a89e6210f5675f28e24", null ],
    [ "DefaultNPCText", "dc/d99/a06433.html#a98baeb9db9affc8b3c12d877a4d1b59f", null ],
    [ "ID", "dc/d99/a06433.html#a35b974a1fa597d6b35c8dae5ac83703e", null ],
    [ "IsAI", "dc/d99/a06433.html#a9a4ee71285be0a72fcb34ad581bf80b7", null ],
    [ "NPCName", "dc/d99/a06433.html#a788751c617cd37dbe137ba5acb6ea34b", null ],
    [ "Orientation", "dc/d99/a06433.html#a9a0e30a52b7bbb4bfa4b239515d1ead2", null ],
    [ "Position", "dc/d99/a06433.html#ad58ce3800fc6d93a5927077085fbad9c", null ],
    [ "QuestIDs", "dc/d99/a06433.html#a1d94b55bae72e30b6e896e1fdd87d2f1", null ]
];