var a00764 =
[
    [ "ExpansionClientUIMarkerSize", "d3/d2c/a00764.html#af5935b7f5195c947a73d2af5c768f99c", [
      [ "VERYSMALL", "d3/d2c/a00764.html#af5935b7f5195c947a73d2af5c768f99ca4739bee0b4de52f0638fde60adb00ff5", null ],
      [ "SMALL", "d3/d2c/a00764.html#af5935b7f5195c947a73d2af5c768f99caea5e596a553757a677cb4da4c8a1f935", null ],
      [ "MEDIUM", "d3/d2c/a00764.html#af5935b7f5195c947a73d2af5c768f99ca5340ec7ecef6cc3886684a3bd3450d64", null ],
      [ "LARGE", "d3/d2c/a00764.html#af5935b7f5195c947a73d2af5c768f99ca716db5c72140446e5badac4683610310", null ]
    ] ]
];