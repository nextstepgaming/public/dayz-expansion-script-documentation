var class_weapon___base =
[
    [ "CanPutAsAttachment", "d3/d9c/class_weapon___base.html#a457cc3f2d74f0ca6f01c1f71edb2d187", null ],
    [ "CanPutInCargo", "d3/d9c/class_weapon___base.html#a2fb82740ee773dea54347e00c2e60bd6", null ],
    [ "CanPutIntoHands", "d3/d9c/class_weapon___base.html#a93da219121114dfbc437b7dadaee9440", null ],
    [ "Expansion_HasAmmo", "d3/d9c/class_weapon___base.html#a7fee48bede708d806c31b8b4e48253e7", null ],
    [ "Expansion_IsChambered", "d3/d9c/class_weapon___base.html#a11fe0f2f0f63f5487e5446525cdec53a", null ],
    [ "ExpansionGetMagAttachedFSMStateID", "d3/d9c/class_weapon___base.html#a25237554229a3359715eb5b4fd59598c", null ],
    [ "ExpansionGetMagAttachedFSMStateID", "d3/d9c/class_weapon___base.html#a25237554229a3359715eb5b4fd59598c", null ]
];