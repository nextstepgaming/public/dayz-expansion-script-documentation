var a04737 =
[
    [ "ExpansionPhysicsStructure", "d3/de8/a04737.html#a42fc3a16fb92e038dc7f06b9ad4191ed", null ],
    [ "~ExpansionPhysicsStructure", "d3/de8/a04737.html#addf87d513bb135272f68a8147bcc0ff8", null ],
    [ "AdjustPosition", "d3/de8/a04737.html#a2a905953b9b1099c65bae89b85a8f3d3", null ],
    [ "Create", "d3/de8/a04737.html#aeab85092e6ec9ac25818f16eb8ddb717", null ],
    [ "EOnContact", "d3/de8/a04737.html#a677a4a7a0100c0bf258cf613a2cc8909", null ],
    [ "Expansion_CarContactActivates", "d3/de8/a04737.html#ac5112f77aa9aacb4b35eacaf1b70303a", null ],
    [ "ExpansionGetPhysicsType", "d3/de8/a04737.html#ac8c5c165b766c48a9a5c642200132ec3", null ],
    [ "OnCreate", "d3/de8/a04737.html#a986a97c47054d00b9147cbf2908c10ec", null ],
    [ "OnDestroy", "d3/de8/a04737.html#a701a99b96999b1afd9aa5659468a6591", null ],
    [ "OnVariablesSynchronized", "d3/de8/a04737.html#aef5536ebf653b22b8eaf0c154f079267", null ],
    [ "m_BakedMapObject", "d3/de8/a04737.html#a90a40e1fd4ead1845ea7db368dbbdd70", null ],
    [ "m_PosX", "d3/de8/a04737.html#affc79762d4c1957d6f51d6f01d779d63", null ],
    [ "m_PosY", "d3/de8/a04737.html#a816ddc3f6ff3ad5636a0e5617c4d6ad3", null ],
    [ "m_PosZ", "d3/de8/a04737.html#a60f26c2634995f1ce5d34c5616f5eced", null ],
    [ "m_Transform", "d3/de8/a04737.html#afd99c05a34217045be63f87d5308fd0b", null ]
];