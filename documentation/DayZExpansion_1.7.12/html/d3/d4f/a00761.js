var a00761 =
[
    [ "ExpansionClientUIChatSize", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4", [
      [ "VERYSMALL", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4a4739bee0b4de52f0638fde60adb00ff5", null ],
      [ "SMALL", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4aea5e596a553757a677cb4da4c8a1f935", null ],
      [ "MEDIUM", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4a5340ec7ecef6cc3886684a3bd3450d64", null ],
      [ "LARGE", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4a716db5c72140446e5badac4683610310", null ],
      [ "VERYLARGE", "d3/d4f/a00761.html#ab92d3614fbd67fb0e8d93616b84ad4f4abc799d9c9fba472208eb3c4939961074", null ]
    ] ]
];