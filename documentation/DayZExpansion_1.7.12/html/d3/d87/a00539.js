var a00539 =
[
    [ "ExpansionBookSettingsBase", "d8/d46/a03881.html", "d8/d46/a03881" ],
    [ "ExpansionBookSettings", "d7/d0d/a03885.html", "d7/d0d/a03885" ],
    [ "ExpansionRulesCategory", "d5/da2/a03889.html", "d5/da2/a03889" ],
    [ "ExpansionRuleButton", "dc/d61/a03893.html", "dc/d61/a03893" ],
    [ "ExpansionRuleSection", "db/dda/a03897.html", "db/dda/a03897" ],
    [ "ExpansionServerInfos", "df/dee/a03901.html", "df/dee/a03901" ],
    [ "ExpansionServerInfoButtonData", "d6/de4/a03905.html", "d6/de4/a03905" ],
    [ "ExpansionServerInfoSection", "d7/d9a/a03909.html", "d7/d9a/a03909" ],
    [ "ExpansionRule", "d9/d8e/a03913.html", "d9/d8e/a03913" ],
    [ "EnableBook", "d3/d87/a00539.html#abfea6a61249a744e00948f596b6690d0", null ],
    [ "RuleCategories", "d3/d87/a00539.html#a3e54aab01a2219f2e9a4d33630369118", null ],
    [ "ServerInfo", "d3/d87/a00539.html#a39eb455c9fb0662a3738b10ae0b4db86", null ]
];