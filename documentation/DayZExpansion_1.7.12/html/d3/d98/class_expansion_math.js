var class_expansion_math =
[
    [ "_MovingAvg", "d3/d98/class_expansion_math.html#a337d6627212aa9e59cfdd28fe52c3d38", null ],
    [ "AbsAngle", "d3/d98/class_expansion_math.html#a2beed2cef1a07df5dac2d4db96efbc90", null ],
    [ "AngleDiff2", "d3/d98/class_expansion_math.html#a8e826f5def0384fd533036faf39148a0", null ],
    [ "Clamp", "d3/d98/class_expansion_math.html#a46671c1c5b15a5748ea5446fd13a86cf", null ],
    [ "Cmp", "d3/d98/class_expansion_math.html#a7b9934afa6666e1ca8bfda9ca34b5680", null ],
    [ "ExRotateAroundPoint", "d3/d98/class_expansion_math.html#a9baeeb89726ccf435c169c649466b2d8", null ],
    [ "GetRandomPointAtDegrees", "d3/d98/class_expansion_math.html#ab1945443c3ef04261d7f7c84ce7eff61", null ],
    [ "GetRandomPointAtRadians", "d3/d98/class_expansion_math.html#a76acdd0931820e1359e51d0ea502d9a3", null ],
    [ "GetRandomPointInCircle", "d3/d98/class_expansion_math.html#a89e847a3c5391476b2fab0cd91dedd79", null ],
    [ "GetRandomPointInRing", "d3/d98/class_expansion_math.html#a0e283713bdffca3eb6a2f5f74e4ea81f", null ],
    [ "LinearConversion", "d3/d98/class_expansion_math.html#adf82ec2acc0b91574840e1ba20b228d6", null ],
    [ "Log2", "d3/d98/class_expansion_math.html#a1976bbda83920514af13915242eb864f", null ],
    [ "MovingAvg", "d3/d98/class_expansion_math.html#a864531a95b257ed617c5391b441ee56b", null ],
    [ "PascalTriangle", "d3/d98/class_expansion_math.html#a43c2461c6b6defe2053a0c34b0afc68e", null ],
    [ "PathInterpolated", "d3/d98/class_expansion_math.html#a3b6964193e3a30480591237eb974c8f2", null ],
    [ "PowerConversion", "d3/d98/class_expansion_math.html#a507dd39bab045a3b8f34e45a0b9f9470", null ],
    [ "RandomFloatInclusive", "d3/d98/class_expansion_math.html#a6a41e81c2ab0974ad267bb4c22ca6cad", null ],
    [ "RelAngle", "d3/d98/class_expansion_math.html#aab3879bfbf90851cd8b08f1e5e0a212d", null ],
    [ "SCurve", "d3/d98/class_expansion_math.html#af7d1a5120786f88b26ffc279433fd6d0", null ],
    [ "SmoothStep", "d3/d98/class_expansion_math.html#a6040344ead1e539ad9a7d87452ae1521", null ]
];