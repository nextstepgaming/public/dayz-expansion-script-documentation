var a05889 =
[
    [ "MaxValue", "d3/d83/a05889.html#a23f773979c36af4e715a1826f535fdd6", null ],
    [ "MoneyDepositValue", "d3/d83/a05889.html#aabc94f6d85c6bdfc222cbeeaeeefb42c", null ],
    [ "PartyID", "d3/d83/a05889.html#a27c78b1dd3191aa78ea01a3e72c3acf7", null ],
    [ "PartyMaxValue", "d3/d83/a05889.html#a05ea17ed752192f2e5e8f637f6d06af1", null ],
    [ "PartyMoneyDepositValue", "d3/d83/a05889.html#a25da8ab97c63f304987af3219ea8df50", null ],
    [ "PartyName", "d3/d83/a05889.html#adb9404e52979df18216e57cae4ba7ae4", null ],
    [ "PartyOwner", "d3/d83/a05889.html#acb5de7c1c9651451df82a59a01d2f7bf", null ],
    [ "PlayerEntries", "d3/d83/a05889.html#a7aa59444e359ebf59d037aa234a075ac", null ],
    [ "PlayerMoneyValue", "d3/d83/a05889.html#afdffd0cdb423fa0dc470885aceb65881", null ]
];