var a06265 =
[
    [ "ExpansionQuestNPCBase", "d3/dfe/a06265.html#a10a4eab433ac7b50b2b99bc83a312086", null ],
    [ "DeferredInit", "d3/dfe/a06265.html#aac309467b528b42ee87b8082ba25be26", null ],
    [ "GetQuestNPCData", "d3/dfe/a06265.html#a641fc8660ab606d9a1fa21d627b4edb9", null ],
    [ "GetQuestNPCID", "d3/dfe/a06265.html#af79265ed197fef36102f8b3ad76d8f40", null ],
    [ "SetQuestNPCData", "d3/dfe/a06265.html#a934c7d639150d2caa71518b581ca471a", null ],
    [ "SetQuestNPCID", "d3/dfe/a06265.html#a7f332fb3e5996c142f916fe313219a86", null ],
    [ "m_QuestNPCData", "d3/dfe/a06265.html#aa7d4c2f3fe0d014e1498cf02d233ba41", null ],
    [ "m_QuestNPCID", "d3/dfe/a06265.html#ac874cf9347b89100fff61a179fa86dd7", null ]
];