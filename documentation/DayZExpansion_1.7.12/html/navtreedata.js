/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "DayZExpansion", "index.html", [
    [ "DayZExpansion Script Documentation", "index.html", null ],
    [ "Deprecated List", "d1/d3a/a03353.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d0/d2a/a04925.html#a1145e79d70da368117affcedad3526b8",
"d0/d2a/a04925.html#a9578015422d87b2cee61d749fb838853",
"d0/d2b/a01997_source.html",
"d0/d6f/a05137.html#ae2e3e6f517e15473f7530605848eb385",
"d0/d7e/a00770.html#abb0aa28c7279cdbc6a4e1fd88d3f2f36a1e30656933b3b9686b3c9009e0d97e68",
"d0/da3/a04961.html#ae08c73bffe5f3dc105420b3ad9fa1c6b",
"d0/dea/a02585.html#a0064efdbf0b93dabf345d562ccda88dc",
"d1/d18/a04993.html#a0c3f91604a1985ada929c8a77a1b678e",
"d1/d57/a02543_source.html",
"d1/dd2/a05073.html#a9cc0922620f84eed3b0979b9e8198cdc",
"d1/df9/a03665.html#a3571aa57ca5450090cafbe21c5b9d153",
"d1/df9/a03665.html#a90da502e25f08bac7e3459e3da207374",
"d1/df9/a03665.html#ae2413665b5d86944691b3e4e4d345899",
"d2/d22/a04473.html#ae160390318021c65688ae7fb8037e4f7",
"d2/d53/a06573.html#a6a3cd4ea70e6cbbdcb4458f8c339fc31",
"d2/da1/a06997.html#a58f2fe42ff1e5c353b55c9347fedcadf",
"d3/d0d/a04261.html#a59111e0fc4a7f5ed965ad0e3aa6b71b7",
"d3/d4c/a01724.html",
"d3/d87/a00539.html#a39eb455c9fb0662a3738b10ae0b4db86",
"d3/dc0/a03733.html#a425116fbe029599be43938cea4671403",
"d4/d21/a05245.html#a8a38aea68286d865aa722384e8733064",
"d4/d49/a07257.html#ad7a3d8264b94a9a116ae7c72cee2a690",
"d4/d93/a03781.html#a6935b2e93a40ab6d9ab1688450568487",
"d4/de0/a04189.html#a5f7ab9d61cd44ff5be5eea063af35489",
"d5/d37/a04117.html#ae030b5f82c035b762099446f4157f8e0",
"d5/d5f/a00461_source.html",
"d5/da2/a06441.html#a89ff724082927f7078fc38ca12a8b297",
"d5/df8/a04353.html#af60828b24959acd49f10e7f0e2d334ba",
"d6/d55/a05925.html#a27b87fcd30897fe2715dc3e1b4b1fbb6",
"d6/d5f/a05173.html#a5a96b4cc91d6bd07c01b0f8bddf256f4",
"d6/da1/a06025.html#a8a6c663583b39498ae9e6f7492489577",
"d7/d0d/a03885.html#a7b8a5ed0bbf34eb6e6c9ff847d400cab",
"d7/d40/a03681.html#a1e70671682caa70a3706915557ca90e8",
"d7/d40/a03681.html#ad44d504a5af020ade7ac424dbf32204d",
"d7/d72/a06017.html#a1da90046d879647dffbaae02749f2059",
"d7/dbc/a07477.html#ace49e23491a4d46eaaac37e676ec526a",
"d7/dca/a04205.html#aa2f57b38fea2f28da6c395a05f914f64",
"d7/dfb/a07041.html#a20f64ac0ede2d76c121a130b6cdb6958",
"d8/d25/a07353.html#aa4858d60913e322fb05b6b62f145fdc0",
"d8/d4e/a01355.html#adfe2624b86a0394ce5a386bd04fdd325",
"d8/db6/a04429.html#a32ed2727ab3ad60598ee5ac44ee961ed",
"d8/dec/a06749.html#a6199dacc11678e9d10e4ed6360f56440",
"d9/d32/a04557.html#ade998b01366b7a4f292bc934f9be0dd4",
"d9/d63/a01877.html#afe93d92fcf44106b3bf27a76992ba4f3",
"d9/d73/a06713.html#a4f863d6d86d32ecae15e7ae2f12253fc",
"d9/da3/a03673.html#aa70f25021edb3e8f1c3e32f0d993a2bb",
"da/d0e/a05201.html#a1dcfb7ff99e04dc0ee252273483b4ccc",
"da/d40/a04137.html#aee6e85e04e1b8c83bd414f6baa711aa5",
"da/d7c/a05021.html#a90e2e98a05ba3fc3d77c4f3f13ab632f",
"da/d93/a03765.html#afacbed1dd7f270b47c097c54032169e7",
"da/dca/a05933.html#a32138ab27a7a4310eeeeed1d789d49e3",
"da/ded/a05713.html#a9294c79763f3990c77dbb650e4b1dfaa",
"da/df4/a07625.html",
"db/d1a/a03849.html#ad67583f83302e97e22cb667aafa929cf",
"db/d77/a00683_source.html",
"db/dd3/a07453.html#aaea2aa052cb701e09135218f262bfdfd",
"dc/d39/a04529.html#a17a8685915035303c1d14ba4ba045aaf",
"dc/d8e/a01331.html#a01eb04373c7165413e8b58362323743a",
"dc/de3/a06205.html#a101799bb989e31da301b4de05227b54c",
"dd/d15/a05117.html#ada831a93cb236523cfd130f2eb516f0b",
"dd/d44/a07317.html#a3ebdb7e61a56362b78b2d2f3cf463e08",
"dd/d6e/a07445.html#a959e6a0489ab7ead7a3072ab101e7947",
"dd/d8e/a03377.html#a9eca8055718378307d97df28f6b0734f",
"dd/db3/a05121.html#a7cbecb54d5bc49d62707cf7bdc2f8d65",
"dd/dfd/a03017.html",
"de/d38/a06165.html#ad31856061bcf3810643ef10be786c605",
"de/d5d/a08769.html#a7880f6730032434b0128fddab2510625",
"de/d79/a03761.html#a23e7c6310cd27f1a57d957c4f309a2a7",
"de/db8/a07289.html#a09f12b1c31d2e5a1befaa7dc00146e5d",
"de/dde/a00587.html",
"df/d40/a07573.html#afbdc51682fefc47679429d9f48737d48",
"df/d81/a02966_source.html",
"df/da9/a06417.html#ad2c60ad8750f7f9ed19e83a93d6537b3",
"dir_0d79a6a0549ca5f187c50ba519253c0f.html",
"dir_7b5baaae8f1522aea91ea78a09fe78cc.html",
"dir_ebb4de486f7ba170894a1ef0e1e4abd4.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';