var class_zombie_base =
[
    [ "ZombieBase", "d2/ded/class_zombie_base.html#a73dc4204de3c1020cbf9d62cd7b6d65d", null ],
    [ "~ZombieBase", "d2/ded/class_zombie_base.html#a007db5bcb4841d8dded45c1ebe4dbf0a", null ],
    [ "GetAll", "d2/ded/class_zombie_base.html#af3351659cc77b1766e3b707587281fe7", null ],
    [ "IsInSafeZone", "d2/ded/class_zombie_base.html#a35f82605eac8c7d43011d73673e3e2c6", null ],
    [ "NameOverride", "d2/ded/class_zombie_base.html#a73686c24b70de059ee1320550c2667df", null ],
    [ "OnEnterZone", "d2/ded/class_zombie_base.html#a93d72faf67300ede3950ea054d0eefd1", null ],
    [ "OnExitZone", "d2/ded/class_zombie_base.html#ae62cd5fc073cdf3789b609d54f3c0c55", null ],
    [ "OnRPC", "d2/ded/class_zombie_base.html#a14c50865a8c7baa5329f1f8660b283af", null ],
    [ "m_Expansion_AllInfected", "d2/ded/class_zombie_base.html#ab2f1ed0cb86b2809c094adfcfbbf036d", null ],
    [ "m_Expansion_IsInSafeZone", "d2/ded/class_zombie_base.html#a2619a46fea6f8262013f3fc8c7e33d4e", null ],
    [ "m_Expansion_NetsyncData", "d2/ded/class_zombie_base.html#a5bc34df10fd548edda64065ddc2151bf", null ],
    [ "m_Expansion_SafeZoneInstance", "d2/ded/class_zombie_base.html#a0849752c39ca72f2d72e3d30e8332757", null ]
];