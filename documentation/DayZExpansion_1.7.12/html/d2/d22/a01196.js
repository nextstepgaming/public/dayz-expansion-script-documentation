var a01196 =
[
    [ "ExpansionOwnedContainer", "db/d75/a04761.html", "db/d75/a04761" ],
    [ "CanCombineAttachment", "d2/d22/a01196.html#a1081ef7ddc1312d603129a94b5daa555", null ],
    [ "CanReceiveAttachment", "d2/d22/a01196.html#ae7bd4d02b82fa1017e3a4cf7e3caba18", null ],
    [ "CanReceiveItemIntoCargo", "d2/d22/a01196.html#a3d6c23666c8f172fd72a16c6dc67d4ab", null ],
    [ "CanSwapItemInCargo", "d2/d22/a01196.html#a72686bc2d0bcd282fb767a41a71d0c4f", null ],
    [ "ExpansionCheckStorage", "d2/d22/a01196.html#ab074b805d53ed5f8f22a91b973a317b5", null ],
    [ "ExpansionDeleteStorage", "d2/d22/a01196.html#abf8416cc1639f544c2b55a1876175db0", null ],
    [ "ExpansionSetCanReceiveItems", "d2/d22/a01196.html#a053a726f4489524843aa5a4a5c2483bd", null ],
    [ "ExpansionStorageNotification", "d2/d22/a01196.html#a4a425fc8bf6638052b57f308e5b3b74a", null ],
    [ "ExpansionTemporaryOwnedContainer", "d2/d22/a01196.html#a3c885baa8dc388a8f446c07c21b977ef", null ],
    [ "m_ExpansionCanReceiveItems", "d2/d22/a01196.html#ad02486bb1328039f6b5217d31e7e4a9c", null ]
];