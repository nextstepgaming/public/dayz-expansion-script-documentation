var a03389 =
[
    [ "EnableTerritories", "d2/dce/a03389.html#aedf640cba106c100f32af8a7465cc366", null ],
    [ "MaxMembersInTerritory", "d2/dce/a03389.html#a8654d0d5a0b9fd4345e32ec3b98cf191", null ],
    [ "MaxTerritoryPerPlayer", "d2/dce/a03389.html#a24112f5cd29ccaf77f2fc83fcb489fdd", null ],
    [ "TerritorySize", "d2/dce/a03389.html#a8b195554e785fd14f42bce0694a2b5a6", null ],
    [ "UseWholeMapForInviteList", "d2/dce/a03389.html#a3df8acabbb2fb60f4310af013ec90805", null ]
];