var notificationruntimeenum_8c =
[
    [ "NotificationRuntimeEnum", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aa", [
      [ "UNKNOWN", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa6ce26a62afab55d7606ad4e92428b30c", null ],
      [ "MUTED", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaad7750b2464175cffe19792309edf40f8", null ],
      [ "PLAYER_JOINLEAVE", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa034910561929fe9679bf4a3f98485530", null ],
      [ "KILLFEED", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaaddf118ed7fbb19b9aaa6e3189e16cf28", null ],
      [ "SAFEZONE", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa6ba1ce65a60f7a844f8ec20d2760c743", null ],
      [ "TERRITORY", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaafb4e57fdacd7c27425ab06c3b0475619", null ],
      [ "PARTY", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaabb6f1d787307698ca002d5718bed6605", null ],
      [ "MISSION_AIRDROP", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa9d774cd9ec4c96d5020a0f3636836eab", null ],
      [ "MISSION_HORDE", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa731ad9ce7706239e0b3eacbc53cd8f6a", null ],
      [ "MISSION_AI", "d2/daf/notificationruntimeenum_8c.html#ac3c80b62317785af723bbe93b95428aaa0a2e174a5bd76b8612103f57a862d7d5", null ]
    ] ]
];