var a04081 =
[
    [ "ExpansionBookMenu", "d2/d5a/a04081.html#aff381c4f6b5ee5448ae38d37bef50a75", null ],
    [ "~ExpansionBookMenu", "d2/d5a/a04081.html#a713596b8b5b7160d0f68133102fb9e6f", null ],
    [ "AddBookmark", "d2/d5a/a04081.html#ac67a39b9d6b79708da4cbe86eb59c3f1", null ],
    [ "AddTab", "d2/d5a/a04081.html#a16d5a81b9d389fbba7ce3c99c76dae54", null ],
    [ "AddTabs", "d2/d5a/a04081.html#a247563339d7fa619cafe7f23c8b14405", null ],
    [ "Callback", "d2/d5a/a04081.html#a1b112bfa04cf32b87c503cdfc438fdbb", null ],
    [ "CanClose", "d2/d5a/a04081.html#ac3d0269f3676f3478fd4a05c9b572d89", null ],
    [ "GetBookmarksSideSpacerLeft", "d2/d5a/a04081.html#a7c47047377e1aa73c95eca55c0b2fd94", null ],
    [ "GetBookMenuManager", "d2/d5a/a04081.html#a493ab60bd34ea1667f6fff9b0356a48d", null ],
    [ "GetControllerType", "d2/d5a/a04081.html#a9d7cd6420d0e427b31912fdd22738b7c", null ],
    [ "GetLastOpenedTab", "d2/d5a/a04081.html#af0eb128e4298365f147d25d74e2f9df0", null ],
    [ "GetLayoutFile", "d2/d5a/a04081.html#a3ea995eba2627cf2066ce4e57ad2e2d5", null ],
    [ "LockControls", "d2/d5a/a04081.html#a0c7b5519989274ed986c5854cfcec484", null ],
    [ "OnBackButtonClick", "d2/d5a/a04081.html#af5e0f42e2d8c24edca6eb3efa785b8ed", null ],
    [ "OnHide", "d2/d5a/a04081.html#ac26e3e1f64bd302c2fa4160e08feb1be", null ],
    [ "OnMouseEnter", "d2/d5a/a04081.html#a04cc315bd746a2ba3138ba3ff96cbedd", null ],
    [ "OnMouseLeave", "d2/d5a/a04081.html#ad42f743ab6ca09296d72bf456bcd525a", null ],
    [ "OnShow", "d2/d5a/a04081.html#a5d6c07ab5b1e77520ba4025229becb34", null ],
    [ "PlayCloseBookSound", "d2/d5a/a04081.html#aa74117190a94284435c19474e8677609", null ],
    [ "PlayDrawSound", "d2/d5a/a04081.html#aa3c1b1348de653fc8a4212a1ba830759", null ],
    [ "PlayOpenBookSound", "d2/d5a/a04081.html#a538c688e2bf79abce2f289659f1e4496", null ],
    [ "PlaySwitchPageSound", "d2/d5a/a04081.html#a624fc62f914a367b9e84ea379df4e0e7", null ],
    [ "RegisterTab", "d2/d5a/a04081.html#af3d4da02968b2c7d4a12c60b2f35b5fd", null ],
    [ "RemoveElements", "d2/d5a/a04081.html#af9857826237c74c478aedc2517f28b1e", null ],
    [ "SetLastOpenedTab", "d2/d5a/a04081.html#a4066211acf47e384cefbff89a5929f0e", null ],
    [ "ShowBookMainContent", "d2/d5a/a04081.html#a4f2ef3011b885741e8ddaa481af86aa6", null ],
    [ "SwitchMovementLockState", "d2/d5a/a04081.html#a195c727ce81a357eafb09d275e2d9c44", null ],
    [ "UnregisterTab", "d2/d5a/a04081.html#ad5a55ce496f21453e5c59895759f7f60", null ],
    [ "UpdateBookTabs", "d2/d5a/a04081.html#acc161afd2f87df66243bfb8087120055", null ],
    [ "back_button", "d2/d5a/a04081.html#a02a2485bff7491332d79efb91ac7eb5f", null ],
    [ "back_button_label", "d2/d5a/a04081.html#a5f781e06a3f766c00a3bcda5e8f878c4", null ],
    [ "book_content_panel", "d2/d5a/a04081.html#add9ae5f8d1139cdda0c18208d76f6eda", null ],
    [ "m_BookManager", "d2/d5a/a04081.html#a7eefcb04ec56d7489c70c823fb38e14c", null ],
    [ "m_BookMenuController", "d2/d5a/a04081.html#adff48d405ba7f5d945ab16059bd8222c", null ],
    [ "m_IsMainContentVisible", "d2/d5a/a04081.html#ad51b8b7cfa6189a16a4feae07b05eceb", null ],
    [ "m_LastOpenedTab", "d2/d5a/a04081.html#a20f01c182631ccf49186a85415692a26", null ],
    [ "m_MaxNumberOfBookmarks", "d2/d5a/a04081.html#acdf88ce997d8f0d6c9b5aa30b590f7a3", null ],
    [ "m_MaxNumberOfElements", "d2/d5a/a04081.html#a4eef442ba3f6f94eac8881f69ecd0d5e", null ]
];