var a06477 =
[
    [ "AddAttachment", "df/dcb/a06477.html#ae00456859e628fc1f97f8e42f1f10cf4", null ],
    [ "GetAmount", "df/dcb/a06477.html#a17e1f9be6b6ff3ca46a984827391c78c", null ],
    [ "GetAttachments", "df/dcb/a06477.html#a2abe5581b5456b47524932689db4e290", null ],
    [ "GetClassName", "df/dcb/a06477.html#aadddc6050020a3588e60f979e41794dc", null ],
    [ "IsVehicle", "df/dcb/a06477.html#af0e6e383ba63f90aab9d53118a110ee1", null ],
    [ "OnRecieve", "df/dcb/a06477.html#a9379e99d1af77b16ba1c87a6ac8d3eb4", null ],
    [ "OnSend", "df/dcb/a06477.html#acdfc2acae2e35ea1b735c4c82387d6fa", null ],
    [ "QuestDebug", "df/dcb/a06477.html#a1702b028300a34776fafd37bf221719e", null ],
    [ "SetAmount", "df/dcb/a06477.html#a81229698dc613770df4645581144c4f8", null ],
    [ "SetClassName", "df/dcb/a06477.html#acdae560b2dabbd4dc37e3985d2e69b36", null ],
    [ "Attachments", "df/dcb/a06477.html#aefaf471efacce12828f618c1fbb88e7f", null ]
];