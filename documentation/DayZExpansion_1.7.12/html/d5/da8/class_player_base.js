var class_player_base =
[
    [ "PlayerBase", "d5/da8/class_player_base.html#a4c273da961f398a1189c1f219bc31717", null ],
    [ "FindNearestFlag", "d5/da8/class_player_base.html#afc6f2bbece98a91961aea9ab50ddf469", null ],
    [ "GetBuildNoBuildZone", "d5/da8/class_player_base.html#a1a79aecd9a9dbe882fc10c9451f68250", null ],
    [ "GetTerritoryIDInside", "d5/da8/class_player_base.html#a44b3a7db7b67930143dd903f8070daa2", null ],
    [ "IsInPerimeter", "d5/da8/class_player_base.html#a33e7650de06caefd1e064d2a1eec56ee", null ],
    [ "IsInsideOwnPerimeter", "d5/da8/class_player_base.html#a510b38c82603d64ef1276eecef4911d6", null ],
    [ "IsInsideOwnTerritory", "d5/da8/class_player_base.html#aa8b20ccd5b11c8a3a6470afa6410772a", null ],
    [ "IsInsideOwnTerritoryOrPerimeter", "d5/da8/class_player_base.html#aba3299993ba39224932908bc4b52dfef", null ],
    [ "IsInTerritory", "d5/da8/class_player_base.html#aac7d8816c898efbe2419763d6dba3ff2", null ],
    [ "IsInTerritoryOrPerimeter", "d5/da8/class_player_base.html#a97309549e2818d4a2b76f91a4690d655", null ],
    [ "IsTargetInActiveRefresherRange", "d5/da8/class_player_base.html#a041ae1ae2d1a4f4c0d544205982dd82d", null ],
    [ "SetActions", "d5/da8/class_player_base.html#a092cf1bb1246b2a342676d2921b2c80b", null ],
    [ "SetTerritoryIDInside", "d5/da8/class_player_base.html#a17a36f6f5f783a39548190ee72420bf3", null ],
    [ "TerritoryModuleExists", "d5/da8/class_player_base.html#a5e502ba2a70b4792e224676312a43c51", null ],
    [ "m_TerritoryIdInside", "d5/da8/class_player_base.html#af83e952c23544054bbe0a492174204af", null ],
    [ "m_TerritoryModule", "d5/da8/class_player_base.html#a2bad2b6e7ba450ada45a75751782eadb", null ]
];