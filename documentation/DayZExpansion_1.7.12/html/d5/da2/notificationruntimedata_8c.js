var notificationruntimedata_8c =
[
    [ "ExpansionNotificationType", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02", [
      [ "TOAST", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02af85b8f3416b2236ea00dcb7c2c1f5df6", null ],
      [ "BAGUETTE", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02a9aee4467f46d6f57db879225e4672536", null ],
      [ "ACTIVITY", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02a995d34bcabbc4f6dd5347f17464c4a2f", null ],
      [ "KILLFEED", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02addf118ed7fbb19b9aaa6e3189e16cf28", null ],
      [ "MARKET", "d5/da2/notificationruntimedata_8c.html#a87895fe18688d17fbc88515ccb6f6c02a9e0221c89ef1ee595163e8cb2f316420", null ]
    ] ],
    [ "GetIcon", "d5/da2/notificationruntimedata_8c.html#aa52f1c04b89be3ac40919c7b296ef0c2", null ],
    [ "GetObject", "d5/da2/notificationruntimedata_8c.html#aaa4160de65ac77debe2902527ebfc4f2", null ],
    [ "GetType", "d5/da2/notificationruntimedata_8c.html#abf2f1d0d1afc0eaf2994523ab122712c", null ],
    [ "SetObject", "d5/da2/notificationruntimedata_8c.html#a50d769a8c3c64df8eaafb29fd2aa7723", null ],
    [ "SetType", "d5/da2/notificationruntimedata_8c.html#a221704fcc463a5eeaeeee6f38302e71a", null ],
    [ "m_LeaveJoinNotif", "d5/da2/notificationruntimedata_8c.html#a58019ce4a31264c5ca54e96cedf518b6", null ],
    [ "m_Object", "d5/da2/notificationruntimedata_8c.html#a3e8611572e6ae0823d4aeb0d0a2e0d69", null ],
    [ "m_Type", "d5/da2/notificationruntimedata_8c.html#a462f1b2b6a46de514f2ec378e5d2a1ff", null ]
];