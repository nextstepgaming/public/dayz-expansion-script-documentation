var a06441 =
[
    [ "IsActive", "d5/da2/a06441.html#a85d2f8b2a907ab1dbd6ef4b5b45d6073", null ],
    [ "IsCompleted", "d5/da2/a06441.html#aeaa60124966a0e5fe03721d326dfe2b9", null ],
    [ "ObjectiveAmount", "d5/da2/a06441.html#a89ff724082927f7078fc38ca12a8b297", null ],
    [ "ObjectiveCount", "d5/da2/a06441.html#a6195adce8082d886f49514c1a74a724b", null ],
    [ "ObjectiveIndex", "d5/da2/a06441.html#a3149f5de2a3f38307cf69ccf92e20d43", null ],
    [ "ObjectivePosition", "d5/da2/a06441.html#a83f880ee6c6c4d5327b3287e3e16dff7", null ],
    [ "ObjectiveType", "d5/da2/a06441.html#a1e6a5e61a161e5388b2a8623920d5744", null ],
    [ "QuestID", "d5/da2/a06441.html#add3f15fd941bdeeff6563c1cdad67d5e", null ]
];