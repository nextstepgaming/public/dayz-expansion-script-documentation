var a00767 =
[
    [ "ExpansionClientUIMemberMarkerType", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6", [
      [ "PERSON", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6ac9878c552c3ddcde707fff8aefa250ae", null ],
      [ "MARKER", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a6be825f32de60fabdef2bd3da1310f63", null ],
      [ "ROUNDMARKER", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6ad60f7dabbcfc0bd4bc7edd0c1c452087", null ],
      [ "ROUNDMARKER2", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a5a1be6d4718dd5ffa76ca7adbf9f6b31", null ],
      [ "INFECTED", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a439547dca6e52df4ceedaeae94b3944e", null ],
      [ "INFECTED2", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a93850a450bb4f378be141f59cbbcc9cb", null ],
      [ "HEART", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a2dc2f5c8b096c26695b79f60d735dd90", null ],
      [ "SKULL", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6ab84c75ef6892c5e12b8c71c798638a6a", null ],
      [ "SKULL2", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6ad98be30a5ce71da475943a53310fbd15", null ],
      [ "SHIELD", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6affdd98146fb0c71b3381f94b036a100f", null ],
      [ "INFO", "d5/dae/a00767.html#ad42bb4ab46282851bb66010cd71a19f6a748005382152808a72b1a9177d9dc806", null ]
    ] ]
];