var a01901 =
[
    [ "ExpansionMarketMenuItemTooltipEntryBase", "d6/dc2/a06021.html", "d6/dc2/a06021" ],
    [ "ExpansionMarketMenuItemTooltipEntryItemInfoController", "d6/da1/a06025.html", "d6/da1/a06025" ],
    [ "ExpansionMarketMenuItemTooltipEntryItemInfo", "d5/d99/a01901.html#aee380edd581eeb295c9b94602a32578e", null ],
    [ "GetControllerType", "d5/d99/a01901.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "d5/d99/a01901.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "SetColor", "d5/d99/a01901.html#a9b8d6ae5aa9ccd4fe010396c6564f2e3", null ],
    [ "SetIcon", "d5/d99/a01901.html#a015fb32c827408f1515af730bee07bd1", null ],
    [ "SetText", "d5/d99/a01901.html#a47a3d1a4bd1182cef70d052c48bc5eb7", null ],
    [ "m_TooltipEntryController", "d5/d99/a01901.html#a1b6db07c5871b60137346f2f17b2668a", null ],
    [ "tooltip_entry_text", "d5/d99/a01901.html#a0f6b18cd99aeb7a24903bade02ef10d3", null ],
    [ "tooltip_entry_title", "d5/d99/a01901.html#a44988ad2d6f8b85a78bdb72e26be1084", null ],
    [ "tooltip_icon", "d5/d99/a01901.html#a32dc742f1e6c71123f6d527d571903f1", null ]
];