var a06145 =
[
    [ "Copy", "d5/d6e/a06145.html#a5234eaa3e1092cd82f661b924cdd079d", null ],
    [ "CopyInternal", "d5/d6e/a06145.html#a9ec34d2e5e0ece5668178dd700e52f4e", null ],
    [ "CopyInternal", "d5/d6e/a06145.html#a8d87702f39ffd82bfa7a95f2369c56e5", null ],
    [ "Defaults", "d5/d6e/a06145.html#a1c0f8a165a263f5a6a14e72d2ea128a4", null ],
    [ "IsLoaded", "d5/d6e/a06145.html#a5e0d4b04a99b2034856455e126eebe8d", null ],
    [ "OnLoad", "d5/d6e/a06145.html#acf41887ce12da13f528b893301451c64", null ],
    [ "OnRecieve", "d5/d6e/a06145.html#a52bc8bc6eb15ab0b29e67f26533feb76", null ],
    [ "OnSave", "d5/d6e/a06145.html#a9ac4d62ba011104e38b82a2d76852ac5", null ],
    [ "OnSend", "d5/d6e/a06145.html#a85d7818795f6b9f369ad3e0591361014", null ],
    [ "Send", "d5/d6e/a06145.html#ad8d574319217d187396d8a6bb322340b", null ],
    [ "SettingName", "d5/d6e/a06145.html#a59839bf5c058c9cda0fe3e043ddf09b3", null ],
    [ "Unload", "d5/d6e/a06145.html#a4f89b25da2dee5e9f4ff8276bddd4bba", null ],
    [ "Update", "d5/d6e/a06145.html#ac16a738320579806d13b8f81560d31d8", null ],
    [ "m_IsLoaded", "d5/d6e/a06145.html#a8ee6983ef9fb622ddccab1b075e5572b", null ],
    [ "OnlyInSafeZones", "d5/d6e/a06145.html#a50b1b55b6a4e3f11bd601f11a257ed40", null ],
    [ "OnlyInTerritories", "d5/d6e/a06145.html#a8e9bb45e6aec685586763db24ab27a6f", null ],
    [ "PlayerNameColor", "d5/d6e/a06145.html#ae10540e3bba27defea1dfc758815ad1b", null ],
    [ "PlayerTagsColor", "d5/d6e/a06145.html#a49d1bfe2e98bfabd868c1accdf4300f0", null ],
    [ "VERSION", "d5/d6e/a06145.html#a3685ee904ff17c1658a1ed3af85fc3c5", null ]
];