var a03365 =
[
    [ "AllowBuildingWithoutATerritory", "d7/d13/a03365.html#a908babf7b5cff084d67e1da2d4dac01f", null ],
    [ "AutomaticFlagOnCreation", "d7/d13/a03365.html#ac20a85c273f7d273c51ae61a1d3eac2d", null ],
    [ "CanBuildAnywhere", "d7/d13/a03365.html#ab0ab9be24d7d8c2efbfb977fb71960e1", null ],
    [ "CanCraftExpansionBasebuilding", "d7/d13/a03365.html#a3e1e981961538650dbd7a13a364c72d1", null ],
    [ "CanCraftTerritoryFlagKit", "d7/d13/a03365.html#a815814fe1829794af24e1b8474a065cf", null ],
    [ "CanCraftVanillaBasebuilding", "d7/d13/a03365.html#ac607105bed3ebc0af21e7e1655dc7914", null ],
    [ "CodelockActionsAnywhere", "d7/d13/a03365.html#a3bb811a992072911ceee172b644f1e03", null ],
    [ "CodeLockLength", "d7/d13/a03365.html#ab1ce9bdcb8f837a36eaafe1cbee95af8", null ],
    [ "DamageWhenEnterWrongCodeLock", "d7/d13/a03365.html#aa9894627f25687b3f74b4cc19124e4d2", null ],
    [ "DeployableInsideAEnemyTerritory", "d7/d13/a03365.html#a6631f2d2b2b45b79c34e58047fb469c0", null ],
    [ "DeployableOutsideATerritory", "d7/d13/a03365.html#a86b7f6aeda1d5a960603ad71507828f1", null ],
    [ "DestroyFlagOnDismantle", "d7/d13/a03365.html#a445e282c5aad77a5677ffc0a0c234bb0", null ],
    [ "DismantleAnywhere", "d7/d13/a03365.html#a84d4c3ed925ebf00fa6bcaebd4131440", null ],
    [ "DismantleInsideTerritory", "d7/d13/a03365.html#adaf7edf6305271b5f1caae85849ed44b", null ],
    [ "DismantleOutsideTerritory", "d7/d13/a03365.html#a088b0de6862ae7b3802b842a6b617249", null ],
    [ "DoDamageWhenEnterWrongCodeLock", "d7/d13/a03365.html#ab744afedf0748152c1ec1fcfd06d1138", null ],
    [ "GetTerritoryFlagKitAfterBuild", "d7/d13/a03365.html#a19218c21e969f9e0d6735d383ecde0ef", null ],
    [ "RememberCode", "d7/d13/a03365.html#abf599211723429e8cf1f06a347f69834", null ],
    [ "SimpleTerritory", "d7/d13/a03365.html#ac40790d51b13b295e9d957edb6020268", null ]
];