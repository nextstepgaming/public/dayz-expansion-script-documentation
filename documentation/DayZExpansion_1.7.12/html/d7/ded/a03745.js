var a03745 =
[
    [ "ExpansionHesco", "d7/ded/a03745.html#a38bcb79580a1f35988daea4e36951cf7", null ],
    [ "~ExpansionHesco", "d7/ded/a03745.html#a98ab5debf2507e4deea864c8a08754a2", null ],
    [ "CanBeDamaged", "d7/ded/a03745.html#a4d776f68936274436628ddbd571ab489", null ],
    [ "CanObstruct", "d7/ded/a03745.html#aa5c3fc881a8418ddd2eec86c27409231", null ],
    [ "CanPutInCargo", "d7/ded/a03745.html#ae0d792c599b8d603398464cf7a3af51c", null ],
    [ "CanPutIntoHands", "d7/ded/a03745.html#a658d168a7fbc5d3b57b7ed0c077ed53f", null ],
    [ "GetConstructionKitType", "d7/ded/a03745.html#ace838725316d4ea9f0971b920898c7c5", null ],
    [ "SetPartsAfterStoreLoad", "d7/ded/a03745.html#af9ef122b454819aa4bf0238de5355d23", null ]
];