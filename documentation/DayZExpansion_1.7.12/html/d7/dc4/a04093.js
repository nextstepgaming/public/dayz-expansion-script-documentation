var a04093 =
[
    [ "ExpansionBookMenuTabBase", "d7/dc4/a04093.html#a02a3892b4ea14a75f7155ee32b0ee940", null ],
    [ "AddChildTab", "d7/dc4/a04093.html#a801dbe4b039261e6797b6802937d10d4", null ],
    [ "CanClose", "d7/dc4/a04093.html#a3a06335824c212a948e61f39e63470a8", null ],
    [ "GetBookMenu", "d7/dc4/a04093.html#aafb4335f21f9546db5534a17bec4b5d1", null ],
    [ "GetChildTabs", "d7/dc4/a04093.html#a5be5abd8e89d1a440b04539f58b90282", null ],
    [ "GetParentTab", "d7/dc4/a04093.html#abd3112e6a8e598f2764204f44667f4c0", null ],
    [ "GetTabColor", "d7/dc4/a04093.html#a6b72818acb9c73e18bb1872031e197b5", null ],
    [ "GetTabIconName", "d7/dc4/a04093.html#a81cfab6b14cd88ad42bb4c451103c170", null ],
    [ "GetTabName", "d7/dc4/a04093.html#a908971c1cd83a1a488bc3cb37f08efa6", null ],
    [ "IgnoreBackButtonBase", "d7/dc4/a04093.html#a520dc62d8f05972571c7f7172693adb3", null ],
    [ "IsChildTab", "d7/dc4/a04093.html#a1c7257b9e0a4c7ae417ea9c8ea4f89ef", null ],
    [ "IsParentTab", "d7/dc4/a04093.html#a1870bcdbcdefa04aed4ca7350941f05d", null ],
    [ "OnBackButtonClick", "d7/dc4/a04093.html#a6c06d693ad4d424939c5f459fc647096", null ],
    [ "OnHide", "d7/dc4/a04093.html#a0ca00f2496da2fb40d59f25274278c5b", null ],
    [ "OnShow", "d7/dc4/a04093.html#a14a3da1dab03191398d0ad5830974f7c", null ],
    [ "SetParentTab", "d7/dc4/a04093.html#a4d9f693d0d2bbe42f0cd21bef24e01a6", null ],
    [ "SwitchMovementLockState", "d7/dc4/a04093.html#a3db21a8368465b60ace224b7da9b17c5", null ],
    [ "m_BookMenu", "d7/dc4/a04093.html#af262a87ec5c71aaf511222bcdfaebdaf", null ],
    [ "m_ParentTab", "d7/dc4/a04093.html#a871e93ea56c003e0ac98def94c2072d4", null ],
    [ "m_TabChildren", "d7/dc4/a04093.html#a9311c66d23db008012e6f1c681a7f803", null ]
];