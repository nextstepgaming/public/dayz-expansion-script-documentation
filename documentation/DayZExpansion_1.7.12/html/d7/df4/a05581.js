var a05581 =
[
    [ "AttachmentIDs", "d7/df4/a05581.html#ae9f94d3857a44f7df88570f29271a255", null ],
    [ "CategoryID", "d7/df4/a05581.html#adb4446c674d4447af39abd5001584382", null ],
    [ "ClassName", "d7/df4/a05581.html#a962a71814e106ec41a44277f1ea6101d", null ],
    [ "m_StockOnly", "d7/df4/a05581.html#af3ac791787cad7de9b6d47b6e0e79b34", null ],
    [ "MaxPriceThreshold", "d7/df4/a05581.html#abb7c07744d7deefa33633a7b2b135fa8", null ],
    [ "MaxStockThreshold", "d7/df4/a05581.html#a48626c73512a698933aa60ee52e4248c", null ],
    [ "MinPriceThreshold", "d7/df4/a05581.html#ae8f5c1c34fdd322d4a0d31761bae561e", null ],
    [ "MinStockThreshold", "d7/df4/a05581.html#a538e81ee56d3cc0e2e3645b6ecdf247f", null ],
    [ "Packed", "d7/df4/a05581.html#a81edd6ed7c01721f338147cb75f3b2a9", null ],
    [ "Variants", "d7/df4/a05581.html#a6ccf0552df570db23886483875557570", null ]
];