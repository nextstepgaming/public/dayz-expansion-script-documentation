var a01340 =
[
    [ "ExpansionMenuDialogContent_WrapSpacer", "dc/db7/a05105.html", "dc/db7/a05105" ],
    [ "ExpansionMenuDialogContent_WrapSpacerController", "db/d83/a05109.html", "db/d83/a05109" ],
    [ "ExpansionMenuDialogContent_WrapSpacer_EntryController", "d1/dbd/a05113.html", "d1/dbd/a05113" ],
    [ "ExpansionMenuDialogContent_WrapSpacer_Entry", "d7/d51/a01340.html#a3ad6bd5315d58088af903111a9fd7838", null ],
    [ "GetControllerType", "d7/d51/a01340.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "d7/d51/a01340.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "GetWrapSpacerElement", "d7/d51/a01340.html#a04c5ec6f6e8fcda6821423d1c9283d3e", null ],
    [ "SetTextColor", "d7/d51/a01340.html#a8a8a420577d32cf39a2ac11d1e97c537", null ],
    [ "SetView", "d7/d51/a01340.html#aff9a284bb904b06f8ab09a622b5d7d6f", null ],
    [ "entry", "d7/d51/a01340.html#a163104245a2e3c95642c1e1df12dcfd6", null ],
    [ "m_EntryController", "d7/d51/a01340.html#a4938268820b8b77fd570a6c3e90d68fe", null ],
    [ "m_Text", "d7/d51/a01340.html#a11fc80cdcb775f018854049236528029", null ],
    [ "m_WrapSpacerElement", "d7/d51/a01340.html#ad98ef6cfe350a6cfc1fc3881f86bc2ab", null ]
];