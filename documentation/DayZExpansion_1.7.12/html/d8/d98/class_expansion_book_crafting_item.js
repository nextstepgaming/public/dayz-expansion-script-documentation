var class_expansion_book_crafting_item =
[
    [ "ExpansionBookCraftingItem", "d8/d98/class_expansion_book_crafting_item.html#a36bf61939725c7e02b9fa38073e2d9c9", null ],
    [ "CanUseInRecipe", "d8/d98/class_expansion_book_crafting_item.html#aba56c3c8086a619229892bc59e3a0ded", null ],
    [ "CanUseInRecipe", "d8/d98/class_expansion_book_crafting_item.html#a50c0ad89bcea51f1bcadcd5cebc806d3", null ],
    [ "Format", "d8/d98/class_expansion_book_crafting_item.html#a7fffce37b682c3a0b92cefff720a3635", null ],
    [ "Amount", "d8/d98/class_expansion_book_crafting_item.html#a49d2f2548f5949de27af139d4860fc05", null ],
    [ "ClassNames", "d8/d98/class_expansion_book_crafting_item.html#ac13cf4ea0ed9041d9736b3834120e570", null ],
    [ "DisplayName", "d8/d98/class_expansion_book_crafting_item.html#a95a3c4b08cad6ab38e4e6b0472b07a27", null ],
    [ "m_CanUseInRecipe", "d8/d98/class_expansion_book_crafting_item.html#a955f06b13f486b908356b7399ea43937", null ],
    [ "m_PlayerItem", "d8/d98/class_expansion_book_crafting_item.html#a7612985a7aae106e650ecfecf25e2b78", null ]
];