var a02383 =
[
    [ "ExpansionSafeZoneSettings", "db/dd4/a02383.html#a2e55a7c7ee0fec0861ff16c58130115c", null ],
    [ "Copy", "db/dd4/a02383.html#afad12cdc500273e88ee5d91537ff64ab", null ],
    [ "CopyInternal", "db/dd4/a02383.html#a3572ade930682dd37f33761deca4eacd", null ],
    [ "CopyInternal", "db/dd4/a02383.html#ae745037bd100ca829a24c51e96539165", null ],
    [ "Defaults", "db/dd4/a02383.html#adc676763382d6a3ed6c9a5936d0ad73c", null ],
    [ "IsLoaded", "db/dd4/a02383.html#a1f9d09fc9bfb1da2ee9afcc2df1d4f10", null ],
    [ "OnLoad", "db/dd4/a02383.html#ad9bd87e0aa87be4d99317b2cc8ba501f", null ],
    [ "OnRecieve", "db/dd4/a02383.html#aa0061b8fb813afdec666c6c4045a6976", null ],
    [ "OnSave", "db/dd4/a02383.html#a3473f43283cd8998452f345bae259be3", null ],
    [ "OnSend", "db/dd4/a02383.html#a7cbe7ff77cd0cc29a345329c9a66665c", null ],
    [ "Send", "db/dd4/a02383.html#a745b4d2d6b285cc95851d48ed93b47bd", null ],
    [ "SettingName", "db/dd4/a02383.html#ac25179dc9e3e62e5472857756ba44874", null ],
    [ "Unload", "db/dd4/a02383.html#ae85c9284946aae701005880fd420172e", null ],
    [ "ActorsPerTick", "db/dd4/a02383.html#a8125e46a3745c9ae43ce96f8e1317d9f", null ],
    [ "DisableVehicleDamageInSafeZone", "db/dd4/a02383.html#a35c1913aa5df16ad57d6ec767bc1d9b3", null ],
    [ "EnableForceSZCleanup", "db/dd4/a02383.html#ab6619faaf2bbbe1c79747e7fc2cfcf43", null ],
    [ "ForceSZCleanup_ExcludedItems", "db/dd4/a02383.html#a4561db3bf8f5ed854d93d1e3143227aa", null ],
    [ "ItemLifetimeInSafeZone", "db/dd4/a02383.html#a007afc4d58d9e2e2bbbe56e5a4bb60f3", null ],
    [ "m_IsLoaded", "db/dd4/a02383.html#add43f0d1bf9cace866d1780d31697b53", null ],
    [ "VERSION", "db/dd4/a02383.html#aa398175b552c7a3da1b476fa064ce21c", null ]
];