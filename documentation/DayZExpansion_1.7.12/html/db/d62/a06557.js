var a06557 =
[
    [ "CheckEntity", "db/d62/a06557.html#ab6134cc80d97352239862b31f76e6491", null ],
    [ "GetActionState", "db/d62/a06557.html#aad7b139d1c5b46e03b98f0e987919084", null ],
    [ "GetObjectiveType", "db/d62/a06557.html#a083ac5b7c5443cbc5c67dde7c5ebb46d", null ],
    [ "OnActionUsed", "db/d62/a06557.html#a33a2507111676650d21747ba6dfa33ac", null ],
    [ "QuestPrint", "db/d62/a06557.html#a5fb531ece86ba8e33d08e63840a274fc", null ],
    [ "SetActionState", "db/d62/a06557.html#afb9a82c531e4b33104506ef4d7d83485", null ],
    [ "m_ActionState", "db/d62/a06557.html#a7f9dfda6aa058feb5bc636ef98f094d8", null ],
    [ "m_CallLater", "db/d62/a06557.html#a4a7dac6dc04a94e6040738997bb34c3a", null ],
    [ "m_LastEntity", "db/d62/a06557.html#aec9b4976a0fdb7ead70e7c9db0bff631", null ]
];