var a03461 =
[
    [ "ExpansionTerritoryMember", "db/d1b/a03461.html#adfed12b763f5f6ce98224be09c98b401", null ],
    [ "GetID", "db/d1b/a03461.html#a4a0d9f2aa7d54791414d927857c4afd4", null ],
    [ "GetName", "db/d1b/a03461.html#a6342ce891c8bf7d86c0b770eb080ecb4", null ],
    [ "GetRank", "db/d1b/a03461.html#ae303f000699d643270683d509b828389", null ],
    [ "GetRankName", "db/d1b/a03461.html#a1c9bdfe1e67f0bf3336b8c47c03baf10", null ],
    [ "SetRank", "db/d1b/a03461.html#a59cf7d6d7f2a3631dad86e9448af5b90", null ],
    [ "m_ID", "db/d1b/a03461.html#acfb027af3b28689bd73d6eaf671a428f", null ],
    [ "m_Name", "db/d1b/a03461.html#acca8064c0876917a0229c903625240cb", null ],
    [ "m_Rank", "db/d1b/a03461.html#a615db3942b4959755b2eb373185fb489", null ]
];