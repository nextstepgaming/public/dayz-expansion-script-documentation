var class_expansion_book_menu_manager =
[
    [ "~ExpansionBookMenuManager", "db/dcb/class_expansion_book_menu_manager.html#a2987f5161600685afb50a1ba53b3168e", null ],
    [ "DestroyBookMenuTabs", "db/dcb/class_expansion_book_menu_manager.html#a4dc289666e9df3d25e6b985fc23371ae", null ],
    [ "GetBookTabs", "db/dcb/class_expansion_book_menu_manager.html#a8b24fceeb933a8986bc74407e6fbb841", null ],
    [ "GetCraftingTab", "db/dcb/class_expansion_book_menu_manager.html#ab2891eff6031d39dc073759dda317894", null ],
    [ "GetRulesTab", "db/dcb/class_expansion_book_menu_manager.html#a07e9ffd2a825212c863c95a705c55242", null ],
    [ "GetServerInfoTab", "db/dcb/class_expansion_book_menu_manager.html#a4939aa3c2f1b78a7ba20b9272b9707e7", null ],
    [ "RegisterBookMenuTab", "db/dcb/class_expansion_book_menu_manager.html#ab55d135619a1d13611b9dbd9e20ba6e1", null ],
    [ "RegisterBookMenuTabs", "db/dcb/class_expansion_book_menu_manager.html#a31040716067364d1c30443c4a6d01bbe", null ],
    [ "RemoveBookMenuTab", "db/dcb/class_expansion_book_menu_manager.html#ad714e9bedfb24a2dc67b21d7f3b6a57e", null ],
    [ "UpdateBookMenuTabs", "db/dcb/class_expansion_book_menu_manager.html#ab9e612ddb9d609d5c3770e5e6ad29527", null ],
    [ "m_CraftingTab", "db/dcb/class_expansion_book_menu_manager.html#a1c92f4081495cbcd48df81780804bfec", null ],
    [ "m_RulesTab", "db/dcb/class_expansion_book_menu_manager.html#ab0794fe91629cc2d9e089b60b89b8e30", null ],
    [ "m_ServerInfoTab", "db/dcb/class_expansion_book_menu_manager.html#a93aac7d08df9b9c52202e9a079a00073", null ],
    [ "m_Tabs", "db/dcb/class_expansion_book_menu_manager.html#a92239f1e265119d02bc131de4ca5e558", null ]
];