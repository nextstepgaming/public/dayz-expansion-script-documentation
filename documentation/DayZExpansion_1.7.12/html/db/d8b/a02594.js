var a02594 =
[
    [ "CfgPatches", "db/d8b/a02594.html#da/d88/a06821", [
      [ "DayZExpansion_Sounds_Common", "d7/ddc/a06825.html", [
        [ "requiredAddons", "d7/ddc/a06825.html#ae6e3924fb332bd8ab6ec7613d3d47d34", null ],
        [ "requiredVersion", "d7/ddc/a06825.html#ab65e62b98cae558cefb5cc2dcd3e2a12", null ],
        [ "units", "d7/ddc/a06825.html#a604cc3db68f62ffbf13a71403deb99c1", null ],
        [ "weapons", "d7/ddc/a06825.html#a9680ee77b82448e42d1b96dab4182856", null ]
      ] ]
    ] ],
    [ "CfgPatches::DayZExpansion_Sounds_Common", "d7/ddc/a06825.html", "d7/ddc/a06825" ],
    [ "CfgSoundShaders", "db/d8b/a02594.html#d2/d8b/a06829", [
      [ "Expansion_Car_Lock_SoundShader", "d5/df0/a06857.html", [
        [ "samples", "d5/df0/a06857.html#abbbee4566dc4644525adb06a3a20f8e8", null ],
        [ "volume", "d5/df0/a06857.html#a3c035bb49b0fb3df3992408b57a1c01c", null ]
      ] ],
      [ "Expansion_CarLock_SoundShader", "dc/d2a/a06853.html", [
        [ "range", "dc/d2a/a06853.html#a9d96b8f079816c156f810f7a0ded853b", null ]
      ] ],
      [ "Expansion_Horn_Ext_SoundShader", "db/d2d/a06837.html", [
        [ "samples", "db/d2d/a06837.html#a952b73b6e0dcf997016bedd86b80e7e3", null ],
        [ "volume", "db/d2d/a06837.html#a815384326671b3959726f38d58960be3", null ]
      ] ],
      [ "Expansion_Horn_Int_SoundShader", "dc/dec/a06841.html", [
        [ "samples", "dc/dec/a06841.html#a8cc388111022a32c90ac6ffdf4b71f95", null ],
        [ "volume", "dc/dec/a06841.html#aac326c64fca45cce42a6f005da97ee7d", null ]
      ] ],
      [ "Expansion_Horn_SoundShader", "d9/d66/a06833.html", [
        [ "range", "d9/d66/a06833.html#a318e376b39cd19e4528d4417a1559281", null ]
      ] ],
      [ "Expansion_Truck_Horn_Ext_SoundShader", "d7/db6/a06845.html", [
        [ "samples", "d7/db6/a06845.html#ac761f1d71ac65cb9c0cd01746f1213cf", null ],
        [ "volume", "d7/db6/a06845.html#a8e55dda4bc37f43cccd8eb1af150f6c9", null ]
      ] ],
      [ "Expansion_Truck_Horn_Int_SoundShader", "d7/dc7/a06849.html", [
        [ "samples", "d7/dc7/a06849.html#ae4a567088f27b7c5f8113d094292d99a", null ],
        [ "volume", "d7/dc7/a06849.html#abfbd965432a67707d4b383a88d8221f3", null ]
      ] ]
    ] ],
    [ "CfgSoundShaders::Expansion_Horn_SoundShader", "d9/d66/a06833.html", "d9/d66/a06833" ],
    [ "CfgSoundShaders::Expansion_Horn_Ext_SoundShader", "db/d2d/a06837.html", "db/d2d/a06837" ],
    [ "CfgSoundShaders::Expansion_Horn_Int_SoundShader", "dc/dec/a06841.html", "dc/dec/a06841" ],
    [ "CfgSoundShaders::Expansion_Truck_Horn_Ext_SoundShader", "d7/db6/a06845.html", "d7/db6/a06845" ],
    [ "CfgSoundShaders::Expansion_Truck_Horn_Int_SoundShader", "d7/dc7/a06849.html", "d7/dc7/a06849" ],
    [ "CfgSoundShaders::Expansion_CarLock_SoundShader", "dc/d2a/a06853.html", "dc/d2a/a06853" ],
    [ "CfgSoundShaders::Expansion_Car_Lock_SoundShader", "d5/df0/a06857.html", "d5/df0/a06857" ],
    [ "CfgSoundSets", "db/d8b/a02594.html#de/db3/a06861", [
      [ "Expansion_Car_Lock_SoundSet", "dd/db9/a06885.html", [
        [ "soundShaders", "dd/db9/a06885.html#aefa2635444b6fa023c6f945081a0a27d", null ],
        [ "volumeFactor", "dd/db9/a06885.html#a2e51b40052eb4de3cb8ad8ec777fcf1b", null ]
      ] ],
      [ "Expansion_Horn_Ext_SoundSet", "d5/dfb/a06869.html", [
        [ "soundShaders", "d5/dfb/a06869.html#a7635af162c482f6adefafe72761fd24c", null ],
        [ "volumeFactor", "d5/dfb/a06869.html#acbafb41fb619f64a5edee49afcc3b93f", null ]
      ] ],
      [ "Expansion_Horn_Int_SoundSet", "d6/d19/a06873.html", [
        [ "soundShaders", "d6/d19/a06873.html#a5013e670ae740b916fee5809a5fe1d01", null ],
        [ "volumeFactor", "d6/d19/a06873.html#a28ec358eba1b668a9ae93305782bcab8", null ]
      ] ],
      [ "Expansion_Horn_SoundSet", "d5/d2d/a06865.html", [
        [ "distanceFilter", "d5/d2d/a06865.html#a441bce50a6d67f7b20f16a039718db69", null ],
        [ "loop", "d5/d2d/a06865.html#ad7e7518dcdaae245f4070a29cffc77d9", null ],
        [ "sound3DProcessingType", "d5/d2d/a06865.html#a0f408b25ab5c51ef5d65caeed872a0c1", null ],
        [ "spatial", "d5/d2d/a06865.html#a68f2c76a0e08b84755f24f3a45bd7553", null ],
        [ "volumeCurve", "d5/d2d/a06865.html#a2fc5f84e229aa5fab0d740ff6cd005bd", null ],
        [ "volumeFactor", "d5/d2d/a06865.html#a1b44204878798af2cf699725d7645654", null ]
      ] ],
      [ "Expansion_Truck_Horn_Ext_SoundSet", "d1/d85/a06877.html", [
        [ "soundShaders", "d1/d85/a06877.html#abeade9b174d04512270273a8a667d810", null ],
        [ "volumeFactor", "d1/d85/a06877.html#a2827968068127d4f7c7d6767ba3b9780", null ]
      ] ],
      [ "Expansion_Truck_Horn_Int_SoundSet", "dd/dc5/a06881.html", [
        [ "soundShaders", "dd/dc5/a06881.html#a2d8f69f1d5cb3933655793b9c055798d", null ],
        [ "volumeFactor", "dd/dc5/a06881.html#ad916f342c7c493d6511ce2f4d8b4681e", null ]
      ] ]
    ] ],
    [ "CfgSoundSets::Expansion_Horn_SoundSet", "d5/d2d/a06865.html", "d5/d2d/a06865" ],
    [ "CfgSoundSets::Expansion_Horn_Ext_SoundSet", "d5/dfb/a06869.html", "d5/dfb/a06869" ],
    [ "CfgSoundSets::Expansion_Horn_Int_SoundSet", "d6/d19/a06873.html", "d6/d19/a06873" ],
    [ "CfgSoundSets::Expansion_Truck_Horn_Ext_SoundSet", "d1/d85/a06877.html", "d1/d85/a06877" ],
    [ "CfgSoundSets::Expansion_Truck_Horn_Int_SoundSet", "dd/dc5/a06881.html", "dd/dc5/a06881" ],
    [ "CfgSoundSets::Expansion_Car_Lock_SoundSet", "dd/db9/a06885.html", "dd/db9/a06885" ]
];