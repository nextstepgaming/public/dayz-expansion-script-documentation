var class_expansion_action_destroy_territory =
[
    [ "ExpansionActionDestroyTerritory", "db/de6/class_expansion_action_destroy_territory.html#a65adfb13b7cd1827aa7ace6259f218da", null ],
    [ "ActionCondition", "db/de6/class_expansion_action_destroy_territory.html#a8e7f3665806e7e71a3ad6549d9198991", null ],
    [ "ActionConditionContinue", "db/de6/class_expansion_action_destroy_territory.html#a6e603a23243ecd3bef675c07285a3006", null ],
    [ "CreateConditionComponents", "db/de6/class_expansion_action_destroy_territory.html#a0944a225a27cbe0b49765249a61cf17d", null ],
    [ "DismantleCondition", "db/de6/class_expansion_action_destroy_territory.html#a10846cc4b156432a33b56595d18223b5", null ],
    [ "GetAdminLogMessage", "db/de6/class_expansion_action_destroy_territory.html#aed958bfee856a17bc3a6be50098b36ed", null ],
    [ "GetText", "db/de6/class_expansion_action_destroy_territory.html#af3c358c897e390bd7f8381c8b8816591", null ],
    [ "OnFinishProgressServer", "db/de6/class_expansion_action_destroy_territory.html#ac261726166ae3e832a442b359b02b844", null ],
    [ "SetBuildingAnimation", "db/de6/class_expansion_action_destroy_territory.html#a55b9f4ef17ba168f005a2c8742c3ab19", null ],
    [ "SetupAction", "db/de6/class_expansion_action_destroy_territory.html#a4a0a6a16b0477f44eedc4ac0b4633735", null ]
];