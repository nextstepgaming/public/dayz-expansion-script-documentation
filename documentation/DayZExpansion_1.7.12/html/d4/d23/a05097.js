var a05097 =
[
    [ "ExpansionDialogContent_TextScroller", "d4/d23/a05097.html#aad7095c0fd4077271a3f17b0f1878a1e", null ],
    [ "GetControllerType", "d4/d23/a05097.html#ae8789f26df7cc8a71486345406ce1ae3", null ],
    [ "GetDialogText", "d4/d23/a05097.html#a25d5d9d76ec0810edee9ee7a8101b64c", null ],
    [ "GetLayoutFile", "d4/d23/a05097.html#a1ad0f6468974b205c8c67f81259cc0d2", null ],
    [ "OnShow", "d4/d23/a05097.html#ac3be30cbc7bae8e3b2d312220dc6568f", null ],
    [ "SetContent", "d4/d23/a05097.html#aea63e82b76baf63dc22d3a9b9e69ecfe", null ],
    [ "SetDialogText", "d4/d23/a05097.html#aa1bb6427d3a40cb94547efb2935b7a53", null ],
    [ "SetTextColor", "d4/d23/a05097.html#aa150392974377d4761ecdfb28e870291", null ],
    [ "dialog_scroller_text", "d4/d23/a05097.html#aec66afa8df01ce7cff4c5acedae7b0fd", null ],
    [ "m_Text", "d4/d23/a05097.html#aa52a2f0677c185181f71fbb59b74bfce", null ],
    [ "m_TextScrollerController", "d4/d23/a05097.html#a92b3112ed6783afb5357af2968f45f4b", null ]
];