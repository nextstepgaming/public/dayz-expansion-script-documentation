var a01418 =
[
    [ "ExpansionHardlineItemRarity", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5", [
      [ "NONE", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "Common", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5a9cc00ea16fb915697974900a62a04062", null ],
      [ "Uncommon", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5a0d94e9139b5bb4a07ed79382ab048bad", null ],
      [ "Rare", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5ad79c27263931c9594adaa273aada356d", null ],
      [ "Epic", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5a7571136c0686b177e9af8280bf4af857", null ],
      [ "Legendary", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5a32c88b3eeb323eaa4e772b982aa29915", null ],
      [ "Mythic", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5a20aa3427ce68449285b070763b6f3c2c", null ],
      [ "Exotic", "d4/d3c/a01418.html#a182896c3c414084d7214871d2338b7f5af48a280b5a74489442e1a7959de9b854", null ]
    ] ],
    [ "ExpansionHardlineItemTier", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219f", [
      [ "TIER_1", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fa476a7c03cf36a7d40878d8155afc089c", null ],
      [ "TIER_2", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fab400154cf971978036c95242805148de", null ],
      [ "TIER_3", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fa012e6cb02ae05bb3bd3917a72d63d1aa", null ],
      [ "TIER_4", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fad521b66900d38236c68f006a96919e9f", null ],
      [ "TIER_5", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fa758e4b3cbcb92b424c5245582db98e2b", null ],
      [ "TIER_6", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219faba2c946f7f3d06275153d4004747d431", null ],
      [ "TIER_7", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fa3640ed7bc50a6aaa934274b6ffb45d5f", null ],
      [ "TIER_8", "d4/d3c/a01418.html#a31e96dd8760e75548103aa24a3f3219fa7ff7108f840b43597081022e06ff882f", null ]
    ] ],
    [ "ExpansionHardlineRank", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52", [
      [ "INVALID", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52aef2863a469df3ea6871d640e3669a2f2", null ],
      [ "Bambi", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a090abfc37509203dea7effddd64aea72", null ],
      [ "Survivor", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52ac22d07308c5dcbe0847ab70e0584c81b", null ],
      [ "Scout", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a40d13cd4aee7ad4d0392d174c655eda7", null ],
      [ "Pathfinder", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a2e868c33b66a8999b838783a3eb1a243", null ],
      [ "Hero", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a445c4e6d5306679aae8f3200f10353a0", null ],
      [ "Superhero", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a67c7a13f2e10fede899b17e087609d31", null ],
      [ "Legend", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a5a2a733d43ed291ac9063c7b2b61b74a", null ],
      [ "Kleptomaniac", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a0c13fa916262748a3d0ab097d23422fc", null ],
      [ "Bully", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a43903333efc08f45937b3a458f84fd99", null ],
      [ "Bandit", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a1e21403b5022209527d6663f7853ad61", null ],
      [ "Killer", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a2c6310def166b1fd3b80f8939267eb22", null ],
      [ "Madman", "d4/d3c/a01418.html#a3b71495ddc59f25c16e5b2daf514db52a291c5051e3a6ae5567ee17bbf875dfe1", null ]
    ] ]
];