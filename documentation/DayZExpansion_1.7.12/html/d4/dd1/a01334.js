var a01334 =
[
    [ "ExpansionDialogContent_Text", "dc/d88/a05085.html", "dc/d88/a05085" ],
    [ "ExpansionDialogContent_TextController", "de/d1a/a05089.html", "de/d1a/a05089" ],
    [ "ExpansionMenuDialogContent_TextController", "d3/df5/a05093.html", "d3/df5/a05093" ],
    [ "ExpansionMenuDialogContent_Text", "d4/dd1/a01334.html#a3a744778e6fcfabd0b481b677172bc9b", null ],
    [ "GetControllerType", "d4/dd1/a01334.html#aae475e0b4d95b152ef3e68af16944a01", null ],
    [ "GetLayoutFile", "d4/dd1/a01334.html#af50c208477ace3db233bfb6243901f7c", null ],
    [ "GetText", "d4/dd1/a01334.html#ad37cd855eec1e2636806ff274469540a", null ],
    [ "OnShow", "d4/dd1/a01334.html#a44c86b20c27f7a8ddb7c1337e9538f80", null ],
    [ "SetContent", "d4/dd1/a01334.html#a145033b88bc00b5e2dc75940a0b6ddb8", null ],
    [ "SetText", "d4/dd1/a01334.html#a47a3d1a4bd1182cef70d052c48bc5eb7", null ],
    [ "SetTextColor", "d4/dd1/a01334.html#a8a8a420577d32cf39a2ac11d1e97c537", null ],
    [ "dialog_text", "d4/dd1/a01334.html#a1a501a8971a2838045fd057e30e25b1e", null ],
    [ "m_Text", "d4/dd1/a01334.html#af5260a044e7e2e3996533ab172c2b342", null ],
    [ "m_TextController", "d4/dd1/a01334.html#a1f12cb6dca45dcd1307492b6b1fe8490", null ]
];