var a06653 =
[
    [ "Update", "d4/d5f/a06653.html#a8db97dd8cf5379f2223a6f6b492841fe", null ],
    [ "NotifierDividerColor", "d4/d5f/a06653.html#a741587fe80124f63f732afa8cc19b284", null ],
    [ "NotifiersHalfColor", "d4/d5f/a06653.html#ad791855b2b4a0376b2b3020c612dd7f6", null ],
    [ "NotifiersIdealColor", "d4/d5f/a06653.html#a6a989a88639f4015ef390e2c6874e6b8", null ],
    [ "NotifiersLowColor", "d4/d5f/a06653.html#a8fb0bd4b5dc943908c4129f1bbca4668", null ],
    [ "StaminaBarColor", "d4/d5f/a06653.html#ad11ad1c6f52a4167c3c6473a946d4361", null ],
    [ "TemperatureBurningColor", "d4/d5f/a06653.html#a7c64f8c2185bd33c379353eb4a843952", null ],
    [ "TemperatureColdColor", "d4/d5f/a06653.html#af2c1a4f4bbffaa7aa1bc95809fa61d8e", null ],
    [ "TemperatureFreezingColor", "d4/d5f/a06653.html#ab307c0ba563f9e26225d799873ae09ad", null ],
    [ "TemperatureHotColor", "d4/d5f/a06653.html#a65ef39d7572364c15f211127af067bc1", null ],
    [ "TemperatureIdealColor", "d4/d5f/a06653.html#ab378800525dbb93b9f2cc949ff4efc02", null ]
];