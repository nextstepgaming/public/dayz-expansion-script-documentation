var a00917 =
[
    [ "ExpansionSafeZoneSettingsBase", "d7/d17/a04381.html", "d7/d17/a04381" ],
    [ "ExpansionSafeZoneSettings", "de/d09/a04385.html", "de/d09/a04385" ],
    [ "CircleZones", "d4/ddc/a00917.html#a80f12b968e61dc48ca4488fde825bd97", null ],
    [ "Enabled", "d4/ddc/a00917.html#a558f5c44426d0eb7abb82a65e8892d9a", null ],
    [ "EnableVehicleinvincibleInsideSafeZone", "d4/ddc/a00917.html#a4a24cd99d951245c0e4c49551c1d7418", null ],
    [ "FrameRateCheckSafeZoneInMs", "d4/ddc/a00917.html#a63fa878491bbb94f0bb5d95f02735cb2", null ],
    [ "PolygonZones", "d4/ddc/a00917.html#a95f36a14026d170362326ba1c56e4444", null ]
];