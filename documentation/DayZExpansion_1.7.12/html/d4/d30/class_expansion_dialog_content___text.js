var class_expansion_dialog_content___text =
[
    [ "ExpansionDialogContent_Text", "d4/d30/class_expansion_dialog_content___text.html#a49d87798e2e42f6e314ed45cdd10e8dc", null ],
    [ "GetControllerType", "d4/d30/class_expansion_dialog_content___text.html#a24d0984c5f361b125785f7bff00616c1", null ],
    [ "GetLayoutFile", "d4/d30/class_expansion_dialog_content___text.html#afc8dd53dc45a0adfbe5524c199bcd26b", null ],
    [ "OnShow", "d4/d30/class_expansion_dialog_content___text.html#aa4035fbc9b4cc4a6d2ce4587ca00788c", null ],
    [ "SetContent", "d4/d30/class_expansion_dialog_content___text.html#a713fd2a764f54154e1b0eb737fece8d6", null ],
    [ "SetText", "d4/d30/class_expansion_dialog_content___text.html#a683cccb35772e09abb5a3d398b4c0b38", null ],
    [ "SetTextColor", "d4/d30/class_expansion_dialog_content___text.html#a2bfad1fc750793255a6c44f65531157e", null ],
    [ "dialog_text", "d4/d30/class_expansion_dialog_content___text.html#ab83aebf4504452f96c1fa3e5a3b526ed", null ],
    [ "m_Text", "d4/d30/class_expansion_dialog_content___text.html#a72a1b3aef526ebb1456d3c3fa5ee3989", null ],
    [ "m_TextController", "d4/d30/class_expansion_dialog_content___text.html#a982d40252be7e42020542d58ebcaaa96", null ]
];