var a06593 =
[
    [ "DefaultText", "d4/d24/a06593.html#a828f3fdfced77c3dca96601389687fb5", null ],
    [ "HumanityVal", "d4/d24/a06593.html#a175a7101f9eb4134d7bae2ad98b1c408", null ],
    [ "ObjectiveItems", "d4/d24/a06593.html#abad8e9d17d8cb41939f8654b25fe21ae", null ],
    [ "QuestDescription", "d4/d24/a06593.html#a9cf6bdaa377a1a58c029d615de83bfdf", null ],
    [ "QuestNPCName", "d4/d24/a06593.html#a417025a6ee39fc412085669fa67fd8c8", null ],
    [ "QuestObjective", "d4/d24/a06593.html#ae41044923c6d959318b1c09a70a531d7", null ],
    [ "Quests", "d4/d24/a06593.html#ac07c6e2c665556f7621f737d130825e0", null ],
    [ "QuestTitle", "d4/d24/a06593.html#a85876854d0b50b5cf4c1da015b654917", null ],
    [ "RewardEntries", "d4/d24/a06593.html#ac4c082213fe026c9a2992f87085b4725", null ]
];