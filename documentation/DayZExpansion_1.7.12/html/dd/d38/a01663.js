var a01663 =
[
    [ "~ExpansionExplosive", "dd/d38/a01663.html#a98031917408da86208e4e51a0d544721", null ],
    [ "ItemBase", "dd/d38/a01663.html#acfee53d7ba49c3b997e4b217a0bdc093", null ],
    [ "~ItemBase", "dd/d38/a01663.html#a740c0cca5784891108a8046f861aea9d", null ],
    [ "ItemBase", "dd/d38/a01663.html#acfee53d7ba49c3b997e4b217a0bdc093", null ],
    [ "_ExpansionAddConnection", "dd/d38/a01663.html#a99149beade3b7278cbdbd17d76d75c7d", null ],
    [ "_ExpansionRemoveConnection", "dd/d38/a01663.html#a722437f7b4a29f82be087f43722fc7dc", null ],
    [ "AddUser", "dd/d38/a01663.html#a9ada0d190efdd5c7a3b98537bf9bd83a", null ],
    [ "AfterStoreLoad", "dd/d38/a01663.html#a894a167c49bbd606efeaae2798c29d38", null ],
    [ "CanBeDamaged", "dd/d38/a01663.html#a7581a19f3435f6b4ce02d9d915aafed9", null ],
    [ "CanClose", "dd/d38/a01663.html#ac1c862a68706592555e6c3daf0917287", null ],
    [ "CanDetachAttachment", "dd/d38/a01663.html#a8e5514abc697470b14e82092d358a01a", null ],
    [ "CanPutInCargo", "dd/d38/a01663.html#ab4b393b9696f628eb8f493f86cecdd39", null ],
    [ "CanPutInCargo", "dd/d38/a01663.html#ab4b393b9696f628eb8f493f86cecdd39", null ],
    [ "CanPutIntoHands", "dd/d38/a01663.html#a4197d374931f370f21e33572be42b5bb", null ],
    [ "CanReceiveAttachment", "dd/d38/a01663.html#ab4769ca97816420aa353ac0441bb1dba", null ],
    [ "CanReceiveItemIntoCargo", "dd/d38/a01663.html#a1a89e3ce68b290b5c37d7d637973f400", null ],
    [ "CanReleaseAttachment", "dd/d38/a01663.html#ab4f501dcea1fc9f18436385bd2701fc2", null ],
    [ "CanReleaseCargo", "dd/d38/a01663.html#a2e4c369be8b8a82d385ac5a0394e8670", null ],
    [ "Close", "dd/d38/a01663.html#a1110b124d3df9f293720a48d362a2085", null ],
    [ "Close", "dd/d38/a01663.html#a097c0665e771a28ac2b17c9af4f536ed", null ],
    [ "CloseAndLock", "dd/d38/a01663.html#a9594e808136d2422fc7922574135a402", null ],
    [ "DeferredInit", "dd/d38/a01663.html#aac771abcee14bf403cf6643d33e8e926", null ],
    [ "DeferredInit", "dd/d38/a01663.html#aac771abcee14bf403cf6643d33e8e926", null ],
    [ "EEDelete", "dd/d38/a01663.html#a3151bf358d3f46c4dee788cae9f15c94", null ],
    [ "EEHealthLevelChanged", "dd/d38/a01663.html#a7df516e6c0f3a7b4ac5720ef9f45c19b", null ],
    [ "EEHitBy", "dd/d38/a01663.html#afef76f4f25df4573dfc08bda7d5676e1", null ],
    [ "EEInit", "dd/d38/a01663.html#ab567e8645c3fa347625b560a24329100", null ],
    [ "EEItemLocationChanged", "dd/d38/a01663.html#a530aacddc867231d8da109b952ac4794", null ],
    [ "EEKilled", "dd/d38/a01663.html#acec5e7dd659fc49f39181f99ad1a151c", null ],
    [ "EEOnAfterLoad", "dd/d38/a01663.html#a2a79375819d7081e3db60da5f7e80035", null ],
    [ "EEOnDamageCalculated", "dd/d38/a01663.html#ad5dd91eb7377fac6ac3e5cd60a45a155", null ],
    [ "EEOnDamageCalculated", "dd/d38/a01663.html#ad5dd91eb7377fac6ac3e5cd60a45a155", null ],
    [ "Expansion_CanObjectAttach", "dd/d38/a01663.html#a4dab0dda0a7f0c42e577f8731edc35ce", null ],
    [ "Expansion_CarContactActivates", "dd/d38/a01663.html#a7f86d79c7d6f20ee5db685752e2e8f00", null ],
    [ "Expansion_DecreaseStackAmount", "dd/d38/a01663.html#a20e4956068414aaa9768f395730ee73f", null ],
    [ "Expansion_DequeueEntityActions", "dd/d38/a01663.html#a7507a7ae28ae818115e33c9dcd3396c0", null ],
    [ "Expansion_GetAttachmentSlots", "dd/d38/a01663.html#ae4b2c2bf15a6e62b6920faa7f8673d19", null ],
    [ "Expansion_GetBatteryEnergy", "dd/d38/a01663.html#a4b4f927b9c848e520bd0575cd77ba194", null ],
    [ "Expansion_GetFamilyType", "dd/d38/a01663.html#af2677b694c4945b64a08b03112f4da11", null ],
    [ "Expansion_GetInventorySlots", "dd/d38/a01663.html#ac2f2a2f2b046119a848ac043716b440d", null ],
    [ "Expansion_GetStackAmount", "dd/d38/a01663.html#adf819347da22b98d46655d0de2c0c9a4", null ],
    [ "Expansion_HasAttachmentSlot", "dd/d38/a01663.html#a1c2d516bfb6f96c0edf8c55c000d3c3f", null ],
    [ "Expansion_IsAdminTool", "dd/d38/a01663.html#abe4c1186aae7e2c9221ff5a57f4342b0", null ],
    [ "Expansion_IsMeleeWeapon", "dd/d38/a01663.html#a80c99b7d05de27968f1c8469c32fd558", null ],
    [ "Expansion_IsStackable", "dd/d38/a01663.html#a518ff928fc4d03d0edfe3f4038b17759", null ],
    [ "Expansion_QueueEntityActions", "dd/d38/a01663.html#a1e9c24a1abc11d321eb62a06b9535541", null ],
    [ "Expansion_SetIsMeleeWeapon", "dd/d38/a01663.html#ad7320a1ba3e9700c537b8f6b1e955835", null ],
    [ "Expansion_SetStackAmount", "dd/d38/a01663.html#a9eb743b01544c9716280b5fc2ff19b0f", null ],
    [ "ExpansionActionOnAnimationEvent", "dd/d38/a01663.html#aa6b116abfe18ee1662ada9cad16afc89", null ],
    [ "ExpansionActionOnEnd", "dd/d38/a01663.html#a161602c10dc84eaa9ba49d32791eaa9d", null ],
    [ "ExpansionActionOnEndAnimationLoop", "dd/d38/a01663.html#a9a94f27fef4303f380b84684de9bb01a", null ],
    [ "ExpansionActionOnEndInput", "dd/d38/a01663.html#a40c36c95843c721d384dad6787f4e7b1", null ],
    [ "ExpansionActionOnEndRequest", "dd/d38/a01663.html#a463fe0fa7cb1545c6b28a6b8d40e75f5", null ],
    [ "ExpansionActionOnFinishProgress", "dd/d38/a01663.html#ad42d31872610edcde0a61fc7a798e369", null ],
    [ "ExpansionActionOnStageEnd", "dd/d38/a01663.html#a2ba8840fec8013996a59d019bdbc0f75", null ],
    [ "ExpansionActionOnStageStart", "dd/d38/a01663.html#add9df3ccc68f45665f238087e03bb0b4", null ],
    [ "ExpansionActionOnStart", "dd/d38/a01663.html#a6d418e5fd10d9bdf2f831b81d8d024e0", null ],
    [ "ExpansionActionOnStartAnimationLoop", "dd/d38/a01663.html#a2aab455a211edf9df60946ec9d493f68", null ],
    [ "ExpansionActionOnUpdate", "dd/d38/a01663.html#a55560c006e62f43e3121e1c9b0a3d3ae", null ],
    [ "ExpansionAddConnection", "dd/d38/a01663.html#a62e59998f808874b961c9a87cd5e7283", null ],
    [ "ExpansionCanClose", "dd/d38/a01663.html#a563a5fb760d145e6a0f64d056fbec1e9", null ],
    [ "ExpansionCanOpen", "dd/d38/a01663.html#a661d0be085c1e8ff67eff29f5abf3d11", null ],
    [ "ExpansionCanRecievePower", "dd/d38/a01663.html#a5008c46e84f3331979caefe3c846196c", null ],
    [ "ExpansionCanRecievePower", "dd/d38/a01663.html#a5008c46e84f3331979caefe3c846196c", null ],
    [ "ExpansionCanRecievePower", "dd/d38/a01663.html#a8ba1453afbea17ad6fbca18a325e2623", null ],
    [ "ExpansionCodeLock", "dd/d38/a01663.html#a9939ba1576a01c48c4411fa06da78a6b", null ],
    [ "ExpansionCreateAttachment", "dd/d38/a01663.html#a671e3f95754c3c34813634d86eb51671", null ],
    [ "ExpansionCreateCleanup", "dd/d38/a01663.html#acd47aae7162f2ed9fddf898936b78bda", null ],
    [ "ExpansionCreateDynamicFromContact", "dd/d38/a01663.html#a123edba46433c471cb179c40cb64f2fa", null ],
    [ "ExpansionCreateInInventory", "dd/d38/a01663.html#ac6d6606627daba745d06454333fc3c30", null ],
    [ "ExpansionDeferredCreateCleanup", "dd/d38/a01663.html#a3a01ab39c7363f7594a8b24a022ca777", null ],
    [ "ExpansionDisconnect", "dd/d38/a01663.html#ab68634efd56c5cf406969159f56b534e", null ],
    [ "ExpansionDropServer", "dd/d38/a01663.html#af8c0e855581ecfa5130c4222434f0b26", null ],
    [ "ExpansionExplosive", "dd/d38/a01663.html#a7410bcf20965b35e590144b6a07bbe3b", null ],
    [ "ExpansionFindCodeLockSlot", "dd/d38/a01663.html#ab4670c78d0fcf5b3bfa2b7c966c83d23", null ],
    [ "ExpansionGetCodeLock", "dd/d38/a01663.html#a52676972f5793354072ffb09ca20da09", null ],
    [ "ExpansionGetSkins", "dd/d38/a01663.html#a9a53991bbf2f9dc6092c23ab98c99fdf", null ],
    [ "ExpansionHandleCollision", "dd/d38/a01663.html#a66c22e96c2c4761ad9382750d717c199", null ],
    [ "ExpansionHasCodeLock", "dd/d38/a01663.html#a50bcd5ce8f6b8842d2effbb6f3b6ae18", null ],
    [ "ExpansionHasCodeLock", "dd/d38/a01663.html#aea8cd911c4c9645205a6f4163febcc36", null ],
    [ "ExpansionHasSkin", "dd/d38/a01663.html#ab8f12802c2d67b54d2750a1a63ae9feb", null ],
    [ "ExpansionIsConnected", "dd/d38/a01663.html#ad93ff1bcda13d385905addc947ffb4d4", null ],
    [ "ExpansionIsLiquidItem", "dd/d38/a01663.html#a820a1e7972b569e66decab70ad56fb0f", null ],
    [ "ExpansionIsLocked", "dd/d38/a01663.html#a1b94569f63e51cbb3f67ba452b98066d", null ],
    [ "ExpansionIsLocked", "dd/d38/a01663.html#adad79f8c9ebb222b0778530ce25124e2", null ],
    [ "ExpansionIsOpenable", "dd/d38/a01663.html#acc53eea86dce7235807e418d2fe6b1ab", null ],
    [ "ExpansionIsOpenable", "dd/d38/a01663.html#af213b709b04d0ed6275d9fbf2f43c72a", null ],
    [ "ExpansionIsOpened", "dd/d38/a01663.html#a5d35750dab01196ab85bd05b0f64fc32", null ],
    [ "ExpansionIsPowerSource", "dd/d38/a01663.html#a696bc96d979d022098f1b964127fcd12", null ],
    [ "ExpansionIsPowerSource", "dd/d38/a01663.html#a4e0cf3c79956f3b435387b7b62c8cea6", null ],
    [ "ExpansionLock", "dd/d38/a01663.html#ad32183997c375dacdfd11902b36dfeb4", null ],
    [ "ExpansionOnDestroyed", "dd/d38/a01663.html#a23257b790ec37848886f7ae726aad8af", null ],
    [ "ExpansionOnDestroyed", "dd/d38/a01663.html#a406e67625deb22993cdedfd007e594ef", null ],
    [ "ExpansionOnSkinDamageZoneUpdate", "dd/d38/a01663.html#a00455d952255dbef3a5758b2d13e851f", null ],
    [ "ExpansionOnSkinUpdate", "dd/d38/a01663.html#a07d9a9182c072d37df4c9f6bc7930568", null ],
    [ "ExpansionPhaseObject", "dd/d38/a01663.html#ade614acc513b43966be6715a13306294", null ],
    [ "ExpansionPowerSwitch", "dd/d38/a01663.html#a3472ea54adb64bef0af695d937a4bbf1", null ],
    [ "ExpansionSetSkin", "dd/d38/a01663.html#ac09ecb040b563d10461f145db0f55e12", null ],
    [ "ExpansionSetupSkins", "dd/d38/a01663.html#a9b3ba55797b4032f886acf3d6d07448e", null ],
    [ "ExpansionUnlock", "dd/d38/a01663.html#a3d90059cf8345e15798d1338423fc9aa", null ],
    [ "Explode", "dd/d38/a01663.html#a74effc347c6f9d638327286c26fbbacd", null ],
    [ "FailedUnlock", "dd/d38/a01663.html#a54a75aa020fabd88c24be58c4c2a3a25", null ],
    [ "GetCode", "dd/d38/a01663.html#aba586066d2660a4b12e816bc39008252", null ],
    [ "GetCodeLength", "dd/d38/a01663.html#a95af59c7789cb32a5ad6805ab59c1d05", null ],
    [ "GetDestroySound", "dd/d38/a01663.html#a8850a7a5bf95dc0aeefab30a4d762886", null ],
    [ "GetDestroySound", "dd/d38/a01663.html#af482a5ba33e96d19c189bb9d270ec710", null ],
    [ "GetExpansionSaveVersion", "dd/d38/a01663.html#adefb3aadd62f03880a6d7c3f900a53e1", null ],
    [ "GetHiddenSelectionIndex", "dd/d38/a01663.html#aad0644c3d1f235e13503631263acff13", null ],
    [ "HandleClientExplosion", "dd/d38/a01663.html#a530081f300ff92ed98d32ccc34cdb80c", null ],
    [ "HasCode", "dd/d38/a01663.html#ab2db4aed076d5ad2f5199aa498b7060e", null ],
    [ "IsBasebuilding", "dd/d38/a01663.html#a2e057a36873a632dbfa34e3917c6d58f", null ],
    [ "IsDeployable", "dd/d38/a01663.html#aed45677f6aebd244234ee267473e0e08", null ],
    [ "IsDeployable", "dd/d38/a01663.html#aed45677f6aebd244234ee267473e0e08", null ],
    [ "IsExplosive", "dd/d38/a01663.html#afcbf86fd95dd7d871672394e8b07c80e", null ],
    [ "IsKnownUser", "dd/d38/a01663.html#a77bfda22a128dca3b0b90f1c2d48af16", null ],
    [ "IsLocked", "dd/d38/a01663.html#a602f77998973f5ce9b84b2a38e0fedf4", null ],
    [ "IsLocked", "dd/d38/a01663.html#ae283f1b6f166e49b079fc6914db95b68", null ],
    [ "IsOneHandedBehaviour", "dd/d38/a01663.html#af36bf8cdfafcba6bb526e2701bf1df5b", null ],
    [ "IsOneHandedBehaviour", "dd/d38/a01663.html#af36bf8cdfafcba6bb526e2701bf1df5b", null ],
    [ "IsOpen", "dd/d38/a01663.html#a8904ed66094596846fc2bdcb3a5a581e", null ],
    [ "IsOpened", "dd/d38/a01663.html#a706d1c97ef04c357c8a5ae6e6272ee23", null ],
    [ "OnCEUpdate", "dd/d38/a01663.html#afcee5107fc834c0cb2e3cb0874050519", null ],
    [ "OnEnterZone", "dd/d38/a01663.html#ae2413665b5d86944691b3e4e4d345899", null ],
    [ "OnExitZone", "dd/d38/a01663.html#a6137d9b37e575a367548ce6f39384bd8", null ],
    [ "OnFrame", "dd/d38/a01663.html#a22d26ada5956b87f46bb42f6c6f4eff5", null ],
    [ "OnInventoryEnter", "dd/d38/a01663.html#a0cdb77693373d5904933e8f5250db573", null ],
    [ "OnInventoryExit", "dd/d38/a01663.html#ac20fbf0db5404a706e8c1b57bba18a17", null ],
    [ "OnPlacementComplete", "dd/d38/a01663.html#abfb92e4a8a0ea66cda9a7c8b8098f7f3", null ],
    [ "OnRPC", "dd/d38/a01663.html#af1116ee53ca8353e4f2e50dd7b964198", null ],
    [ "OnStoreLoad", "dd/d38/a01663.html#a21e8ac0826ce4d40c6d511b0abc673a2", null ],
    [ "OnSwitchOff", "dd/d38/a01663.html#ab12223addced103a64249aa50cfdc834", null ],
    [ "OnSwitchOn", "dd/d38/a01663.html#a0755087dcadd8fd0d689cda57b373d40", null ],
    [ "OnVariablesSynchronized", "dd/d38/a01663.html#aa5c630f3e789c5bf07b46ce04e859546", null ],
    [ "OnVariablesSynchronized", "dd/d38/a01663.html#aa5c630f3e789c5bf07b46ce04e859546", null ],
    [ "OnVariablesSynchronized", "dd/d38/a01663.html#aa5c630f3e789c5bf07b46ce04e859546", null ],
    [ "OnVariablesSynchronized", "dd/d38/a01663.html#aa5c630f3e789c5bf07b46ce04e859546", null ],
    [ "OnWasAttached", "dd/d38/a01663.html#ab25ae69addd27f6c73194dd9cae125fa", null ],
    [ "OnWasDetached", "dd/d38/a01663.html#a275b23ae34e1bf9eec6dca0968556e6f", null ],
    [ "OnWorkStart", "dd/d38/a01663.html#a2b29afb0e524bcb961681f37190d9a9d", null ],
    [ "OnWorkStart", "dd/d38/a01663.html#a2b29afb0e524bcb961681f37190d9a9d", null ],
    [ "OnWorkStart", "dd/d38/a01663.html#a2b29afb0e524bcb961681f37190d9a9d", null ],
    [ "OnWorkStart", "dd/d38/a01663.html#a2b29afb0e524bcb961681f37190d9a9d", null ],
    [ "OnWorkStop", "dd/d38/a01663.html#a77c45495e57cffdd720f302a316dde02", null ],
    [ "OnWorkStop", "dd/d38/a01663.html#a77c45495e57cffdd720f302a316dde02", null ],
    [ "Open", "dd/d38/a01663.html#a705d10a8121269c5436972ceec948c71", null ],
    [ "Open", "dd/d38/a01663.html#a82f9866c70e714a96f0b31e3133d6f59", null ],
    [ "RemoveLater", "dd/d38/a01663.html#a24dfb69133ff3c2c86964301fb9b7ca5", null ],
    [ "RequestKnownUIDs", "dd/d38/a01663.html#af169593b2e148746144c94f5911f59a1", null ],
    [ "SendKnownUIDs", "dd/d38/a01663.html#a93f9669bf4988c3ceb32e204af0f9794", null ],
    [ "SendServerLockReply", "dd/d38/a01663.html#a8e540b82fc4d06bb60c6c837bea1aecd", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetActions", "dd/d38/a01663.html#a45bcceec0acd0ccdc9d6afd42d4e839a", null ],
    [ "SetCode", "dd/d38/a01663.html#aa766b041d57592d506fa2c0da0dbda14", null ],
    [ "SetSlotLock", "dd/d38/a01663.html#a564abdf39abf95e14327c06c60419655", null ],
    [ "SetTakeable", "dd/d38/a01663.html#aae0f8b2396fea256154ad8c30ffc1120", null ],
    [ "SetUser", "dd/d38/a01663.html#a974a2f77631ada726c25f3c4100a78e5", null ],
    [ "SoundC4Beep", "dd/d38/a01663.html#a49c4f3f16e43de64e81b960780717f54", null ],
    [ "SoundCodeLockFailedUnlock", "dd/d38/a01663.html#a6d65ec84bb94df6ef2941b15668e9d80", null ],
    [ "SoundCodeLockLocked", "dd/d38/a01663.html#a4b50cab5209002921cfbdb3753c6f6bf", null ],
    [ "SoundCodeLockUnlocked", "dd/d38/a01663.html#a5a3d79640b2e3577b4b2c08ea579bbc0", null ],
    [ "TriggerExplosion", "dd/d38/a01663.html#ae205ae7540ff0e4335aa90b3a549e6b3", null ],
    [ "TriggerSound", "dd/d38/a01663.html#a32d9cdac11d885d1792a71d553655006", null ],
    [ "Unlock", "dd/d38/a01663.html#ac39df555143745bfd9c6207d3bad542d", null ],
    [ "UnlockAndOpen", "dd/d38/a01663.html#a5e940243b6723319fba6925cd48b6952", null ],
    [ "UnlockServer", "dd/d38/a01663.html#a3fdf84d979febb271546faad12b4db26", null ],
    [ "UpdateVisuals", "dd/d38/a01663.html#a168f6c400289a40423bbefa2a63d3cbb", null ],
    [ "UpdateVisuals_Deferred", "dd/d38/a01663.html#a63f5c282b9c66fcd8eadf7ccb5726f7e", null ],
    [ "DEFAULT_MATERIAL", "dd/d38/a01663.html#a3bc01dffadf8d9d69073558f3a6f99db", null ],
    [ "GREEN_LIGHT_GLOW", "dd/d38/a01663.html#afe818bbab0e1dc09ae3c27ff9bb0a855", null ],
    [ "m_Armed", "dd/d38/a01663.html#a61e378d9a5b1a7dee9a2ef95d0b83d50", null ],
    [ "m_ArmedSynchRemote", "dd/d38/a01663.html#a1943207cdc4d13fb86408f6a076dd7e3", null ],
    [ "m_CanBeSkinned", "dd/d38/a01663.html#a1730644f40945e626cc26a8007b2a90b", null ],
    [ "m_Code", "dd/d38/a01663.html#a7c5ac4b65684c8b6b243313992dc695e", null ],
    [ "m_CodeLength", "dd/d38/a01663.html#a49b97c1424513b6dd0afd5570ed79bb5", null ],
    [ "m_CurrentSkin", "dd/d38/a01663.html#aa0bb3343317434e1bc3f65311e83e61f", null ],
    [ "m_CurrentSkinIndex", "dd/d38/a01663.html#a1e221bdaab0231cc2cd585838b3b5f59", null ],
    [ "m_CurrentSkinName", "dd/d38/a01663.html#a2d8836e00a8b043ac6c4d9e775c961e1", null ],
    [ "m_CurrentSkinSynchRemote", "dd/d38/a01663.html#a16b7bcc5c4ea8ff0007a17fd67680a2a", null ],
    [ "m_ElectricityConnections", "dd/d38/a01663.html#a5405c6e9a18b4df92ede5863234e75c4", null ],
    [ "m_ElectricitySource", "dd/d38/a01663.html#a38ecf6cd51d669a5acfec199786086a8", null ],
    [ "m_Expansion_AcceptingAttachment", "dd/d38/a01663.html#abb6a6a8cf6fe01d68dda094f4b1d2bb3", null ],
    [ "m_Expansion_CanPlayerAttach", "dd/d38/a01663.html#aaad9f353109ce20060d62ac83a37e2b1", null ],
    [ "m_Expansion_CanPlayerAttachSet", "dd/d38/a01663.html#a3ee9af8f5dcbe85b5796ed0e95c02fd3", null ],
    [ "m_Expansion_DamageMultiplier", "dd/d38/a01663.html#ac2e299f036b51adb02ad648ae182627f", null ],
    [ "m_Expansion_HealthBeforeHit", "dd/d38/a01663.html#a3c0be2369e383b6e512a49c0dd0de3c7", null ],
    [ "m_Expansion_IsAdminTool", "dd/d38/a01663.html#a814728c9bf89b972394d1ac0b0bcb8fe", null ],
    [ "m_Expansion_IsMeleeWeapon", "dd/d38/a01663.html#a54db6e8ac7f56237d0d181e9c2b7860e", null ],
    [ "m_Expansion_IsOpenable", "dd/d38/a01663.html#ab7a0e4e55b4a7d3d69c4c8e56e68e0be", null ],
    [ "m_Expansion_IsOpened", "dd/d38/a01663.html#a9e17f91daf1b04cfee3244401f55fa27", null ],
    [ "m_Expansion_IsStoreLoaded", "dd/d38/a01663.html#aeded2de0871152fffd1a641cb3b3fd5d", null ],
    [ "m_Expansion_IsStoreSaved", "dd/d38/a01663.html#a47fa99a479f8649520fe5f523ce07146", null ],
    [ "m_Expansion_IsWorking", "dd/d38/a01663.html#ae9cf40518f06d7522f9763c285156c88", null ],
    [ "m_Expansion_QueuedActions", "dd/d38/a01663.html#a8d948e49853c1f43ecce265e0a201d19", null ],
    [ "m_Expansion_SZCleanup", "dd/d38/a01663.html#af0304b360b51b17c55e7d750d654303e", null ],
    [ "m_ExpansionSaveVersion", "dd/d38/a01663.html#a78bd68be21d726b4f1dce2663b2111b8", null ],
    [ "m_Exploded", "dd/d38/a01663.html#a3571aa57ca5450090cafbe21c5b9d153", null ],
    [ "m_ExplodedSynchRemote", "dd/d38/a01663.html#ad258343d414ba5b2049dc24ca241fba0", null ],
    [ "m_ExplosionTime", "dd/d38/a01663.html#ab5076ff08b2d3d6ae39b5a9e5a4d7e10", null ],
    [ "m_ExplosionTimer", "dd/d38/a01663.html#a15d9e0645671b130e2851c78bb2d7599", null ],
    [ "m_KnownUIDs", "dd/d38/a01663.html#ae00efb71281360ed79bd42782661ee22", null ],
    [ "m_KnownUIDsRequested", "dd/d38/a01663.html#a5f4580d7648449efc297dd476a6bd5f5", null ],
    [ "m_KnownUIDsSet", "dd/d38/a01663.html#aa216a27920f78722cd61dc6138d88f97", null ],
    [ "m_Locked", "dd/d38/a01663.html#ac2e29dcf33f9dc97304f4734462f55ce", null ],
    [ "m_ParticleEfx", "dd/d38/a01663.html#ae7cdeb23778e929991aa8021596e71e9", null ],
    [ "m_SkinModule", "dd/d38/a01663.html#ad19dc52361016aa940463fc016c515e2", null ],
    [ "m_Skins", "dd/d38/a01663.html#ac32a11b8279ea85b501d504252af3337", null ],
    [ "m_Sound", "dd/d38/a01663.html#a914f89bce3030715154a6d79ac2acdd9", null ],
    [ "m_Time", "dd/d38/a01663.html#a19357b68e4ad3588fb0bbcf5a6a343ae", null ],
    [ "m_Timer", "dd/d38/a01663.html#aa62967d984066859b4af7b4bcb31e4f9", null ],
    [ "m_WasLocked", "dd/d38/a01663.html#a57416c510e6f0b41c3332626672ed244", null ],
    [ "m_WasSynced", "dd/d38/a01663.html#a52871227a3a4141e039f275bcdf11fbc", null ],
    [ "RED_LIGHT_GLOW", "dd/d38/a01663.html#a9f2b57af4182b1f691b8a1876ec5fca7", null ],
    [ "s_Expansion_CodeLockSlotNames", "dd/d38/a01663.html#acf72df17e5ced65ce22e70dbd5e13186", null ],
    [ "s_Expansion_InventorySlots", "dd/d38/a01663.html#a1ce06e776a5ae51952741db6fa11d1e5", null ]
];