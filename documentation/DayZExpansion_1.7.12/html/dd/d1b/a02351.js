var a02351 =
[
    [ "LampModeEnum", "dd/d1b/a02351.html#a7a33e66313c9659fc7c9f767a574c59f", [
      [ "Disabled", "dd/d1b/a02351.html#a7a33e66313c9659fc7c9f767a574c59faf31108c24daa76e574c8259d9fe46c09", null ],
      [ "RequireGenerator", "dd/d1b/a02351.html#a7a33e66313c9659fc7c9f767a574c59fa38a83abcd73e3bc118e32e1a657c33d6", null ],
      [ "AlwaysOn", "dd/d1b/a02351.html#a7a33e66313c9659fc7c9f767a574c59fa234ad444e81508b3c4e21b7bb3e52ada", null ],
      [ "AlwaysOnEverywhere", "dd/d1b/a02351.html#a7a33e66313c9659fc7c9f767a574c59fa82f9527fd542b7ccaebad656d90fca21", null ]
    ] ]
];