var a02127 =
[
    [ "ExpansionChatSettings", "d1/d90/a02127.html#a41eeb2c33e57e3f9d6953cf5b4259d81", null ],
    [ "Copy", "d1/d90/a02127.html#ae73ad2e3f875fd2671fecf56aa932686", null ],
    [ "CopyInternal", "d1/d90/a02127.html#a71d4264be629e0ed3191badc36360c28", null ],
    [ "CopyInternal", "d1/d90/a02127.html#af8e1cab6009f10158546806e58b4a150", null ],
    [ "Defaults", "d1/d90/a02127.html#a9208f5a1b93eda249ea1194b69e3c297", null ],
    [ "IsLoaded", "d1/d90/a02127.html#aedd70e4c6a58547068f67bda8a12709c", null ],
    [ "OnLoad", "d1/d90/a02127.html#ab3f5e813da3f5747dd4ad9c0bc72d7ff", null ],
    [ "OnRecieve", "d1/d90/a02127.html#a5be39ba477a91323b9f9417437cf8af5", null ],
    [ "OnSave", "d1/d90/a02127.html#a837ecabc2f7cee4536a3ca9f2e82d337", null ],
    [ "OnSend", "d1/d90/a02127.html#a1e3ce857df5129e1f04941a60d4b5c53", null ],
    [ "Send", "d1/d90/a02127.html#afc1efeb703ff074b1e9bf6aa7f1edfb4", null ],
    [ "SettingName", "d1/d90/a02127.html#ad7efc6afcea21d08028208f3d9165afb", null ],
    [ "Unload", "d1/d90/a02127.html#adec8ef892f95d31953f266076b2cffe2", null ],
    [ "Update", "d1/d90/a02127.html#a3e975a31c62e9770a404dc78371441fc", null ],
    [ "ChatColors", "d1/d90/a02127.html#af9306220dc11dd4994f130111ed80abc", null ],
    [ "m_IsLoaded", "d1/d90/a02127.html#afff93e0894f8988c66ce0beb7a95d46b", null ],
    [ "VERSION", "d1/d90/a02127.html#a7660a4c6ea701984665b20be3b358a3b", null ]
];