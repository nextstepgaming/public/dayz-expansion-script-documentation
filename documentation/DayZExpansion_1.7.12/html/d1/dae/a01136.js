var a01136 =
[
    [ "ExpansionActionToolBaseCB", "d2/db6/a04681.html", "d2/db6/a04681" ],
    [ "ActionCondition", "d1/dae/a01136.html#aa9aa4864fae11769c65e644b5fd97c66", null ],
    [ "CreateAndSetupActionCallback", "d1/dae/a01136.html#ad7c2b71ba925a43a0fef9ef4aa765963", null ],
    [ "ExpansionActionToolBase", "d1/dae/a01136.html#a2ec52ac05c8ad9e5ed942654ca2bf05b", null ],
    [ "GetActualTargetObject", "d1/dae/a01136.html#ab13031d4de848683f92141940303ee08", null ],
    [ "GetTargetItem", "d1/dae/a01136.html#a7e310183f43025ce658eacd370b5c9d5", null ],
    [ "OnEndAnimationLoopServer", "d1/dae/a01136.html#acda6d37054929cd6366f74e65d1fcb37", null ],
    [ "OnFinishProgressServer", "d1/dae/a01136.html#a00cda41fe9dd093059da5c4b83b591bc", null ],
    [ "OnStartAnimationLoopServer", "d1/dae/a01136.html#a498da9d7d9b0a9126f488206f249e2f2", null ],
    [ "Setup", "d1/dae/a01136.html#ae6a4a9c901dbed9f819534aac090ffdb", null ],
    [ "m_Cycles", "d1/dae/a01136.html#add986a8b0d3f724b01921fec4db98151", null ],
    [ "m_MinHealth01", "d1/dae/a01136.html#a5d58fa44f06a47200f78a92dedcb5efd", null ],
    [ "m_TargetName", "d1/dae/a01136.html#a4749cf45b5ac2fa0714864c058c69ef7", null ],
    [ "m_Time", "d1/dae/a01136.html#a251cefc67ac9028e215f99dcff20e945", null ],
    [ "m_ToolDamagePercent", "d1/dae/a01136.html#a51b79bdc2b37130cb81f4e4dece7bcc8", null ]
];