var a02591 =
[
    [ "ExpansionTabType", "d1/de9/a02591.html#ac114bc833ee6820805f2f04ab00a7ca2", [
      [ "DIRECT", "d1/de9/a02591.html#ac114bc833ee6820805f2f04ab00a7ca2a1ea328a644e627283a35716a54108248", null ],
      [ "EXPANSION", "d1/de9/a02591.html#ac114bc833ee6820805f2f04ab00a7ca2a3bf18f494c2c28fde7cc5f5ca2057c18", null ]
    ] ],
    [ "ConnectDirect", "d1/de9/a02591.html#a2346983939d1a32bb6a18a6453792dff", null ],
    [ "GetSelectedTab", "d1/de9/a02591.html#a65e2fc3010260be49710146b5b32025a", null ],
    [ "Init", "d1/de9/a02591.html#ab212fc3a61460b771a18d71f052d31e8", null ],
    [ "IsInDirect", "d1/de9/a02591.html#a7409cf82d4f514853fa4b72a742298f7", null ],
    [ "OnClick", "d1/de9/a02591.html#aee6a27cad085ec84d5f5dbb765b63b53", null ],
    [ "Refresh", "d1/de9/a02591.html#a3056ff926528b94b606ba7e8617dda3d", null ],
    [ "EXPANSION_DIRECT_TAB_INDEX", "d1/de9/a02591.html#aba52b0a4756cc8f6eef5e0ea00186e40", null ],
    [ "m_DirectTab", "d1/de9/a02591.html#ad555a273c5c45a605d3dfbb87c86d425", null ],
    [ "m_IsDirect", "d1/de9/a02591.html#a4ef86aa9bd6a59cf9155565c7beff39f", null ]
];