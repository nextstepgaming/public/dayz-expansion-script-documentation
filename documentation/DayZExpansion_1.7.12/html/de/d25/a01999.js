var a01999 =
[
    [ "ExpansionBookMenuTabRules", "de/d25/a01999.html#a872f37be5ab3d7c2075274e0ff280f80", null ],
    [ "AddRule", "de/d25/a01999.html#ab26d08bff18c5710b7b5ce37380bf326", null ],
    [ "AddRuleCategory", "de/d25/a01999.html#a9e8ede5a4498fdd01a63bbfa659f7118", null ],
    [ "CanShow", "de/d25/a01999.html#ab15fac2c593a375b8c1c44964aa8a818", null ],
    [ "GetControllerType", "de/d25/a01999.html#acc80b920dd604296fcc462bcf0897fac", null ],
    [ "GetLayoutFile", "de/d25/a01999.html#a3f06ae12536413cc0cff91550ce8eb49", null ],
    [ "GetTabColor", "de/d25/a01999.html#a41c56eb84abf23f3164739a73ac58c4c", null ],
    [ "GetTabIconName", "de/d25/a01999.html#a1af31ebf909826a7312889af37eefb07", null ],
    [ "GetTabName", "de/d25/a01999.html#a9548cec3c6f08b8afab025a77147f933", null ],
    [ "IsParentTab", "de/d25/a01999.html#a2413c04b4d2978e0d281a824c8940fa3", null ],
    [ "OnBackButtonClick", "de/d25/a01999.html#a726ad3676c836f4510631b642812e781", null ],
    [ "OnHide", "de/d25/a01999.html#a41da9b34ab96666712b68a1073ba005f", null ],
    [ "OnMouseEnter", "de/d25/a01999.html#aed84815bc137830fa2e9ce822d31021e", null ],
    [ "OnMouseLeave", "de/d25/a01999.html#a8ef9541aaa73e1263e4341f9597db8a6", null ],
    [ "OnShow", "de/d25/a01999.html#a6b22e550400ee361e4bcdf23a2b52d68", null ],
    [ "SetCategory", "de/d25/a01999.html#aae10794bb9d34637bd5cb771989ca51a", null ],
    [ "SetView", "de/d25/a01999.html#ac73063c60949eab7a768f6181cbaef0e", null ],
    [ "m_RulesTabController", "de/d25/a01999.html#ab3b993ca6e73bedf782f9b0bd929d282", null ],
    [ "rules_list_scroller", "de/d25/a01999.html#afea8aa86725b67dc86d22dbf6f17c2d9", null ]
];