var a05265 =
[
    [ "bayonets", "de/daf/a05265.html#a24cb0d8a2d87c6d1388c531f5265d96b", null ],
    [ "bullets", "de/daf/a05265.html#a60c314096ff9ebaa5ba1270705f0f028", null ],
    [ "buttstocks", "de/daf/a05265.html#ac39d2c8d2bfc90677b97226658a2a3f2", null ],
    [ "ghillies", "de/daf/a05265.html#a956f46350797b6c21850ab1df28ba218", null ],
    [ "handguards", "de/daf/a05265.html#a1707ffd04070f1f5ed2982581a4e5d05", null ],
    [ "lights", "de/daf/a05265.html#a2c55011dbb674b2008263d9fb8fc51dd", null ],
    [ "magazines", "de/daf/a05265.html#ac2387dba915f646cbb4da464c2915da0", null ],
    [ "optics", "de/daf/a05265.html#ad3004670d3bf7164b413bf3cc5b3897e", null ],
    [ "rails", "de/daf/a05265.html#aa7025be6b9e8858b371797bec17e6c1b", null ],
    [ "suppressors", "de/daf/a05265.html#a1496ef424fe9472bcb2ca154abdfcc7f", null ],
    [ "switchables", "de/daf/a05265.html#a8b239173ee817e5b3e8b76727279cf02", null ]
];