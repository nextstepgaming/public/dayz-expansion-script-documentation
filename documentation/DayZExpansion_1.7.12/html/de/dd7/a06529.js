var a06529 =
[
    [ "AddDelivery", "de/dd7/a06529.html#aee6ffafcb7699ca75790f6a57a82b27a", null ],
    [ "GetDeliveries", "de/dd7/a06529.html#aa9a4976dafde2b7d1b9fcaabe370f772", null ],
    [ "GetMarkerName", "de/dd7/a06529.html#ad3bd923dfdce5da0208d008dd9894667", null ],
    [ "GetMaxDistance", "de/dd7/a06529.html#a1bc400171b5a2fe3c11aa3f7704c1778", null ],
    [ "GetPosition", "de/dd7/a06529.html#a6322e70ff53e3b2b85fcf0f41fa2ac30", null ],
    [ "OnRecieve", "de/dd7/a06529.html#af56d402ab48ccee5bbf07cf12e1d346f", null ],
    [ "OnSend", "de/dd7/a06529.html#a210bc00a8286c9749c12c7b0e6d218ed", null ],
    [ "QuestDebug", "de/dd7/a06529.html#acc93ee818bdb7e6bea51f42d5bccbd01", null ],
    [ "Save", "de/dd7/a06529.html#a6b9af33db6d9529d4ecf066d1c57a1c7", null ],
    [ "SetMarkerName", "de/dd7/a06529.html#a90cfda370c09e82a63dbc5b767083d80", null ],
    [ "SetMaxDistance", "de/dd7/a06529.html#a4a72950a56ea9fb0261448cfbff98f34", null ],
    [ "SetPosition", "de/dd7/a06529.html#ace89af54922675bfdf65a55f39bddbd4", null ],
    [ "Deliveries", "de/dd7/a06529.html#a45a2c39811a43e4626897d02f19559f1", null ],
    [ "MarkerName", "de/dd7/a06529.html#a170ff71292ca60dea6dc216324d99c61", null ],
    [ "MaxDistance", "de/dd7/a06529.html#a17078e469e6d61237f3d02b37520bec5", null ],
    [ "Position", "de/dd7/a06529.html#a9d54649db865a266fe14f5ca3d2be9ae", null ]
];