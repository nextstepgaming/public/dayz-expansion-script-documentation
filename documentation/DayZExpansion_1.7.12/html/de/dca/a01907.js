var a01907 =
[
    [ "ExpansionMarketMenuState", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061", [
      [ "INVALID", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061aef2863a469df3ea6871d640e3669a2f2", null ],
      [ "NONE", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061ac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "LOADING", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061a3a147bbbb7cd01dc067e6306c965769a", null ],
      [ "DIALOG", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061a4c775f01785801b0b11ec0cd5bdd7230", null ],
      [ "REQUESTING_SELECTED_ITEM", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061a99e64ac9b196737892c5702ad4d1ced2", null ],
      [ "REQUESTING_PURCHASE", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061a714f7e5797a65c8fd86fb6a17fffc1ee", null ],
      [ "REQUESTING_SELL", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061ae04f85a1ebe88f8f1b462e6d92a3e348", null ],
      [ "COUNT", "de/dca/a01907.html#a59f555e4cf0d0b298dbb85dab72d4061a2addb49878f50c95dc669e5fdbd130a2", null ]
    ] ]
];