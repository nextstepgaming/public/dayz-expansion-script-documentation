from os import path, walk


if __name__ == '__main__':
	whitelist = [
		'1_Core',
		'2_GameLib',
		'3_Game',
		'4_World',
		'5_Mission',
		'Common',
	]

	input_files = list()

	for folder, _, _ in walk('P:\\DayzExpansion'):
		for term in whitelist:
			if folder.lower().endswith(term.lower()):
				input_files.append(folder)

	with open('doxygen_input_file_includes', 'w') as f:
		for file in input_files:
			f.writelines(f'INPUT += {file}\n')
